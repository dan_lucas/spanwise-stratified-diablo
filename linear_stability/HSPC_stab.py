from numpy import *
from pylab import *
from cheb  import cheb
from scipy import linalg

def linS(kx,kz,Re,Fh,Pr,N):

#    Pr=700.0

    Ri=1./(Fh**2.)
    nu=1./Re
    k2=(kx**2+kz**2)

    D,x = cheb(N);
    D2 = dot(D,D)              
    D2 = D2[1:-1,1:-1]
    D = D[1:-1,1:-1]
    
    id=eye(N-1)
    zz=zeros((N-1,N-1),dtype=complex)
    u=diag(x[1:-1])
    du=id
    
    A = zeros((5*(N-1),5*(N-1)),dtype=complex)
    
    A[0:N-1,0:N-1] = nu*(D2-k2*id)-1J*kx*u
    A[0:N-1,N-1:2*(N-1)] = -du
    A[0:N-1,4*(N-1):5*(N-1)] = -1J*kx*id
    
    A[N-1:2*(N-1),N-1:2*(N-1)] = nu*(D2-k2*id)-1J*kx*u
    A[N-1:2*(N-1),4*(N-1):5*(N-1)] = -D
    
    
    A[2*(N-1):3*(N-1),2*(N-1):3*(N-1)] = nu*(D2-k2*id)-1J*kx*u
    A[2*(N-1):3*(N-1),3*(N-1):4*(N-1)] = -Ri*id
    A[2*(N-1):3*(N-1),4*(N-1):5*(N-1)] = -1J*kz*id
    
    A[3*(N-1):4*(N-1),2*(N-1):3*(N-1)] = id
    A[3*(N-1):4*(N-1),3*(N-1):4*(N-1)] = nu*(D2-k2*id)-1J*kx*u
    
    A[4*(N-1):5*(N-1),0:(N-1)] = 1J*kx*id
    A[4*(N-1):5*(N-1),(N-1):2*(N-1)] = D
    A[4*(N-1):5*(N-1),2*(N-1):3*(N-1)] = 1J*kz*id
    
    B = eye(5*(N-1))
    
    B[4*(N-1):5*(N-1),4*(N-1):5*(N-1)]=0.0

    eigVals,eigVecs =linalg.eig(A,B)
    
    sumE=0.0
    NN=0
    maxE=0.0
    for i in range(np.size(eigVals)):
        if(np.real(eigVals[i]) >0.0 and not(np.isinf(np.real(eigVals[i] ))) and np.real(eigVals[i])<1e9):
            sumE=sumE+np.real(eigVals[i])
        #            print(np.real(eigVals[i]))                                                                              
            maxE = max(maxE,np.real(eigVals[i]))
            NN=NN+1

    return sumE, maxE, NN
