#!/usr/bin/python                                                                                                    
import numpy as np
import math as m
import sys
from HSPC_stab import linS

N=257
Re=1.e4
Fh=0.1
for k in range(100):
    kx = 0.75+(3.25*k/100.0)
    kz = np.sqrt(m.pi*m.pi/(Fh*Fh*0.65*0.65*kx*kx)-kx*kx - m.pi*m.pi + 1./(Fh*Fh*0.65*0.65))
    sig=linS(kx,kz,Re,Fh,N)
    
    print '{}	{}	{}	{}	{}	{}	{}'.format(Re,Fh,kx,kz,sig[2],sig[1],sig[0])
    sys.stdout.flush()
print("  /n")

