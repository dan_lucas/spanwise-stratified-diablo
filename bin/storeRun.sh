#!/bin/bash

N=$1

ListFiles="*.txt *.saved *.res *.vis lic_* end.h5 start.h5 out.h5"

if [ -d "$N" ]; then 
    echo " Error. The run with name "$N" already exist. "
    exit 0
else
# Secure the grid files
    mkdir $N
    for item in $ListFiles
    do 
	if [ -f $item ]; then
	    mv $item  $N/.
	fi
    done
    # mv $N/[x,y,z]grid*.txt .
    cp grid.h5    $N"/."
    cp grid_def*  $N"/."
    cp input*.dat $N"/."
fi
