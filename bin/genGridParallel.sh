#!/bin/bash

GetNumberNy(){
    local FileIn=$1
    local NY=$(sed -n 5p $FileIn)
    NY=${NY%%)*}
    NY=${NY##*=}
    echo $NY
}

if [ -z $PATH_DIABLO ]
then 
SDIR="/data/oceanus/ed441/code/diablo/"
else
SDIR=$PATH_DIABLO
fi

cp $SDIR"/pre_process/create_grid_y_mpi2.m" create_grid_y_mpi.m
cp $SDIR"/pre_process/matlab_basher.sh" .

FileIn=$1
NP=$2
FileUt=$3

# Create the grid
NY=$(GetNumberNy $FileIn)
# Get the number of processes per node which keep that number of total NY
# NYP=(NY-1)/NP + 1
if [ $(expr '(' '(' $NY - 1 ')' % $NP ')') -ne 0 ]
then 
    echo " Error. Combination of NP and NY are not suitable to be used. "
    echo " Choose something else. "
    exit 0
fi
NYP=$(expr '(' '(' $NY - 1 ')' '/' $NP ')' + 1 )
./matlab_basher.sh "create_grid_y_mpi.m" "GRID_TYPE=1;nprocs="$NP";NY="$NYP";L=2;CS=1.75;"

# Generate the new grid_file
sed '5 c \\t PARAMETER(NY='$NYP')' $FileIn > $FileUt

# CleanUp
rm matlab_basher.sh create_grid_y_mpi.m