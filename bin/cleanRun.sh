#!/bin/bash

ListFiles="*.txt *.saved *.res *.vis lic_*"

# Secure the grid files
mkdir temp
mv [x,y,z]grid*.txt temp/.
# Remove files
rm $ListFiles
# Get back the grid files
mv temp/* .
rm [x,y,z]grid_out.txt
rmdir temp