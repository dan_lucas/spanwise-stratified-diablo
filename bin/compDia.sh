#!/bin/bash

HHOME=$(pwd)

if [ -z $PATH_DIABLO ]
then 
SDIR="/data/oceanus/ed441/code/diablo/for"
else
SDIR=$PATH_DIABLO"/for"
fi

CONLY="null"
FONLY="no"
FCLN="no"

N=$#

for (( c=1 ; c<=$N ; c++ ))
do 
    ARG=$1
    if [ "${ARG:0:8}" = "-SimDir=" ]
    then
	SDIR=${ARG#-SimDir=}
    elif [ "${ARG:0:9}" = "-Options=" ]
    then
	FDIA=${ARG#-Options=}
    elif [ "${ARG:0:7}" = "-Clean=" ]
    then	
	FCLN=${ARG#-Clean=}
    fi
    shift
done

cd $SDIR

echo " "
echo " Bla directory: "$SDIR

echo " "
echo " Compiling diablo ... "
echo "    FLAGS: "$FDIA

cp grid_def grid_def.bu
rsync -t $HHOME"/grid_def" .
if [ $FCLN = "yes" ]
then
    make clean
fi
make $FDIA > $HHOME"/cbla.out"
if [ $? = 1 ]
then
    echo " "
    echo " *** Error compiling bla "
    echo " "
    exit 1
fi
    
cp diablo $HHOME"/."
mv grid_def.bu grid_def

cd $HHOME

echo " "
echo " \$ ls -l diablo"
ls -l diablo

echo " "
echo " Compilation completed successfully."