#!/bin/bash 

GetNumberNy(){
    local FileIn=$1
    local NY=$(sed -n 5p $FileIn)
    NY=${NY%%)*}
    NY=${NY##*=}
    echo $NY
}
command -v matlab >/dev/null 2>&1 || { echo >&2 "I require matlab but it's not installed.  Aborting."; exit 1; }

if [ -z $PATH_DIABLO ]
then 
SDIR="/data/oceanus/ed441/code/diablo/"
else
SDIR=$PATH_DIABLO
fi

cp $SDIR"/pre_process/create_grid_h5.m" create_grid_h5.m
cp $SDIR"/pre_process/matlab_basher.sh" .

NY=$(GetNumberNy $1) 
if [ -f "grid.h5" ]; then rm grid.h5; fi
./matlab_basher.sh create_grid_h5.m "GRID_TYPE=1;N="$NY";L=2;CS=1.75;"

# CleanUp
rm matlab_basher.sh create_grid_h5.m
