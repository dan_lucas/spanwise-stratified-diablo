#!/bin/bash

GetNumberNy(){
    local FileIn=$1
    local NY=$(sed -n 5p $FileIn)
    NY=${NY%%)*}
    NY=${NY##*=}
    echo $NY
}

if [ -z $PATH_DIABLO ]
then 
SDIR="/data/oceanus/ed441/code/diablo/for"
else
SDIR=$PATH_DIABLO"/for"
fi

FileIn=$1
NPROCY=$2

# Create the grid
NY=$(GetNumberNy $FileIn)
# Get the number of processes per node which keep that number of total NY
# NYP=(NY-1)/NPROCY + 1
if [ $(expr '(' '(' $NY - 1 ')' % $NPROCY ')') -ne 0 ]
then 
    echo " Error. Combination of NP and NY are not suitable to be used. "
    echo " Choose something else. "
    exit 0
fi
NYP=$(expr '(' '(' $NY - 1 ')' '/' $NPROCY ')' + 1 )

# Generate the new grid_file
sed '5 c \\t PARAMETER(NY='$NYP')' $FileIn > grid_def

