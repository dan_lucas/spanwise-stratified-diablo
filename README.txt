____________________________________________________________________________________________________________________________

	Edited DIABLO codes with gravity in the z-direction
____________________________________________________________________________________________________________________________

	This repository contains a modified version of the DNS code DIABLO, originally by John Taylor 
	which is available here 
	https://bitbucket.org/edeusebio85/diablo 
	or 
	https://github.com/johnryantaylor/DIABLO/

	The principle modification rotates gravity from the y (finite-difference) direction to z (periodic) direction. 
	In doing so we add a background stratification and solve for the perturbation density which is periodic. 

	This code is to accompany the article 
	
	"Layer formation and relaminarisation in plane Couette flow with spanwise stratification" by Lucas, Caulfield & Kerswell. 
	JFM 2019
	https://arxiv.org/abs/1808.01178
	
	Please cite this work when publishing using this code.

	Also included is a linear stability code used in the above, written in python, making use of Chebyshev polynomials.

	See the directories herein for further readme information. Please feel free to use the contact details below for assistance using the code. 
    	
  -----------
  Contact
  -----------

        Authors: Dan Lucas
        Institution: Keele University
        Email:  d.lucas1@keele.ac.uk
	
March 2019
