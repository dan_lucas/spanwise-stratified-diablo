#!/bin/bash

mscript=$1
shift
matlab_exec=matlab
echo $* > matlab_command.m
cat $mscript >> matlab_command.m
${matlab_exec} -nodisplay >& /dev/null << EOF          
matlab_command
exit
EOF
# -nojvm -nodisplay -nosplash < matlab_command.m
rm matlab_command.m
