% function create_grid_y_mpi(GRID_TYPE,nprocs,NY,LY,CSY)
% This file creates a grid in the y-direction for input to DIABLO
% Written by John Taylor, 10/23/2005
% The grid definitions are produced as follows:
%   1 First, define the GY grid based on a specified function with
%     loops over j=0,NY+1 where the 0 and NY+1 correspond to the bottom
%     and top wall values respectively.  Since matlab is unable to store
%     elements in the zero index, all arrays are indexed starting at 1.
%   2 Then, define the fractional grid GYF halfway between neighboring
%     GY grid locations
%   3 Stretch both GYF and GY so that GYF(0) and GYF(NY) now correspond
%     to the upper and lower walls.
% To create a new grid stretching function, the user must just add a case
% for step 1.

% Select the type of grid function to be used
% --- disp('1) Closed channel');
% --- disp('2) Open channel');
% --- disp('3) Input from file');
% --- disp('4) Surface boundary layer');
% --- GRID_TYPE=input('Select a grid type: ');

% Set the dimensions of the grid 
% This should match NY in grid_def
% --- nprocs=input('Enter the number of processors: ');
% --- NY=input('Enter number of grid cells per processor, NY: ');

% Enter the domain size unless we are reading the grid
% ---   LY=input('Enter the domain size: ');
% Select the stretching parameter
% ---   CSY=input('Enter the stretching parameter, CSY (1.75 recommended): ');

% Prevent CS from being exactly zero to prevent divison by zero
CS=max(CS,1e-12);

%Update with the real number of points in the vertical direction
N=(NY-1)*nprocs+1;

%Generate the grid in the standard way
if (GRID_TYPE==1) 
% Closed Channel
  for J=1:N+1
    G(J+1)=(L/2.0)*tanh(CS*((2.0*(J-1))/(N)-1.0))/tanh(CS);
  end
elseif (GRID_TYPE==2)
% Grid for a shear layer, with higher resolution in the center
  for J=1:N+1
    G(J+1)=(L/2.0)*(CS*((2.0*(J-1))/N-1.0)^3+(2.0*(J-1))/N-1.0)/(CS+1);
  end
elseif (GRID_TYPE==3)
% Closed Channel
  for J=1:N+1
    G(J+1)=(L/2.0)*tanh(CS*((2.0*(J-1))/(N)-1.0))/tanh(CS)+L/2.0;
  end
elseif (GRID_TYPE==4)
% For boundary layer grid
  for J=1:N+1
    G(J+1)=L*tanh(CS*((N+2-J)/N-1.0))/tanh(CS)+L;
  end
  G(:)=G(:)*-1;
else
   disp('Error, entered grid type unknown');
end

% The following lines are done for all cases
% First, define the half (fractional) grid points
for J=1:N
  GF(J+1)=(G(J+1)+G(J+2))/2.0;
end

if (GRID_TYPE==4)
% For boundary layer grid put G(N) at z = 0 and G(2) at z = -H (with one
% added to the index)
gy_lower=G(2+1);
gy_upper=G(N+1);
for J=1:N+1
  G(J+1)=G(J+1)*L/(gy_upper-gy_lower);
end
gy_upper=G(N+1);
for J=1:N+1
  G(J+1)=G(J+1)*L/(gy_upper-gy_lower);
end
for J=1:N
  GF(J+1)=(G(J+1)+G(J+2))/2.0;
end
GF(1)=GF(2)-(G(3)-G(2));

else
% Scale both grids to place GF(2) and GF(NY+1) at the walls
gf_lower=GF(2);
gf_upper=GF(N+1);
g_lower=G(2);
g_upper=G(N+2);
for J=1:N
  GF(J+1)=GF(J+1)*(g_upper-g_lower)/(gf_upper-gf_lower);
end
for J=1:N+1
  G(J+1)=G(J+1)*(g_upper-g_lower)/(gf_upper-gf_lower);
end
gf_lower=GF(2);
gf_upper=GF(N+1);
% And shift the grids if necessary
shift=g_lower-gf_lower;
for J=1:N
  GF(J+1)=GF(J+1)+shift;
end
for J=1:N+1
  G(J+1)=G(J+1)+shift;
end
gf_lower=GF(2);
gf_upper=GF(N+1);
end

% Subdivide the grid in the processors
for np=1:nprocs

% Now, write the grid to file
count=1;
matwrite(1)=NY;
matwrite(2:NY+2)=G(2+(NY-1)*(np-1):2+(NY-1)*(np-1)+NY);
matwrite(NY+3:2*NY+2)=GF(2+(NY-1)*(np-1):1+(NY-1)*(np-1)+NY);

% % For testing the grid stretching
% for j=2:N-1
%   r(j)=(GF(j+1)-GF(j))/(GF(j)-GF(j-1));
% end
% disp('The maximum grid-stretching ratio is:'),max(r)
% disp(' ');
dlmwrite(['ygrid' num2str(np) '.txt'],matwrite','\t');
% disp('grid.txt has been written to the current directory');
% disp('  re-name this file to either xgrid.txt, ygrid.txt, or zgrid.txt');
% disp('  and move to the desired case directory');
    
end


