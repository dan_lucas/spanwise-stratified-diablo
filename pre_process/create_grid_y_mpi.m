% This file creates a grid in the y-direction for input to DIABLO
% Written by John Taylor, 10/23/2005
% The grid definitions are produced as follows:
%   1 First, define the GY grid based on a specified function with
%     loops over j=0,NY+1 where the 0 and NY+1 correspond to the bottom
%     and top wall values respectively.  Since matlab is unable to store
%     elements in the zero index, all arrays are indexed starting at 1.
%   2 Then, define the fractional grid GYF halfway between neighboring
%     GY grid locations
%   3 Stretch both GYF and GY so that GYF(0) and GYF(NY) now correspond
%     to the upper and lower walls.
% To create a new grid stretching function, the user must just add a case
% for step 1.

% Select the type of grid function to be used
disp('1) Closed channel');
disp('2) Open channel');
disp('3) Input from file');
GRID_TYPE=input('Select a grid type: ');

% Set the dimensions of the grid 
% This should match NY in grid_def
nprocs=input('Enter the number of processors: ');
NY=input('Enter number of grid cells per processor, NY: ');

if (GRID_TYPE ~= 4)
% Enter the domain size unless we are reading the grid
  LY=input('Enter the domain size: ');
% Select the stretching parameter
  CSY=input('Enter the stretching parameter, CSY (1.75 recommended): ');
end

for np=1:nprocs

if (GRID_TYPE==1) 
  % Closed Channel
  for J=1:NY+1
    GY(J+1)=(LY/2.0)*tanh(CSY*((2.0*(J-1+NY*(np-1)))/(nprocs*NY)-1.0))/tanh(CSY);
  end
elseif (GRID_TYPE==2)
  % Open channel
  for J=1:NY+1
    GY(J+1)=(LY/2.0)*tanh(CSY*((2.0*(J-1+NY*(np-1)))/(nprocs*NY)-1.0))/tanh(CSY)+(LY/2.);
  end
elseif (GRID_TYPE==3)
  % Read grid from file
  disp('Place the grid to be read in the file ''./y_input.txt''')
  disp('It should contain NY+1 rows with one number per row')
  disp('The values at the first and last row should correspond to the')
  disp('boundary locations.  When ready, press ''Enter'' ...');
  pause
  yinput=dlmread('y_input.txt');
  for J=1:NY+1
    GY(J+1)=yinput(J,1);
  end
else 
   disp('Error, entered grid type unknown');
end

% The following lines are done for all cases
% First, define the half (fractional) grid points
for J=1:NY
  GYF(J+1)=(GY(J+1)+GY(J+2))/2.0;
end

% Now, scale both grids to place GYF(2) and GYF(NY+1) at the walls
gyf_lower=GYF(2);
gyf_upper=GYF(NY+1);
gy_lower=GY(2);
gy_upper=GY(NY+2);
for J=1:NY
  GYF(J+1)=GYF(J+1)*(gy_upper-gy_lower)/(gyf_upper-gyf_lower);
end
for J=1:NY+1
  GY(J+1)=GY(J+1)*(gy_upper-gy_lower)/(gyf_upper-gyf_lower);
end
gyf_lower=GYF(2);
gyf_upper=GYF(NY+1);
% And shift the grids if necessary
shift=gy_lower-gyf_lower;
for J=1:NY
  GYF(J+1)=GYF(J+1)+shift;
end
for J=1:NY+1
  GY(J+1)=GY(J+1)+shift;
end
gyf_lower=GYF(2);
gyf_upper=GYF(NY+1);


% Now, write the grid to file
count=1;
matwrite(1)=NY;
for j=1:NY+1
  count=count+1;
  matwrite(count)=GY(j+1);
end
for j=1:NY
  count=count+1;
  matwrite(count)=GYF(j+1);
end
dlmwrite(['ygrid' num2str(np) '.txt'],matwrite','\t');

end

