.. diablo_guide documentation master file, created by
   sphinx-quickstart on Fri Feb  6 10:32:47 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to diablo_guide's documentation!
========================================

Contents:

.. toctree::
   :maxdepth: 1

   intro
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

