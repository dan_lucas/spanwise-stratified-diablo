\documentclass[a4paper, 10pt]{article}

\usepackage[latin1]{inputenc}
\usepackage[american]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{chngpage}
\usepackage{amsmath}
\usepackage{verbatim}
\usepackage{natbib}
\usepackage{color}
\usepackage{hyperref}
\usepackage{alltt}

\title{HDF5 and Diablo}
\author{Enrico Deusebio}

\newcommand{\HDFWebSite}{\url{http://www.hdfgroup.org/HDF5}}
\newcommand{\CommandFormat}[1]{\hspace{5cm}\begin{alltt}  #1 \end{alltt}}
\newcommand{\CommandShell}[2]{\vspace{0.25cm} \begin{alltt}  ed441: #1 \$ #2 \end{alltt} \vspace{0.25cm} }

\begin{document}

\maketitle

This guide gives an overview of the HDF5 subroutines which have been implemented in DIABLO and provides a framework to handle IO operations. 

\section*{What is HDF5}

The Hierarchical Data Format 5 (HDF5) is a very flexible standard data format designed to handle large amount of data in a very structured fashion. The main tools for scientific computing, such as Python or Matlab, generally provide interfaces to read/write HDF5 files. Fortran and C codes need to be linked to HDF5 libraries (can be found here \HDFWebSite). In the following we only provide an very general overview on HDF5 and how DIABLO HDF5 files are organized. For further information, we refer the interested reader to the userguide (\url{http://www.hdfgroup.org/HDF5/doc/index.html}). 

\subsection*{Structure of HDF5}

HDF5 allows to create files with a very flexible structure similar to a directory tree. The structure is composed by two basic primary elements 
\begin{itemize}
\item {\it Groups} corresponding to directories, allow one to organise the data
\item {\it Datasets} correponsing to files and constitutes the actual data contained in the file
\end{itemize}
Both groups and datasets can also be further described by attributes, which can be short information or data associated with them. 

\subsection{Advantages of HDF5}

HDF5 has several advantages with respect to other types of IO operations and it is becoming increasingly used in several fields, especially in the context of High-Performance Computing. 

\begin{itemize}
\item {\it Flexibilty.} HDF5 allows a large degree of flexibility, letting the user decide how to organize his/her own file in a tree-like structure. Compared to binary or formatted output data, there are no headers and/or no sequential reading/writing, thus  making it very convenient for backwards compatibility when developing codes.
\item{\it Large amount of data.} HDF5 also allows the handling of large amount of data which need not to be all read in the memory. It allows to read only part of the file and subsets of the datasets, making it possible to post-process big files in local machines if needed. 
\item{\it Parallel IO.} HDF5 features very efficient implementations for handling IO operations in parallel codes. The interface, available both in Fortran and C, automatically manage the processes and the IO operations on single file by multiple processes. This is very convinient as in many codes, parallel IO is done by either dumping one file for each process (overloading file systems for large number of processes) or communicating all the information to/from the master process that writes to/reads from the disk.     
\end{itemize}

Perhaps the only drawback of HDF5 is that it requires to install the HDF5 library on local machines, although the process is usually smooth and effortless. In many HPC centres, due to its increasing use, HDF5 is already present and it is one of the recommended IO procedures for PRACE allocations.

\section*{HDF5 Procedures}

In the following we give a quick overview of the HDF5 procedure implemented in Diablo. They are contained in two files: \verb=hdf5s.f= to be used when the code is compiled serially; \verb=hdf5.f= when the code is compiled for parallel use. The two files features exactly the same subroutines (same name and arguments) with slightly different implementation in order to account for the parallel output. This allows the core codes (\emph{e.g.} \verb=channel.f=) to be the same in parallel and serial run; and depending on the actual compilation rules (PARALLEL=TRUE or PARALLEL=FALSE) reference to the appropiate subroutine.  

\vspace{0.5cm}

\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf \verb=WriteHDF(FNAME,SAVE_PRESSURE)=.} Writes out an entire flow field to disk.

\vspace{0.2cm}
\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf Args:}
\begin{itemize} 
\item  \verb=FNAME : character*55= 
  
  Name of the output file. 

\item  \verb=SAVE_PRESSURE : logical= 
  
  Flag to save also the pressure field. This was done in order to assure perfect agreement between restarting simulations (recomputing the pressure when a file is read would otherwise introduce a small error). 
\end{itemize}

\end{minipage}

\end{minipage}




\vspace{0.5cm}

\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf \verb=ReadHDF(FNAME)=.} Reads in a flow field from the disk. After reading the flow field overwrites the variable \verb=U1=, \verb=U2=, \verb=U3= and \verb=TH= and converts them to spectral space into the variable \verb=CU1=, \verb=CU2=, \verb=CU3= and \verb=CTH=. If the field {\verb=P=} exists, it reads also the pressure. 

\vspace{0.2cm}
\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf Args:}
\begin{itemize} 
\item  \verb=FNAME : character*55= 
  
  Name of the file to be read in. 
\end{itemize}

\end{minipage}

\end{minipage}




\vspace{0.5cm}

\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf \verb=ReadGridHDF(FNAME,coord)=.} Reads in the grid (or the relevant portion of the grid in parallel cases) from a file. 

\vspace{0.2cm}
\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf Args:}
\begin{itemize} 
\item  \verb=FNAME : character*55= 
  
  Name of the file containing the grid. 

\item  \verb=coord : integer=
  
  Integer referring to the coordinate (1-$x$, 2-$y$, 3-$z$) whose grid ought to be read. {\bf Note:} At the present stage, only \verb#coord=2# has been implemented.  
 
\end{itemize}

\end{minipage}

\end{minipage}


\vspace{0.5cm}

\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf \verb=ReadGridHDF(FNAME,coord)=.} Reads in the grid (or the relevant portion of the grid in parallel cases) from a file. 

\vspace{0.2cm}
\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf Args:}
\begin{itemize} 
\item  \verb=FNAME : character*55= 
  
  Name of the file containing the grid. 

\item  \verb=coord : integer=
  
  Integer referring to the coordinate (1-$x$, 2-$y$, 3-$z$) whose grid ought to be read. {\bf Note:} At the present stage, only \verb#coord=2# has been implemented.  
 
\end{itemize}

\end{minipage}

\end{minipage}




\vspace{0.5cm}

\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf \verb=WriteHDF5_XYPlane(FNAME,gname,var2d)=.} Writes out a $x$-$y$ plane. 

\vspace{0.2cm}
\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf Args:}
\begin{itemize} 
\item  \verb=FNAME : character*35= 
  
  Name of the output file. 

\item  \verb=gname : character*10=
  
  Name of the group where the new plane will be placed. The planes are stored with an increasing id. The group has a property \verb=SAMPLES= representing the number of planes saved. Each dataset has a property \verb=Time= representing the simulation time at which the plane has been saved.   

\item  \verb=var2d : real*8, dimension(NX,NY)=
  
  2D plane to be saved. Note that in parallel cases \verb=NY= is the number of vertical points per process.  
 
\end{itemize}

\end{minipage}

\end{minipage}




\vspace{0.5cm}

\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf \verb=WriteHDF5_ZYPlane(FNAME,gname,var2d)=.} Writes out a $y$-$z$ plane. 

\vspace{0.2cm}
\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf Args:}
\begin{itemize} 
\item  \verb=FNAME : character*35= 
  
  Name of the output file. 

\item  \verb=gname : character*10=
  
  Name of the group where the new plane will be placed. The planes are stored with an increasing id. The group has a property \verb=SAMPLES= representing the number of planes saved. Each dataset has a property \verb=Time= representing the simulation time at which the plane has been saved.   

\item  \verb=var2d : real*8, dimension(NZ,NY)=
  
  2D plane to be saved. Note that in parallel cases \verb=NY= is the number of vertical points per process.  
 
\end{itemize}

\end{minipage}

\end{minipage}


\vspace{0.5cm}

\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf \verb=WriteHDF5_XZPlane(FNAME,gname,var2d)=.} Writes out a $x$-$z$ plane. 

\vspace{0.2cm}
\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf Args:}
\begin{itemize} 
\item  \verb=FNAME : character*35= 
  
  Name of the output file. 

\item  \verb=gname : character*10=
  
  Name of the group where the new plane will be placed. The planes are stored with an increasing id. The group has a property \verb=SAMPLES= representing the number of planes saved. Each dataset has a property \verb=Time= representing the simulation time at which the plane has been saved.   

\item  \verb=var2d : real*8, dimension(NX,NZ)=
  
  2D plane to be saved. 
 
\end{itemize}

\end{minipage}

\end{minipage}


\vspace{0.5cm}

\noindent The next two subroutines are generally commented out and may be useful when bug-hunting. 


\vspace{0.5cm}

\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf \verb=WriteHDF5_var_real(FNAME)=.} Writes out to the disk a \emph{real} variable. The variable is written out to the \verb=FNAME= file under the group \verb=U=. The variable to be written out should be stored in a variable \verb=real*8, dimension(0:NX+1,0:NZP+1,0:NY+1) tvar= to be declared in the header.

\vspace{0.2cm}
\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf Args:}
\begin{itemize} 
\item  \verb=FNAME : character*35= 
  
  Name of the output file. 

\end{itemize}

\end{minipage}

\end{minipage}



\vspace{0.5cm}

\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf \verb=WriteHDF5_var_complex(FNAME)=.} Writes out to the disk a \emph{complex} variable. The variable is written out to the \verb=FNAME= file under the group \verb=U=. The variable to be written out should be stored in a variable \verb=complex*16, dimension(0:NXP,0:NZ+1,0:NY+1) tvar= to be declared in the header.

\vspace{0.2cm}
\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf Args:}
\begin{itemize} 
\item  \verb=FNAME : character*35= 
  
  Name of the output file. 

\end{itemize}

\end{minipage}

\end{minipage}



\vspace{0.5cm}


\section*{Diablo HDF5 Files}

In the following we provide a brief description of the HDF5 files used in DIABLO. 



\vspace{0.5cm}

\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\large \bf \verb=grid.h5=.} 

\vspace{0.2cm}

File containing the grid. Now only the $y$ grid is supported, altough this can be easily expanded to include grids in $x$ and $z$ as well. 

\vspace{0.2cm}
\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf Groups}
\begin{itemize} 
\item  \verb=/grids= 
  contains the locations of the grids. 
  
  {\it Datasets}
  
  \begin{itemize}
    \item \verb=y=. Location of the points in the full grid. The fractional grid is calculated within diablo. 
  \end{itemize}

\end{itemize}

\end{minipage}

\end{minipage}




\vspace{0.5cm}

\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf \large \verb=end.h5= and \verb=end.h5=} 

\vspace{0.2cm}

Full flow fields in physical space. 

\vspace{0.2cm}
\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf Groups}
\begin{itemize} 
\item  \verb=/= 
  Root with general information about the flow and the simulation
  
  {\it Attributes}

  \begin{itemize}
    \item \verb=Resolution=. Number of points in each direction.  
    \item \verb=Data=. Data of creation of the file.
    \item \verb=Info=. Additional extra-information about the file
  \end{itemize}

\item  \verb=/Timestep= 

  Group containing the flow field for a particular time step. Possibly, this could also be extended such that each file contains more flow fields in different group \verb=Timestep1=,\verb=Timestep2=, \verb=Timestep3=, ...
  
  {\it Attributes}

  \begin{itemize}
    \item \verb=Time=. Simulation time
  \end{itemize}

  {\it Datasets}

  \begin{itemize}
    \item \verb=U=. 3D flow field (streamwise velocity) 
    \item \verb=V=. 3D flow field (vertical velocity) 
    \item \verb=W=. 3D flow field (spanwise velocity) 
    \item \verb=TH1=. 3D flow field (first scalar)
    \item \verb=TH?=. 3D flow field (\# scalar)
    \item \verb=P=. 3D flow field (pressure)
  \end{itemize}

\end{itemize}

\end{minipage}

\end{minipage}



\vspace{0.5cm}

\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\large \bf \verb=movie.h5=.} 

\vspace{0.2cm}

   Files written with the \verb=writeHDF5_xyplane= and \verb=writeHDF5_zyplane= subroutines. The one below is just an example of a possible file

\vspace{0.2cm}
\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf Groups}
\begin{itemize} 
\item  \verb=u_xy= 
  
  {\it Attributes}

  \begin{itemize}
    \item \verb=SAMPLES= Number of planes in the group
  \end{itemize}

  {\it Datasets}

  \begin{itemize}
    \item \verb=0000=. U $x$-$y$ plane. An attribute \verb=Time= specifies the time instant. 
    \item \verb=0001=. U $x$-$y$ plane. An attribute \verb=Time= specifies the time instant. 
    \item $\cdots$ \verb=SAMPLES=  
  \end{itemize}

\end{itemize}

\end{minipage}

\end{minipage}


\vspace{0.5cm}

\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{ \large \bf \verb=pixie.h5=.} 

\vspace{0.2cm}

PIXIE format files are HDF5 files which have a predefined structure and that can be imported straight away into visualisation tools such as Visit or Paraview. The contains information about the data to be visualised and its grid. This works for both structured (rectangular and curvilinear) and unstructured grids.

\vspace{0.2cm}
\hfill\begin{minipage}{\dimexpr\textwidth-1cm}
{\bf Groups}
\begin{itemize} 
\item  \verb=/node_coords= 
  
  Group containing the location in $x$, $y$ and $z$ of the gridpoints.

  {\it Datasets}
  \begin{itemize}
    \item \verb=X=. Dataset of dimension \verb=[Nxg,Nyg,Nzg]= containing $x$ locations.
    \item \verb=Y=. Dataset of dimension \verb=[Nxg,Nyg,Nzg]= containing $y$ locations.
    \item \verb=Z=. Dataset of dimension \verb=[Nxg,Nyg,Nzg]= containing $z$ locations.
  \end{itemize}
  

\item  \verb=/Timestep= 
  
  This group has all the data pertaining to a particular grid pointed by the attribute of the group

  {\it Attributes}

  \begin{itemize}
    \item \verb=coords=. Three element arrays of strings referring to the dataset containing the grid, \emph{e.g.}
      \verb=["/node_coords/X ", "/node_coords/Y ", "/node_coords/Z "]=
    \item \verb=Time=. Data simulation time stamp 
  \end{itemize}

  {\it Datasets}

  Here one can put as many datasets as he wants (with the same grid). There is a one-to-one map between datasets and grid coordinates.

  \begin{itemize}
    \item \verb=NameVar1=.  Dataset of dimension \verb=[Nxg,Nyg,Nzg]=. 
    \item $\cdots$ \verb=NameVarN=  
  \end{itemize}

\end{itemize}

\end{minipage}

\vspace{0.5cm}

From version 1.8.0, HDF5 also supports external links, meaning that groups or datasets can also link to groups or data in other external files. Since it is often the case that several PIXIE files refer to the same grid, it could be efficient to store the grid only once and use external links to refer to it.  

\end{minipage}






\section*{HDF5 and computing tools}

Most of the tools used in scientific computing provide some interfaces to read/write HDF5 files. Here we provide a quick guide on some of them. 

\subsection*{Matlab}

Matlab supports a nice interface which allows to easily access HDF5. In order to have a glance of the HDF5 file just use

\CommandFormat{h5disp('myfile.h5')}

For reading a particular attribute, e.g. time in 'end.h5', use

\CommandFormat{h5readatt('end.h5','/Timestep','Time')}

whereas reading a dataset can be achieved by 

\CommandFormat{h5readatt('end.h5','/Timestep/U')}


\subsection*{Python}

In order to use python to read HDF5 files, an additional package, \verb=h5py=, must be installed (\url{http://www.h5py.org}). Moreover, I have developed an additional interface (module \verb=h5= in the ext folder) which provides very similar commands as the one found in matlab. 

Assuming that a \verb=h5py= package is installed and working, import the \verb=h5= module by 
 
\CommandFormat{import h5}

and then use the subroutines 

\CommandFormat{h5.h5disp}
\CommandFormat{h5.h5readatt}
\CommandFormat{h5.h5read}

similarly to what found in Matlab. 

\subsection*{From the command line ...}

When installing the HDF5 packages, some useful binaries are also installed. In the following, we outline some of them which can provides some insights into HDF5 files without the need of Python or Matlab. 

In order to list the content of an HDF5 files use

\CommandFormat{h5ls [-r] myfile.h5}

The flag \verb=-r= recursively enters into the groups, listing the content.

Some further information can also be provided using 

\CommandFormat{h5dump -H myfile.h5}

which gives an overview on all the elements, along with their attribute, data type and size. \verb=h5dump=, if the \verb=-H= flag is removed, can also read and print out the actual values of attributes and datasets (although this is not recommended for large files). 

Finally, if some further global informations on the file are needed (e.g. storage, number of groups, number of datasets, etc), refer to 

\CommandFormat{h5stat myfile.h5}

\vspace{0.4cm}

{\bf
Note: HDF5 is a C library in which the order of the fastest element changing in a matrix is reversed with respect to Fortran. Thus, don't be surprised if when printing the size of matrices with bash, dimensions are left-to-right flipped. 
}

\end{document}
