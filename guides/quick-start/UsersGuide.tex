%\documentclass[prb,12pt,letterpaper,superscriptaddress]{revtex4}
% Group addresses by affiliation; use superscriptaddress for long
% author lists, or if there are many overlapping affiliations.
% For Phys. Rev. appearance, change preprint to twocolumn.
% Choose pra, prb, prc, prd, pre, prl, prstab, or rmp for journal
%  Add 'draft' option to mark overfull boxes with black boxes
%  Add 'showpacs' option to make PACS codes appear
%  Add 'showkeys' option to make keywords appear
\documentclass[prb,final,groupedaddress]{revtex4}
%\documentclass[aps,prl,preprint,superscriptaddress]{revtex4}
%\documentclass[aps,prl,twocolumn,groupedaddress]{revtex4}

%\oddsidemargin 0.0in
%\textwidth 6.5in
%\topmargin 0.25in
%\headheight 0.25in
%\headsep 0.25in
%\textheight 9.0in


\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{floatflt}
\usepackage{verbatim}
\usepackage{subfigure}
\usepackage{alltt}

%\renewcommand\subfigcapskip{-10pt}

% \input{defgen}
% \input{defles}

%\def\figlen{3.75in}
%\def\figh{2.5in}
%\def\caplen{-0.30in}

% Single space =1.0, Double Space=2.0
\renewcommand\baselinestretch{1.5}

\pagenumbering{arabic}

\pagestyle{plain}

\renewcommand{\eqref}[1]{Eq.~(\ref{#1})}


%old
%\documentclass[12pt,doublespace]{article}
%\usepackage[pdftex]{graphicx}
%\usepackage{amsmath}

\begin{document}


\title{User's Guide to Diablo}
%\author{John Taylor}
%\affiliation{Department of Mechanical and Aerospace Engineering, University of California San Diego, La Jolla, CA}
\date{\today}

\maketitle

\newcommand{\CommandFormat}[1]{\hspace{5cm}\begin{alltt}  #1 \end{alltt}}
\newcommand{\CommandShell}[2]{\vspace{0.25cm} \begin{alltt}  ed441: #1 \$ #2 \end{alltt} \vspace{0.25cm} }

\newcounter{ctProb}
\setcounter{ctProb}{1}
\newcommand{\CommandPython}[1]{\hspace{5cm}\begin{alltt}  [\arabic{ctProb}] #1 \end{alltt} \addtocounter{ctProb}{1}}



\section{Getting Started}
In order to use Diablo, it is necessary to install a Fortran 90 compiler and FFTW, if they are not already installed. The Fortran 90 compiler is required since some features used in Diablo such as zero-sized arrays are not supported by Fortran 77. Fortunately, high quality Fortran 90 compilers are freely available for various platforms. Free compilers that have been successfully used with Diablo are g95 (www.g95.org), gfortran (http://gcc.gnu.org/wiki/GFortran\#download), ifort (The Intel Fortran compiler which is free for non-commerical use at: www.intel.com/cd/software/products/asmo-na/eng/282038.htm).  FFTW can be downloaded from www.fftw.org. Diablo has been tested using FFTW version 2.1.5. Unfortunately the newest versions of FFTW (3.x) use a different calling sequence than versions 2.x, so the older version must be used. In order to update Diablo to use the newer FFTW, the wrapper subroutines in ``fft.f'' need to be changed to reflect the new calling conventions.\\

\subsection{Optional packages}

In order to run Diablo on multiple processors with distributed memory, it is necessary to install MPI libraries on each computer. Diablo has been tested using LAM/MPI version 1.5, OpenMPI 1.6.1 and MPICH 3.0.4. Since only standard MPI subroutines are used, it should work on any standard MPI distribution. 
In order to run on a cluster, you will need to install MPI, FFTW, and Diablo on all computers. It will also be necessary to allow the computers to communicate without requiring the user's password. One way to do this, is to add a list of all hosts that will be running in parallel to the ``authorized\_keys'' file in the ``.ssh'' directory inside the user's home directory. Make sure this is working by trying to ssh between computers and verify that the login prompt is bypassed. 

% The script ``go\_mpi'' in the main Diablo directory shows how to start an MPI job on a cluster. The code is first compiled with mpif90 on the local computer, and the resulting executable and the input files are copied to each remote computer. The IP addresses of each host are specified, and the job is started with mpirun.\\

%The input parameters for each process should be the same, with the exception of the grid files which should cover only the local physical domain. A matlab script in the ``pre\_process'' directory called create\_grid\_y\_mpi.m illustrates how to create multiple grids for a domain decomposition (in this case in the y-direction).\\

Diablo includes subroutines for writing data in the NetCDF format. NetCDF is a self-describing binary format that can be used by a variety of data processing and visualization software. In order to use NetCDF, the library files must be downloaded and installed on the  local system. The NetCDF package is freely available from http://www.unidata.ucar.edu/software/netcdf. Diablo has been tested using NetCDF version 3.\*. A list of software that is capable of reading NetCDF files can be found at \url{http://www.unidata.ucar.edu/software/netcdf/software.html}. 

Diablo also includes subroutines for writing data in HDF5 format, which is a very flexible standard data format designed to handle large amount of data. In order to use these subroutines, the HDF5 library must be downloaded and installed in the local machine. We strongly recommend the use the HDF5 library for IO operations, especially when running in parallel with MPI. The HDF5 subroutines allow a large degree of flexibility and to use the same grid files and flow fields produced by other simulations, even when using different number of processes. {\bf At the present stage, only the channel version of diablo permit the use of HDF5 subroutines. }   

In order to enable additional library in Diablo, the MAKEFILE should be updated accordingly. Once new libraries are installed, the LINKDIR and INCLUDEDIR variables in the Diablo Makefile should be set to the locations where the relevant library and include files can be found. When more library are to be used, all the relevant path should be accessible. However, it is worth mentioning that nowadays MPI and HDF5 library generally comes with wrappers (\verb=mpif90= for MPI, \verb=h5fc= for serial HDF5 and \verb=h5pfc= for parallel HDF5). The wrapper should be substituted to the compiler and will make sure that the right files are included in the compilation. In the case that wrapper are used, it is enough to follow the compiling rules outlines in section \ref{makefile}.

Once FFTW and a Fortran 90 compiler are installed, we are ready to perform our first simulation of Diablo. The latest version of diablo can be cloned on the local machine using Mercurial (see the tutorial on Mercurial for further informations) using 

\CommandShell{anyFolder}{ hg clone https://edeusebio85@bitbucket.org/edeusebio85/diablo}

This should create a new working version of diablo. The diablo release contains the following subfolders

\begin{itemize}
\item \emph{bin} contains some useful bash scripts to handle simulations with diablo;
\item \emph{for} contains the source code;
\item \emph{guides} contains userguides on various matters (among which this quick start);
\item \emph{input\_files} contains examples of input files;
\item \emph{post\_process} contains subroutines/packages to be used for post-processing using different tools used in scientifici computing (e.g. Matlab, Python and Vapor);
\item \emph{pre\_process} contains a number of Matlab scripts to be used to generate the grids;
\item \emph{tests} allows to carry out some tests to verify that diablo compiles correctly and gives the right outputs.   
\end{itemize}

%After downloading the latest version of Diablo (available from http://fccr.ucsd.edu/software.html), unzip and expand the tarball with the unix commands ``gunzip diablo\_latest.tgz'' and ``tar -xvf diablo\_latest.tar''. Then inside the Diablo directory, open the Makefile with a text editor. At the top of the Makefile are various user options which are described in detail in section \ref{makefile}. For now, just change the name of the Fortran 90 compiler to the one that is locally installed, and make sure that the variable LINKDIR points to the location where the FFTW library was installed. To double-check the FFTW installation directory, search for the files: ``libfftw.a'' and ``libfftw.la''. Once the changes to the Makefile have been made, a test simulation using Diablo can be started by running the ``go'' script in the Diablo directory. This script is written using c-shell notation, so if you are using bash or some other shell, start a c-shell first by typing ``csh'' in the terminal. The run the script with ``./go''. The test simulation is a low resolution closed channel flow. After the code compiles, the simulation should start printing some data to the screen. If you have Matlab on your system, you can run matlab from within the ``post\_process/matlab'' subdirectory, and then run the Matlab script called ``realtime\_movie.m'' to get a `live' two-dimensional slice of the velocity field as the simulation progresses.\\



\section{Code Structure}
%The directory structure for Diablo has been organized into a main code directory and separate case subdirectories. The case directories contain the input and output files for each simulation and help to organize these files when multiple simulations are performed. 
The main code directory contains the Fortran source code for Diablo, header files and the Makefile.  The basic source code consists of the following:
\begin{itemize}
\item diablo.f - The main program for Diablo. This contains the main time-stepping loop.
\item diablo\_io.f - Input/Output subroutines for Diablo.
\item periodic.f - Subroutines for time-stepping of the triply-periodic case (zero wall-bounded directions).
\item channel.f - Subroutines for time-stepping of the channel geometry (one wall-bounded direction).
\item duct.f - Subroutines for time-stepping of the duct geometry (two wall-bounded directions).
\item cavity.f - Subroutines for time-stepping of the closed cavity geometry (three wall-bounded directions).
\item courant.f - A subroutine for setting the time-step based on the CFL condition for a specified Courant number.
\item fft.f - Wrappers for the FFTW library which are used for the periodic, channel, and duct cases.
\item mpi.f - (Optional) Subroutines for running Diablo with distributed memory parallelization
\item netcdf.f - (Optional) Subroutines used to write output files in the self-describing binary NetCDF format.
\item hdf5s.f - (Optional) Subroutines used in \emph{serial} runs to write output files in HDF5 format.
\item hdf5.f - (Optional) Subroutines used in \emph{parallel} runs to write output files in HDF5 format.
\end{itemize}
The direcory ``dummy\_code'' contains empty versions of the subroutines in mpi.f, netcdf.f and others which allows Diablo to be compiled and used without MPI or NetCDF enabled or installed.\\

\section{Makefile}
\label{makefile}
In the main code directory, a Makefile is provided to compile Diablo and create and executable. At the top of the Makefile are various compile-time user options.  These include:
\begin{itemize}
\item COMPILER - The name of the compiler to use.  Note while the source code is written in Fortran 77 fixed-form, a Fortran 90 compiler is required due to the use of several Fortran 90 features including zero sized arrays. Free compilers that have been successfully used with Diablo are g95 (www.g95.org), gfortran (http://gcc.gnu.org/wiki/GFortran\#download), ifort (The Intel Fortran compiler which is free for non-commerical use at: www.intel.com/cd/software/products/asmo-na/eng/282038.htm). 
\item USEROPTS - Any number of user specified compiler options.  Some common options are "-g" for debugging and "-OLEVEL" where LEVEL is an integer specifying the amount of compiler optimization. Larger LEVEL numbers will lead to a higher level of optimization at the expense of longer compile times.
\item LINKDIR - Location where FFTW (and other libraries) has been installed. The default location on linux systems is often /usr/local/lib.
\item INCLUDEDIR - Locations where other include files are located.  The default location on linux systems is often /usr/local/include. 
\item PARALLEL - An option to compile the code with distributed memory parallelism using MPI. PARALLEL = TRUE to use MPI, PARALLEL = FALSE to compile in serial mode. This will also specify the \verb=mpif90= wrapper as the compiler to be used. Make sure that MPI compatible Fortran compiler (such as mpif90) can be found in the enviromental variable PATH.
\item NETCDF - An option to use the NetCDF file format for output. This makes it easy to use the Diablo outupt files in a wide range of NetCDF compatible post-processing programs. Note that the NetCDF library needs to be installed on the local system if this option is enabled by NETCDF = TRUE.
\item HDF5 - An option to use the HDF5 file format for output. 
Note that the HDF5 library needs to be installed on the local system if this option is enabled by NETCDF = TRUE. For serial cases, e.g. PARALLEL=FALSE, this will set COMPILER to \verb=h5fc=. For parallel compilation, this will set COMPILER to \verb=h5pfc=.
\item LES - If this option is enabled with LES = TRUE, then the subroutines necessary for a large-eddy simulation will be compiled. Since the LES model adds many additional storage arrays, this should be disabled (by for direct numerical simulations (DNS) when the LES subroutines are not needed.
\end{itemize}

Diablo can be compiled simply by typing ``make''. When running ``make'' the Makefile will determine which files have been modified since the last time the Makefile was run, and only the modified files (and their dependencies) will be re-compiled. Sometimes it is desirable to re-compile all of the source code. To do this, first run ``make clean'', followed by ``make''.
When values of the above variables differ from the default value, this can be specified in the command line by typing, 

\CommandShell{for}{make PARALLEL=TRUE}

for compiling in parallel, and 

\CommandShell{for}{make PARALLEL=TRUE HDF5=TRUE}

for compiling in parallel with HDF5 libraries. 

\section{Grid}

Diablo uses a staggered grid in each direction that is treated with finite differences. This avoids the even/odd gridpoint de-coupling that can occur when using a co-located grid. Specifically, the wall-normal velocity in each finite difference direction is staggered with respect to other variables. Since Diablo uses cartesian grids, each three dimensional grid can be represented by a combination of three one-dimensional arrays of gridpoint locations. Diablo uses the `base grid', designated by GX, GY, and GZ for all variables in the periodic flow case. This grid is also used for the wall-normal velocity in each direction treated with finite-differences. Other quantities are defined on the `fractional grid', designated by GXF, GYF, and GZF. The fractional grid is defined to be exactly halfway between neighboring base grid points. Note that when grid-stretching is used, the converse is not true, the base grid points are not necessarily halfway between neighboring fractional grid points. A schematic of the staggered grid with wall locations is shown in Figure \ref{grid}.\\

\begin{figure}[h]
\centering
%\includegraphics[width=5.5in, height=0.91in]{./grid.pdf}
\caption{Grid layout of Diablo in the wall-bounded directions treated with finite differences. The wall-normal velocity is stored at $G$ points (open circles), all other variables are stored at $G_{F}$ points (closed circles). Note that $G$ here stands for $G_X$, $G_Y$, and/or, $G_Z$, depending on which directions are wall-bounded.}
\label{grid}
\end{figure}

When a given direction is treated with finite-differences, the grid spacing can be made non-uniform. For example, when considering wall-bounded flows, it is generally desirable to stretch the grid to increase the resolution near the wall in order to resolve the large gradients that then to occur in this region. In order to allow for nonuniform grids, Diablo requires a file containing the user-defined grid locations for each direction that uses finite-differences. Grid may be defined either with (a) ASCII files or (b) HDF5 files if the library are installed. In (a), the grid files should be named ``xgrid.txt'', ``ygrid.txt'', and ``zgrid.txt'' and placed in the case directory, as needed depending on which directions will be treated with finite differences (set through the NUM\_PER\_DIR parameter). In order to make it easier to create a grid, a script called ``create\_grid.m'' has been created and can be found inside the \verb=pre_process= directory. Either Matlab or Octave should be able to run this script, and the script can be modified to include user-defined stretching functions. In (b), the grids are contained in a HDF5 file called ``grid.h5'' (see the HDF5 guide for further information). This file can be generated by a script in the \verb=pre_process= directory, called ``create\_grid\_h5.m'' in a very similar manner as for ``create\_grid.m''. We recommend to use the (b) method when running simulation in parallel. When running in parallel, the total number of points $N_y$ should be chosed such that 
\begin{equation}
N_y= N_p \cdot \left( N_y^p - 1 \right) + 1, 
\end{equation}
where $N_y^p$ is the number of points per process (equal among the processes) and $N_p$ is the number of processes. For instance, $N_y=129$ is a suitable combination for $(N_y^p=17,N_y^p=8)$, $(N_y^p=33,N_y^p=4)$ and $(N_y^p=65,N_y^p=2)$.

%% It would also be straightforward to create the grid file with other means. It is an ASCII file with one entry per line with the following lines:
%% \begin{enumerate}
%% \item Integer number of gridpoints.
%% \item Base grid (G) locations indexed from 1...NY+1 (one entry per line).
%% \item Fractional grid (GF) locations indexed from 1...NY (one entry per line).
%% \end{enumerate}

\section{Input Parameter Files}
\label{input_diablo}
When Diablo is executed, two input files containing user-specified parameters will be read in.  All simulations, regardless of how many periodic or wall-bounded directions are used, will read the file ``input.dat'' from inside the specified case directory. This file contains the following paramters:
\begin{itemize}
\item FLAVOR - A string used to distinguish the modes of operation for Diablo. For example, if you want to add a specialized forcing term to Diablo, you could set FLAVOR to a flag of your choice, say FLAVOR="Body Force". Then inside the source code, appropriate hooks can be added with If statements to check the value of FLAVOR.
\item VERSION - The version number of the Makefile.  Used to ensure that the input file is compatible with the source code.
\item USE\_MPI - Option to use MPI
\item USE\_LES - If TRUE, then solve the LES filtered equations with the addition of a LES model.  Note that in order to use the LES, the flag LES should also be set to TRUE inside the Makefile.
\item NU - The viscosity in code units.  If the code has been nondimensionalized, then this constant becomes 1/Re where Re is the Reynolds number using the selected velocity and length scale.
\item LX, LY, LZ - The domain size.
\item NUM\_PER\_DIR - The number of periodic directions. This determines which timestepping algorithm will be used:\\
   3=periodic flow, 2=channel flow, 1=duct flow, 0=cavity flow
\item CREATE\_NEW\_FLOW - If TRUE, then initialize the velocity field using the CREATE\_FLOW subroutines, if FALSE, then initialize the velocity field from a restart file called diablo.saved
\item N\_TIME\_STEPS - Number of integration time steps.  Each timestep contains three Runge-Kutta substeps.
\item TIME\_LIMIT - Maximum wall-clock time in seconds
\item DELTA\_T - The size of the timestep if constant time-steps are used
\item RESET\_TIME - If TRUE, then the simulation time will start at zero, if FALSE, then the simulation will start using the time contained in the initial velocity field (if CREATE\_NEW\_FLOW=FALSE)
\item VARIABLE\_DT - If VARIABLE\_DT=TRUE then the CFL number will be used to specify the timestep every UPDATE\_DT timesteps
\item CFL - The constant CFL number that will be used to determine the size of the timesteps if VARIABLE\_DT=TRUE
\item UPDATE\_DT - How often (in timesteps) to update the CFL number. Since the procedure for determining the size of the time-step based on the CFL number is expensive, it is often more efficient to do this periodically instead of at every time step.
\item VERBOSITY - How much information should be print to the screen at runtime. High levels of VERBOSITY (integer value 1-5) will result in more information.
\item SAVE\_FLOW\_INT - How often (in timesteps) to save the entire 3d velocity field to a restart file.
\item SAVE\_STATS\_INT - How often to write mean statistics to a file
\item MOVIE - If TRUE, then write out 2d slices through the velocity field every SAVE\_STATS\_INT timesteps in order to create movie files. Note that these files can become very large, so make sure that enough disk space is available when using this option.
\item CREATE\_FLOW\_TH - If TRUE, then initialize the scalar field using CREATE\_TH subroutines. If FALSE, then initialize the scalar field by reading diablo\_th\*\*.start
\item FILTER\_TH - If TRUE, then apply a low-pass filter to the scalar field. Since sharp fronts often form in the scalar field which may result in numerical errors, applying a low-pass filter can help improve the quality of the simulation.
\item FILTER\_INT - If FITLER\_TH=TRUE, then FITLER\_INT sets how often (in timesteps) to apply the filter to the scalar field.
\item RI\_TAU - Quantified the influence of the scalar on the velocity field through the buoyancy term. In the code, the buoyancy term is RI\_TAU*THETA where THETA is the local scalar value. For example, if the density is used as the scalar in dimensional terms, then RI\_TAU should be = $-g/\rho_0$. If the equations have been nondimensionalized, then RI\_TAU should be also be made nondimensional using the appropriate scales.
\item PR - The Prandtl number for the scalar (Defined as the kinematic viscosity divided by the scalar diffusivity $\nu/\kappa$). 
\item BACKGROUND\_TH - This parameter is useful for considering a density stratification in a periodic direction. If this parameter is TRUE then perturbations to a linear background field are computed. This has been implemented in periodic.f to allow simulations of homogeneous stratified turbulence.
\end{itemize}

\section{Channel Geometry}
When NUM\_PER\_DIR=2 the the ``input.dat'' parameter file, the $x$ and $z$ directions will be made periodic and derivatives in these directions will be treated with a pseudo-spectral method, while derivatives in the $y$ direction will be evaluated with second-order, central finite differences. The time-stepping subroutines are contained in the file ``channel.f''. The grid will be staggered in the $y$ direction with the y-velocity, U2 defined on the GY grid, and all other variables defined on the fractional GYF grid. The required input files for a simulation using the channel flow geometry are ``ygrid.txt'' which contains the location of the staggered grid points, ``grid\_def'' which contains the grid size and the number of scalars to consider, ``input.dat'' which was described in Section \ref{input_diablo}, and ``input\_chan.dat'' which contains parameters that are native to the channel flow geometry. The file ``input\_chan.dat'' contains the following parameters:
\begin{itemize}
\item VERSION - The version number of the Makefile.  Used to ensure that the input file is compatible with the source code.
\item TIME\_AD\_METH - The time-stepping method used. Two options are available for the channel geometry, when TIME\_AD\_METH=1 all terms in the  momentum equations involving wall-normal derivatives are treated implicitly using Crank-Nicolson and all other terms are explicitly time-stepped using a third-order Runge-Kutta method. When TIME\_AD\_METH=2 all viscous terms are time-stepped using Crank-Nicolson and other terms are treated with Runge-Kutta. 
\item LES\_MODEL\_TYPE - This parameter gives the type of subgrid-scale model to use when performing a large-eddy simulation. When LES\_MODEL\_TYPE=1, a constant Smagorinsky model is used (a wall-damping function can be turned on or off inside les.f). When LES\_MODEL\_TYPE=2, a dynamic Smagorinsky model is used. The Smagorinsky constant is computed using the dynamic procedure, but this model is much more computationally expensive than the constant Smagorinksy. When LES\_MODEL\_TYPE=3, a scale-similar part is added to the dynamic model for a ``dynamic mixed model''. This is the most expensive of the three options, but has the advantage of providing a mechanism for energy backscatter from the subgrid scales to the resolved scales.
\item IC\_TYPE - If CREATE\_NEW\_FLOW=TRUE, this flag determines which set of initial conditions to use in the subroutine CREATE\_FLOW\_CHAN.
\item KICK - If CREATE\_NEW\_FLOW=TRUE, then we often want to add a random perturbation on top of the initial condition. This parameter sets the amplitude of the perturbation.
\item I\_RO\_TAU -   Sets the wall-normal rotational number (inverse of the Rossby number).
\item F\_TYPE -   Sets the type of body force applied: F\_TYPE=1 force with a constant pressure gradient PX0, F\_TYPE=0 use a pressure gradient that keeps the bulk velocity constant UBULK0, F\_TYPE=2 force with an oscillatory pressure gradient, amplidude AMP\_OMEGA0, frequency OMEAG0, and F\_TYPE>2 will not add a body force. 
\item U,V,W,TH\_BC\_LOWER - Set the type of boundary condition to be applied at the lower wall to the $x$-velocity, U, the $y$-velocity, V, the $z$-velocity, W, and the scalars TH (if any are used). If this parameter = 0, then Dirichlet boundary conditions will be applied with the value at the wall specified by \*\_BC\_LOWER\_C1.  If this parameter = 1, then Neumann-type boundary conditions will be applied by specifying the wall-normal gradient in \*\_BC\_LOWER\_C1. The other constants here, \*\_BC\_LOWER\_C2, etc. are not presently used, but are provided in case user-specified boundary conditions need more input data.
\item U,V,W,TH\_BC\_UPPER - Same as for \*\_BC\_LOWER, but specifies the boundary conditions at the upper wall.
\end{itemize}



\section{Creating a New Simulation}

In order to run a serial case (for a channel simulation), do the following steps:

\begin{enumerate}
\item Create a new directory called \verb=ser=
\item Copy the \verb=grid_def=, \verb=input.dat= and \verb=input_chan.dat= to \verb=ser=
\item Set the number of points in $x$, $y$ and $z$ in \verb=grid_def=  
\item Go to the \verb=for= folder of diablo 
\item Copy the \verb=grid_def= file in \verb=for= folder
\item Compile the code with \verb#make HDF5=TRUE# (hoping with no errors!)
\item Copy the executable \verb=diablo= to the run directory \verb=ser=
\item Generate the grid by running the Matlab script \verb=create_grid_h5.m= in the \verb=pre_process= folder
\item Copy the output file \verb=grid.h5= to the run folder
\item Set the appropriate input parameters in \verb=input.dat= and \verb=input_chan.dat=
\item If the simulation start from a flow field, copy the flow field \verb=flow.h5= to the folder and rename it as \verb=start.h5=
\item Run the simulation by issuing \verb=./diablo=
\end{enumerate}

Some of the binaries in the \verb=bin= folder can be helpful to automize the process. For instance \verb=compDia.sh= is a shell-script which automatically does from 4-7 and \verb=genGridHDF5.sh= does 8-9. To see how these work, first set the enviromental variable \verb=PATH_DIABLO=

\CommandShell{ser}{export PATH\_DIABLO=path/to/diablo/folder}

where \verb=path/to/diablo/folder= is the absolute path of the diablo folder. Then make the bash scripts in \verb=bin= visible by issuing 

\CommandShell{ser}{export PATH=\$PATH\_DIABLO/bin/:\$PATH}

At this point, you can use the shell script, simply by issuing from the \verb=ser= directory

\CommandShell{ser}{compDia.sh -Options=''HDF5=TRUE'' -Clean=yes}

This will compile the code and copy the executable to the current folder. Generate the \verb=grid.h5= file with 

\CommandShell{ser}{genGridHDF5.sh grid\_def}

, which automatically takes the number of points $N_y$ from \verb=grid_def=.

In order to run a parallel case, the workflow outlined above applies, although with some modifications. The \verb=grid_def= used to compile the code should have the number of points per prossess $N_y^p$ instead of $N_y$. It is good procedure to store the total resolution in a file called \verb=grid_def.all= and generate the appropriate \verb=grid_def= file by issuing 

\CommandShell{mpi}{genGridFiles.sh Np}

where \verb=Np= is the number of processes to be used. Diablo can then be compiled by using the bash script 

\CommandShell{mpi}{compDia.sh -Options=''HDF5=TRUE PARALLEL=TRUE'' -Clean=yes}

and run with 

\CommandShell{mpi}{mpirun -np Np ./diablo}

Runs in parallel will produce only one flow field but multiple statistic files. These files can be merged into a single file by using the python package which is described below. 


\subsection{Post-processing with Python}

In order to post-process the data with Python, the Python package \verb=h5py= must be installed which permits to read HDF5 files (\url{http://www.h5py.org}). Before starting ipython, make sure that the relevant paths are included in the enviromental variable \verb=PYTHONPATH=, e.g.
 
\CommandShell{ser}{export PYTHONPATH=\$PYTHONPATH:\$PATH\_DIABLO/post\_process/python/}
\CommandShell{ser}{export PYTHONPATH=\$PYTHONPATH:\$PATH\_DIABLO/post\_process/python/ext}

Start ipython from the run folder. Import the \verb=diablo_pack= module by 

\CommandPython{import diablo\_pack as d} 

You can then create a run object with 

\CommandPython{r=d.run.RunDiablo('grid\_def.all')} 

The \verb=r= object will contain a bunch of attributes and methods, characteristic of the run. If a parallel run has outputted seperate statistic files, these can be merged by using 

\CommandPython{r.join\_stats()} 

and then the statistics can be read in the object with 

\CommandPython{r.read\_stat()} 

This will create a \verb=StatsDiablo= object file in \verb=r.st=, which contains all the statistics. For instance if mean profiles (averaged along the simulation) are to be plotted, use

\CommandPython{plot(r.st.yf,np.mean(r.st.Var['ume'],0))} 
 
(this assumes that you have imported the module \verb=numpy= as \verb=np=). Finally, if a flow field needs to be read, just create a \verb=FlowDiablo= object by issuing 

\CommandPython{r.read\_flow('end.h5')} 

which store the object in the dictionary \verb=r.flow= pointed by its name,  \emph{e.g.}

\CommandPython{myFlow=r.flow['end.h5']}.

For further information about the objects and methods of the Python package, please refer to its manual.  

%% In order to run a new case do the following steps:
%% \begin{enumerate}
%% \item Create a new case directory with a name of your choice.
%% \item Copy grid\_def, and input.dat from an existing directory. Copy either input\_per.dat, input\_chan.dat, input\_duct.dat, or input\_cavity.dat to the current directory, for simulations with three, two, one, or zero periodic directions, respectively. Then edit the contents of these input files as desired.
%% \item In the diablo/pre\_process directory run create\_grid\_*.m in matlab or octave to create a new grid for each direction that will use finite differences (non periodic) and move the created *grid.txt files to the new case directory.
%% \item If the simulation will start from a user-specified flow field, set the initial conditions in the create\_flow\_(per,chan,duct, or cavity) and create\_th\_(per,chan,duct, or cavity) subroutines.
%% \item Edit the script diablo/go by setting the rundir equal to the new case directory.
%% \item Execute diablo by ./go from the diablo/ directory.
%%  (Note, If you have just changed the size of the grid in grid\_def,
%%   the makefile may not recognize the change and recomplie.  If you get
%%   an error in the grid dimensions, type "./make clean" and then re-run "./go"
%% \item As the simulation runs, output files will be created inside the case directory.
%\end{enumerate}




\section{Triply Periodic Geometry}
When NUM\_PER\_DIR=3 in ``input.dat'', periodic boundary conditions will be applied in all three directions and all spatial derivatives will be treated with a pseudo-spectral method. Since all directions are treated in the same manner, and since all variables are co-located on the same grid, the algorithm for this case is the most straightforward of the four options in Diablo, and this case may be a good place to start to learn the methods used in Diablo. The user-defined options specific to this case are specified in a file called ``input\_per.dat'' located inside the case directory. The contents of this file are:
\begin{itemize}
\item VERSION - A flag to set the version number of the input files. This is used to make sure that the source code and input files are compatible.
\item TIME\_AD\_METH - Integer flag to specify which time-stepping method to use. At this time, only one method is active in periodic.f, so this parameter should always be set to 1.
\item LES\_MODEL\_TYPE - This flag will be used to specify which LES model should be used. However, at this time the LES model has not been written for the triply periodic geometry.
\item IC\_TYPE, KICK - Parameters used to set the initial conditions for the velocity field. If CREATE\_NEW\_FLOW = TRUE, then IC\_TYPE=0 will initialize the velocity with a Taylor-Green vortex, IC\_TYPE=1 will use an ideal vortex with an axis aligned with the $y$-axis. Users can create new initial conditions in the CREATE\_FLOW\_PER subroutine inside periodic.f
\item BACKGROUND\_GRAD(N) - This parameter should be set for each scalar that is used in the simulation. If BACKGROUND\_GRAD(N)=TRUE, then scalar number N will be solved for perturbations about a linear background profile, with a gradient in the $y$-direction. This is designed to allow simulations of stratified turbulence in a triply periodic geometry where density perturbations are assumed to be periodic in all three directions.
\end{itemize}

\section{Frequently Asked Questions}
\noindent \textbf{Q}: Why are indices for the three-dimensional arrays ordered $i,k,j$?\\
\textbf{A}: This is done in order to speed-up the FFTs as much as possible. With the exception of the cavity geometry, it is expected that the FFTs will be the most expensive part of the Diablo algorithm. Therefore, we can get a significant speedup by ordering the arrays to keep the data for the Fourier transforms as close together as possible in memory. Fortran stores data in memory starting with the first index.  That is data with the index (i,k,j) will be stored next to the data for (i+1,k,j). Since the duct flow geometry uses FFTs in the $x$ direction, the $i$ index is first, and since the channel flow geometry performs FFTs in the $x$ and $z$ directions, it makes sense to index over $k$ next.\\

\noindent \textbf{Q}: Can I compile and run Diablo under Windows?\\
\textbf{A}: The short answer is yes, but we don't recommend it. High performance computing is normally done in C or Fortran on a machine running a unix-like operating system (such as linux, mac os X, ibm aix, etc). If you don't have access to a dedicated unix (or linux) machine, probably the best option is to make your computer running Windows dual-boot either by creating a linux partition on your current hard drive, or by installing linux on a separate hard drive. However, some students in MAE 223 at UCSD were able to get Diablo running under Windows using ``cygwin'', although it took significant work to get this going. Here is a description of how to do this, courtesy of Karsten Breddermann.
\begin{enumerate}
\item Erase any already installed versions of g95\item Download cygwin from http://www.cygwin.com/  Just the blue link under the logo\item Install the "Devel" packages using cygwin\item Download g95 from g95.org for cygwin (http://ftp.g95.org/g95-x86-cygwin.tgz)\item Download fftw 2.1.5 from www.fftw.org.\item Decompress the FFTW install directory.  Using a terminal in cygwin, go to this directory.  In the Makefile, change the lines F77 = g77 to F77 = g95 and ac\_ct\_F77 = g77 to ac\_ct\_F77 = g95.  In the FFTW install directory, run the configure script by "./configure", followed by "make" and "make install".  After the installation is finished, you can check it by running "make check".  Note after "make install" the directory that it has installed the FFTW library, this will probably end in /usr/local/lib.\item Download Diablo from http://fccr.ucsd.edu and hope for the best!
\end{enumerate}

\noindent \textbf{Q}: Why do I get an error like: ``In function init\_fft\_: undefined reference to fftwnd\_ftt\_\*''?\\
\textbf{A}: For several possible reasons. The first thing to do is to make sure that the FFTW library is installed in the same location as the LIBDIR specified in the Makefile. Look for the files: ``librfftw.a'' and ``librfftw.la'' in this directory, and make sure that you have installed FFTW version 2.*. If this are ok and you still get the error, there may be a problem with appended underscores. The following is from the website http://g95.sourceforge.net/docs.html, although it applies to compilers other than g95 too:
``While g95 produces stand-alone executables, it is occasionally desirable to interface with other programs, usually C. The first difficulty that multi-language program will face is the names of the public symbols. G95 follows the f2c convention of adding an underscore to public names, or two underscores if the name contains an underscore. The -fno-second-underscore and -fno-underscoring can be useful to force g95 to produce names compatible with your C compiler.'' Try adding these options to the USEROPS in the Makefile. If this still doesn't work, then you may have to go into fft.f and add one or more trailing underscores to the subroutines names in the FFTW library. In fft.f, add an underscore to all call statements to subroutines containing ``FFTWND''. Try compiling the Makefile again, and if you get a similar error, try adding a second underscore. I have even come across a situation where I needed three underscores (it was on a shared machine where multiple users had installed different version of FFTW).\\


\end{document}






 
