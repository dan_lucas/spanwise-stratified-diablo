C******************************************************************************|
C periodic.f, the fully-periodic-box solvers for diablo.           VERSION 0.9
C
C These solvers were written by Tom Bewley and John Taylor.
C******************************************************************************|

C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      SUBROUTINE INIT_PER
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|

      INCLUDE 'header'

      IF (N_TH.gt.0) THEN	
      EPSILON_TARGET=((1.d0/DX(1))**4.d0)*(NU**3.d0)*(PR(1))**(-2.d0)
!      EPSILON_TARGET=((1.d0/DX(1))**4.d0)*(NU**3.d0)*(100.d0)**(-2.d0)
      write(*,*) 'EPSILON_TARGET: ',EPSILON_TARGET
      END IF		   
	
      RETURN
      END

C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      SUBROUTINE RK_PER_1
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
C Main time-stepping algorithm for the fully periodic case
C INPUTS  (in Fourier space):  CUi, P, and (if k>1) CFi at (k-1)  (for i=1,2,3)
C OUTPUTS (in Fourier space):  CUi, P, and (if k<3) CFi at (k)
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      INCLUDE 'header'
      REAL*8 TEMP1, TEMP2, TEMP3, TEMP4, TEMP5, TEMP6
      INTEGER I,J,K,N

C Compute the RHS in Fourier Space, CRi.

C If using moving source, propogate the source
      IF (TRUCK) CALL RK_SOURCE

C First, define the constants used for time-stepping
      TEMP1=NU*H_BAR(RK_STEP)/2.0
      TEMP2=BETA_BAR(RK_STEP)*H_BAR(RK_STEP)
      TEMP3=ZETA_BAR(RK_STEP)*H_BAR(RK_STEP)
      TEMP4=H_BAR(RK_STEP)
	TEMP5=0.0

	if ((FLAVOR.eq.'Batch') .AND. (delta_t.lt.0.0)) then
	  call backwards_constants(TEMP1,TEMP2,TEMP3,TEMP4,TEMP5)
	end if

      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
C Start with the explicit part of the Crank-Nicolson viscous term and
C  the pressure gradient treated with Explicit Euler:
            TEMP5=1-TEMP1*(KX2(I)+KY2(J)+KZ2(K))
		
C The following hook should be used for regularizing the NS equations.
C If marches are backwards-in-time (ie delta_t<0) then it will be used.
		IF (DELTA_T.LT.0) CALL quasi_rev_per(TEMP5,i,j,k)
		
            CR1(I,K,J)=TEMP5*CU1(I,K,J)-TEMP4*(CIKX(I)*CP(I,K,J))
            CR2(I,K,J)=TEMP5*CU2(I,K,J)-TEMP4*(CIKY(J)*CP(I,K,J))
            CR3(I,K,J)=TEMP5*CU3(I,K,J)-TEMP4*(CIKZ(K)*CP(I,K,J))
C For each scalar, start with the explict part of the Crank-Nicolson
C diffusive term for each scalar
            DO N=1,N_TH
              TEMP6=1-(TEMP1/PR(N))*(KX2(I)+KY2(J)+KZ2(K))
     &               -REACTION(N)*TEMP1
              CRTH(I,K,J,N)=TEMP6*CTH(I,K,J,N)
            END DO
          END DO
        END DO
        IF (RK_STEP .GT. 1) THEN
          DO K=0,TNKZ
            DO I=0,NKX
C Add the term: ZETA_BAR(RK_STEP)*R(U(RK_STEP-1))
              CR1(I,K,J)=CR1(I,K,J)+TEMP3*CF1(I,K,J)
              CR2(I,K,J)=CR2(I,K,J)+TEMP3*CF2(I,K,J)
              CR3(I,K,J)=CR3(I,K,J)+TEMP3*CF3(I,K,J)
C Do the same for each scalar:
              DO N=1,N_TH
                CRTH(I,K,J,N)=CRTH(I,K,J,N)+TEMP3*CFTH(I,K,J,N)
              END DO
            END DO
          END DO
        END IF
      END DO
C If we are considering a linear background scalar gradient then add
C the term owing to advection of the background state.
C This allows us to consider a mean scalar gradient (ie stratification)
C even though the vertical boundary conditions are periodic.
C (In this case the passive scalar is a perturbation from a linear
C gradient. This gradient and the vertical domain size are used to
C make the passive scalar nondimensional, so here the nondimensional
C gradient is equal to one
      DO N=1,N_TH
      IF (BACKGROUND_GRAD(N)) THEN
C If there is a background scalar gradient add advection term:
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CFTH(I,K,J,N)=-CU2(I,K,J)
          END DO
        END DO
      END DO 
      ELSE
C Otherwise don't
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CFTH(I,K,J,N)=0.D0
          END DO
        END DO
      END DO 
      END IF
      END DO

C Inverse transform to physical space to compute the nonlinear terms
      CALL FFT_XZY_TO_PHYSICAL(CU1,U1)
      CALL FFT_XZY_TO_PHYSICAL(CU2,U2)
      CALL FFT_XZY_TO_PHYSICAL(CU3,U3)

C Here, for the Chemotaxis simulations,
C   transform only the bacterial concentration to physical space
C   leave the nutrient concentration (TH(:,:,:,1)) in Fourier space
      DO N=2,N_TH 
        CALL FFT_XZY_TO_PHYSICAL(CTH(0,0,0,N),TH(0,0,0,N))
      END DO


C Calculate the Chemotaxis terms
C Note that it is assumed that the nutrient concentration is in scalar #1
      IF (FLAVOR.eq.'CHEMOTAXIS') THEN
C Do for each bacterial species
      DO N=2,N_TH
C Here, calculate each of the three chemotaxis terms
C First, CHI*d/dx(B*dC/dx):
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CS1(I,K,J)=CIKX(I)*CTH(I,K,J,1)
          END DO
        END DO
      END DO
      CALL FFT_XZY_TO_PHYSICAL(CS1,S1)
      DO J=0,NYM
        DO K=0,NZM
          DO I=0,NXM
            S1(I,K,J)=S1(I,K,J)*TH(I,K,J,N)
          END DO
        END DO
      END DO
      CALL FFT_XZY_TO_FOURIER(S1,CS1)
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CFTH(I,K,J,N)=CFTH(I,K,J,N)-CHI(N)*CIKX(I)*CS1(I,K,J)
          END DO
        END DO
      END DO
C Now, calculate the CHI*d/dy(B*dC/dy) term:
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CS1(I,K,J)=CIKY(J)*CTH(I,K,J,1)
          END DO
        END DO
      END DO
      CALL FFT_XZY_TO_PHYSICAL(CS1,S1)
      DO J=0,NYM
        DO K=0,NZM
          DO I=0,NXM
            S1(I,K,J)=S1(I,K,J)*TH(I,K,J,N)
          END DO
        END DO
      END DO
      CALL FFT_XZY_TO_FOURIER(S1,CS1)
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CFTH(I,K,J,N)=CFTH(I,K,J,N)-CHI(N)*CIKY(J)*CS1(I,K,J)
          END DO
        END DO
      END DO
C Finally, calculate the CHI*d/dz(B*dC/dz) term:
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CS1(I,K,J)=CIKZ(K)*CTH(I,K,J,1)
          END DO 
        END DO
      END DO
      CALL FFT_XZY_TO_PHYSICAL(CS1,S1)
      DO J=0,NYM
        DO K=0,NZM
          DO I=0,NXM
            S1(I,K,J)=S1(I,K,J)*TH(I,K,J,N)
          END DO
        END DO
      END DO
      CALL FFT_XZY_TO_FOURIER(S1,CS1)
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CFTH(I,K,J,N)=CFTH(I,K,J,N)-CHI(N)*CIKZ(K)*CS1(I,K,J)
          END DO
        END DO
      END DO

C End loop over bacterial species
      END DO
C End if CHEMOTAXIS
      END IF

C Now that we are done with the Chemotaxis terms, we need to transform the
C nutrient concentration to physical space for calculating the nonlinear
C advection terms

      IF (N_TH.gt.0) THEN
        CALL FFT_XZY_TO_PHYSICAL(CTH(0,0,0,1),TH(0,0,0,1))
      END IF


C Compute the nonlinear terms for the passive scalar equation
C Do this before the nonlinear momentum terms to use Fi as a working
C  array before using it for the momentum equation.
      IF (TRUCK) CALL INIT_FORCING

      DO N=1,N_TH
        DO J=0,NYM
          DO K=0,NZM
            DO I=0,NXM
              F1(I,K,J)=U1(I,K,J)*TH(I,K,J,N)
              F2(I,K,J)=U2(I,K,J)*TH(I,K,J,N)
              F3(I,K,J)=U3(I,K,J)*TH(I,K,J,N)
            END DO
          END DO
        END DO
        CALL FFT_XZY_TO_FOURIER(F1,CF1)
        CALL FFT_XZY_TO_FOURIER(F2,CF2)
        CALL FFT_XZY_TO_FOURIER(F3,CF3)
        DO J=0,TNKY
          DO K=0,TNKZ
            DO I=0,NKX
              CFTH(I,K,J,N)=CFTH(I,K,J,N)-CIKX(I)*CF1(I,K,J)
     &                      -CIKY(J)*CF2(I,K,J)
     &                      -CIKZ(K)*CF3(I,K,J)
     
C Add the forcing due to the moving source
              IF (TRUCK) CALL FORCING(I,K,J)
     
C Add R-K terms for the TH equation to the RHS
              CRTH(I,K,J,N)=CRTH(I,K,J,N)+TEMP2*CFTH(I,K,J,N)
            END DO
          END DO
        END DO
      END DO
C The RHS vector for the TH equation is now ready

C Compute the nonlinear terms for the momentum equations
      DO J=0,NYM
        DO K=0,NZM
          DO I=0,NXM
            F1(I,K,J)=U1(I,K,J)*U1(I,K,J)
            F2(I,K,J)=U1(I,K,J)*U2(I,K,J)
            F3(I,K,J)=U1(I,K,J)*U3(I,K,J)
            S1(I,K,J)=U2(I,K,J)*U2(I,K,J)
          END DO
        END DO
      END DO
      CALL FFT_XZY_TO_FOURIER(F1,CF1)
      CALL FFT_XZY_TO_FOURIER(F2,CF2)
      CALL FFT_XZY_TO_FOURIER(F3,CF3)
      CALL FFT_XZY_TO_FOURIER(S1,CS1)
C Here we start constructing the R-K terms in CFi
C Note, that the order of the following operations are important
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CF1(I,K,J)=-CIKX(I)*CF1(I,K,J)
     *                 -CIKY(J)*CF2(I,K,J)
     *                 -CIKZ(K)*CF3(I,K,J)
            CF2(I,K,J)=-CIKX(I)*CF2(I,K,J)
     *                 -CIKY(J)*CS1(I,K,J)
            CF3(I,K,J)=-CIKX(I)*CF3(I,K,J)
            IF (GUSTS) CALL GUST(I,K,J)
          END DO
        END DO
       END DO
C At this point, F1,F2,F3 contain some of the nonlinear terms

C Compute the remaining nonlinear terms
C We cannot use F1,F2,F3 for storage, so use S1 as the working variable
       DO J=0,NYM
        DO K=0,NZM
          DO I=0,NXM
            S1(I,K,J)=U2(I,K,J)*U3(I,K,J)
          END DO
        END DO 
       END DO
       CALL FFT_XZY_TO_FOURIER(S1,CS1)
       DO J=0,TNKY
         DO K=0,TNKZ
          DO I=0,NKX
            CF2(I,K,J)=CF2(I,K,J)-CIKZ(K)*CS1(I,K,J)
            CF3(I,K,J)=CF3(I,K,J)-CIKY(J)*CS1(I,K,J)
          END DO
         END DO
       END DO
       DO J=0,NYM
         DO K=0,NZM
          DO I=0,NXM
            S1(I,K,J)=U3(I,K,J)*U3(I,K,J)
          END DO
         END DO
       END DO
       CALL FFT_XZY_TO_FOURIER(S1,CS1)
       DO J=0,TNKY
         DO K=0,TNKZ
          DO I=0,NKX
            CF3(I,K,J)=CF3(I,K,J)-CIKZ(K)*CS1(I,K,J)
          END DO
         END DO
       END DO
C Done with the computation of nonlinear terms

C If the scalar is active (RI_TAU NE 0), add the bouyancy forcing term
C  as explicit R-K
       DO N=1,N_TH
C Fisrt, convert back to Fourier space
       CALL FFT_XZY_TO_FOURIER(TH(0,0,0,N),CTH(0,0,0,N))
       DO J=0,TNKY
         DO K=0,TNKZ
           DO I=0,NKX
             CF2(I,K,J)=CF2(I,K,J)+RI_TAU(N)*CTH(I,K,J,N)
           END DO
         END DO
       END DO
       END DO

C Add some forcing to the system to keep the Batchelor scale fixed
      EK=0.d0
      DO J=0,NYM
        DO K=0,NZM
          DO I=0,NXM
              EK=EK+U1(I,K,J)**2.d0+U2(I,K,J)**2.d0+U3(I,K,J)**2.d0
          END DO
        END DO
      END DO
C Note, that each cell has the same volume, so we can just average over all points
      EK=EK/dble(NX*NY*NZ)
! Scale EK by an amount to compensate for dissipation from 2/3 de-aliasing:
      EK=0.8d0*EK
      DO J=0,NYM
        DO K=0,NZM
          DO I=0,NXM
            S1(I,K,J)=(EPSILON_TARGET/EK)*U1(I,K,J)
          END DO
        END DO
      END DO
      CALL FFT_XZY_TO_FOURIER(S1,CS1)
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CF1(I,K,J)=CF1(I,K,J)+CS1(I,K,J)
          END DO
        END DO
      END DO
      DO J=0,NYM
        DO K=0,NZM
          DO I=0,NXM
            S1(I,K,J)=(EPSILON_TARGET/EK)*U2(I,K,J)
          END DO
        END DO
      END DO
      CALL FFT_XZY_TO_FOURIER(S1,CS1)
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CF2(I,K,J)=CF2(I,K,J)+CS1(I,K,J)
          END DO
        END DO
      END DO
      DO J=0,NYM
        DO K=0,NZM
          DO I=0,NXM
            S1(I,K,J)=(EPSILON_TARGET/EK)*U3(I,K,J)
          END DO 
        END DO
      END DO
      CALL FFT_XZY_TO_FOURIER(S1,CS1)
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CF3(I,K,J)=CF3(I,K,J)+CS1(I,K,J)
          END DO
        END DO
      END DO

C Now, add the R-K terms to the RHS
C Note, this has already been done for the scalar field TH
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CR1(I,K,J)=CR1(I,K,J)+TEMP2*CF1(I,K,J)
            CR2(I,K,J)=CR2(I,K,J)+TEMP2*CF2(I,K,J)
            CR3(I,K,J)=CR3(I,K,J)+TEMP2*CF3(I,K,J)
          END DO
        END DO
      END DO
C Computation of CRi complete.

C Now solve the implicit system for the intermediate field.
C (In the fully-periodic case, this is easy!)
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            TEMP5=1+TEMP1*(KX2(I)+KY2(J)+KZ2(K))

C The following hook should be used for regularizing the NS equations.
C If marches are backwards-in-time (ie delta_t<0) then it will be used.		
		IF (DELTA_T.LT.0) CALL quasi_rev_per(TEMP5,i,j,k)

            CU1(I,K,J)=CR1(I,K,J)/TEMP5
            CU2(I,K,J)=CR2(I,K,J)/TEMP5
            CU3(I,K,J)=CR3(I,K,J)/TEMP5
            DO N=1,N_TH
              TEMP6=1+(TEMP1/PR(N))*(KX2(I)+KY2(J)+KZ2(K)) 
     &		   + REACTION(N)*TEMP1
              CTH(I,K,J,N)=CRTH(I,K,J,N)/TEMP6
            END DO
          END DO
        END DO
      END DO
C First step of the Fractional Step algorithm complete.


C Begin second step of the Fractional Step algorithm, making u divergence free.

C Compute varphi, store in the variable CR1, and project velocity field
      CALL REM_DIV_PER
C Then, update the pressure with phi.
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CP(I,K,J)=CP(I,K,J)+CR1(I,K,J)/TEMP4
          END DO
        END DO
      END DO
C Second step of the Fractional Step algorithm complete.

      RETURN
      END

C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      SUBROUTINE RK_PER_2
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
C Alternative time-stepping algorithm for the fully periodic case.
C INPUTS  (in Fourier space):  CUi, P, and (if k>1) CFi at (k-1)  (for i=1,2,3)
C OUTPUTS (in Fourier space):  CUi, P, and (if k<3) CFi at (k)
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|

      RETURN
      END

C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      SUBROUTINE REM_DIV_PER
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      INCLUDE 'header'
      INTEGER I,J,K
      REAL*8  TEMP5

C Compute phi, store in the variable CR1.
C Note the coefficient H_BAR is absorbed into phi.
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            TEMP5=-(KX2(I)+KY2(J)+KZ2(K)+EPS)
            CR1(I,K,J)=(CIKX(I)*CU1(I,K,J)+CIKY(J)*CU2(I,K,J)+
     *                  CIKZ(K)*CU3(I,K,J))/TEMP5
          END DO
        END DO
C Then update the CUi to make velocity field divergence-free.
        DO K=0,TNKZ
          DO I=0,NKX
            CU1(I,K,J)=CU1(I,K,J)-CIKX(I)*CR1(I,K,J)
            CU2(I,K,J)=CU2(I,K,J)-CIKY(J)*CR1(I,K,J)
            CU3(I,K,J)=CU3(I,K,J)-CIKZ(K)*CR1(I,K,J)
          END DO
        END DO
      END DO

      RETURN
      END

C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      SUBROUTINE POISSON_P_PER
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      INCLUDE 'header'
      INTEGER I,J,K

      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CR1(I,K,J)=CIKX(I)*CU1(I,K,J)
            CR2(I,K,J)=CIKZ(K)*CU3(I,K,J)
          END DO
        END DO
      END DO

      CALL FFT_XZY_TO_PHYSICAL(CR1,R1)
      CALL FFT_XZY_TO_PHYSICAL(CR2,R2)

      DO J=0,NYM
        DO K=0,NZM
          DO I=0,NXM
            P(I,K,J)=R1(I,K,J)*R1(I,K,J)+R1(I,K,J)*R2(I,K,J)+
     *               R2(I,K,J)*R2(I,K,J)
          END DO
        END DO
      END DO
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CF1(I,K,J)=CIKY(J)*CU1(I,K,J)
            CF2(I,K,J)=CIKX(I)*CU2(I,K,J)
            CF3(I,K,J)=CIKZ(K)*CU1(I,K,J)
            CR1(I,K,J)=CIKX(I)*CU3(I,K,J)
            CR2(I,K,J)=CIKZ(K)*CU2(I,K,J)
            CR3(I,K,J)=CIKY(J)*CU3(I,K,J)
          END DO
        END DO
      END DO
      CALL FFT_XZY_TO_PHYSICAL(CF1,F1)
      CALL FFT_XZY_TO_PHYSICAL(CF2,F2)
      CALL FFT_XZY_TO_PHYSICAL(CF3,F3)
      CALL FFT_XZY_TO_PHYSICAL(CR1,R1)
      CALL FFT_XZY_TO_PHYSICAL(CR2,R2)
      CALL FFT_XZY_TO_PHYSICAL(CR3,R3)
      DO J=0,NYM
        DO K=0,NZM
          DO I=0,NXM
            P(I,K,J)=2*(P(I,K,J)+F1(I,K,J)*F2(I,K,J)
     *                          +F3(I,K,J)*R1(I,K,J)
     *                          +R2(I,K,J)*R3(I,K,J))
          END DO
        END DO
      END DO

      CALL FFT_XZY_TO_FOURIER(P,CP)
      DO J=0,TNKY
        DO K=0,TNKZ
          DO I=0,NKX
            CP(I,K,J)=CP(I,K,J)/(KX2(I)+KY2(J)+KZ2(K)+EPS)
          END DO
        END DO
      END DO
      CP(0,0,0)=0.0

      RETURN
      END
	
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
	subroutine forcing(i,k,j)
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
	include 'header'
	integer i,j,k,n
	
	if (num_per_dir.eq.3) then
	  
C Translate periodic forcing to source location
      do n=1,N_TH
	  CFTH(i,k,j,n) = CFTH(i,k,j,n) - CS2(i,k,j) + CF(i,k,j) * 
     &		   exp(-ci*(KX(i)*pos(1)+KY(j)*pos(2)+KZ(k)*pos(3)))
      end do
	else
	  write(6,*) 'Non-periodic cases not supported'
	end if
	
	
	return
	end
	
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
	subroutine init_forcing
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
	include 'header'
	integer n,i,k,j
	
	if (num_per_dir.eq.3) then
        do n=1,n_th
	    do j=0,nym
	      do k=0,nzm
		  do i=0,nxm
		    S2(i,k,j) = D(i,k,j)*TH(i,k,j,n)
	        end do
	      end do
	    end do
	  end do
	  call fft_xzy_to_fourier(S2,CS2)
	else
	  write(6,*) 'Non-periodic cases not supported'
	end if
	
	
	return
	end
	
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
	subroutine gust(i,k,j)
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
	include 'header'
	integer i,j,k
	real*8 mag,zbqlnor,mu,var
	
	if (num_per_dir.eq.3) then
	    mu = 0.
          var = 70.
	    mag = zbqlnor(mu,sqrt(var))
	    CF1(i,k,j) = CF1(i,k,j) + mag*KZ(k)*exp(-abs(KZ(k)))  
	else
	  write(6,*) 'Non-periodic cases not supported'
	end if
	
	
	return
	end
	
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
	subroutine rk_source
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
	include 'header'
	real*8 temp2, temp3, b,a,gammam,gammae
	real*8 zbqlnor,mu,mag, tpos(3)
	integer i
	
	gammam = 1
	gammae = 10.
	
	temp2 = beta_bar(rk_step)*h_bar(rk_step)
	temp3 = zeta_bar(rk_step)*h_bar(rk_step)
	mu = 0.
	
	  if (rk_step .gt. 1) then
	    do i=1,3
	      pos(i) = pos(i) + temp3*fpos(i)
	    end do
	    theta = theta + temp3*ftheta
	  end if
	  
	  tpos(1) = pos(1)-LX/2.
	  tpos(2) = pos(2)
	  tpos(3) = pos(3)-LZ/2.
	  if (tpos(1).ne.0) then
	    b = atan(tpos(3)/tpos(1))
	    if (tpos(1).lt.0) b = b+pi
	  else
	    b = tpos(3)/abs(tpos(3))*pi
	  end if
	  
	  
	  a = gammam*((max(abs(tpos(1)),abs(tpos(3)))
     &		   /(LX*width*0.95/2.))**gammae)

	  mag = a * (pi-mod(theta-b,2.*pi))
	  ftheta = zbqlnor(mu,sqrt(theta_var)) + mag
	  mag = zbqlnor(v0,sqrt(mag_var))
	  
	  fpos(1) = mag*cos(theta)
	  fpos(2) = 0.
	  fpos(3) = mag*sin(theta)
	  
	  do i=1,3
	    pos(i) = pos(i) + temp2*fpos(i)
	  end do
        theta = theta + temp2*ftheta	  

	
	
	return
	end
	
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      SUBROUTINE INIT_TRUCK
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|

      INCLUDE 'header'
	real version, current_version
	real*8 a,b,px,py,pz,peak,slope,rad
	integer i,j,k,norm


	open(12,file='input_truck.dat',form='formatted',status='old')  
	
	current_version = 1.0
	read(12,*)
	read(12,*)
	read(12,*)
	read(12,*)
	read(12,*) version
	if (version.ne.current_version) stop 'Wrong input data format.'
	read(12,*)
	read(12,*) width
	read(12,*)
	read(12,*) a, b
	read(12,*)
	read(12,*) mag_var, theta_var
	read(12,*)
	read(12,*) px, py, pz, v0, theta
	
	close(12)
      
	
C Initialize forcing
	pos(1) = px+.0000001
	pos(2) = py
	pos(3) = pz+.0000001
	do j=0,NY
	  do k=0,NZ
	    do i=0,NX
	      F(i,k,j) = b*exp(-a*((gx(i)-LX/2.)**2+(gy(j)-LY/2.)**2
     &	                    +(gz(k)-LZ/2.)**2))
	    end do
	  end do
	end do
	
	call fft_xzy_to_fourier(F,CF)
	
	do j=0,tnky
	  do k=0,tnkz
	    do i=0,nkx
	      CF(i,k,j) = CF(i,k,j) * 
     &		   exp(ci*(KX(i)*LX/2+KY(j)*LY/2.+KZ(k)*LZ/2))
	    end do
	  end do
	end do
C Initialize perimeter damping
      peak = 10
	slope = 30
	norm = 10
      do j=0,NY
	  do k=0,NZ
	    do i=0,NX
	     rad = ((GX(i)-LX/2.)**norm + (GZ(k)-LZ/2.)**norm)**(1./norm)
	      D(i,k,j) = peak*(1./pi*atan(slope*(rad-LX*0.45))+0.5)
	    end do
	  end do
	end do


      RETURN
      END



C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      SUBROUTINE CREATE_FLOW_PER
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      INCLUDE 'header'
      INTEGER I, J, K
      REAL*8 RNUM1,RNUM2,RNUM3
      REAL*8 K0

C For an initial vortex, define the location of the centerline
      REAL*8 XC(0:NY+1),ZC(0:NY+1)

      IF ((FLAVOR .EQ. 'Basic').OR.(FLAVOR.eq.'CHEMOTAXIS')) THEN
	
      WRITE(6,*) 'Creating new flow from scratch.'

C Initialize random number generator
      CALL RANDOM_SEED

      IF (IC_TYPE.eq.0) THEN
C Initizlize the flow using a Taylor-Green vortex
C Nondimensionalize with U0 and 1/kappa
      DO J=0,NYM
        DO K=0,NZM
          DO I=0,NXM
! Add a random phase
!            U1(I,K,J)=cos(2*pi*(GY(J))/LY)
!     &               *cos(2*pi*(GX(I))/LX)
!     &               *SIN(2*pi*(GZ(K))/LZ)
!            U2(I,K,J)=0.d0
!            U3(I,K,J)=-cos(2*pi*(GY(J))/LY)
!     &               *sin(2*pi*(GX(I))/LX)
!     &               *COS(2*pi*(GZ(K))/LZ)
            U1(I,K,J)=0.d0
            U2(I,K,J)=0.d0
            U3(I,K,J)=0.d0
          END DO
        END DO
      END DO
      ELSE IF (IC_TYPE.eq.1) THEN
C Start with an ideal vortex centered in the domain
      DO J=0,NYM
        XC(J)=LX/2.+(LX/10.)*sin(2*PI*GY(J)/LY)
!        XC(J)=LX/2.
        ZC(J)=LZ/2.
        DO K=0,NZM
          DO I=0,NXM
            IF ((GX(I)-XC(j))**2.+(GZ(K)-ZC(j))**2..gt.0.1) then
! If we aren't too close to the vortex center
              U1(I,K,J)=-1.d0*(GZ(K)-ZC(j))
     &                /((GX(I)-XC(j))**2.+(GZ(K)-ZC(j))**2.)
              U3(I,K,J)=1.d0*(GX(I)-XC(j))
     &                /((GX(I)-XC(j))**2.+(GZ(K)-ZC(j))**2.)
              U2(I,K,J)=0.d0
            ELSE
! Otherwise:
              U1(I,K,J)=-1.d0*(GZ(K)-ZC(j))
     &                /0.1
              U3(I,K,J)=1.d0*(GX(I)-XC(j))
     &                /0.1
              U2(I,K,J)=0.d0
            END IF 
            CALL RANDOM_NUMBER(RNUM1)
            CALL RANDOM_NUMBER(RNUM2)
            CALL RANDOM_NUMBER(RNUM3)
            U1(I,K,J)=U1(I,K,J)+(RNUM1-0.5)*KICK
            U2(I,K,J)=U2(I,K,J)+(RNUM1-0.5)*KICK
            U3(I,K,J)=U3(I,K,J)+(RNUM1-0.5)*KICK
          END DO
        END DO
      END DO
      ELSE
        WRITE(*,*) 'Warning, Undefined Initial conditions in periodic.f'
      END IF

      CALL FFT_XZY_TO_FOURIER(U1,CU1)
      CALL FFT_XZY_TO_FOURIER(U2,CU2)
      CALL FFT_XZY_TO_FOURIER(U3,CU3)
      DO J=1,TNKY
        DO K=1,TNKZ
          DO I=1,NKX
            CALL RANDOM_NUMBER(RNUM1)
            CALL RANDOM_NUMBER(RNUM2)
            CALL RANDOM_NUMBER(RNUM3)
            K0=sqrt(KX(I)**2.d0+KY(J)**2.d0+KZ(K)**2.d0)
     &        /sqrt(KX(1)**2.d0+KY(1)**2.d0+KZ(1)**2.d0)
            CU1(I,K,J)=CU1(I,K,J)+(RNUM1-0.5)*KICK/K0
            CU2(I,K,J)=CU2(I,K,J)+(RNUM1-0.5)*KICK/K0
            CU3(I,K,J)=CU3(I,K,J)+(RNUM1-0.5)*KICK/K0
          end do
        end do
      end do
	
	ELSE IF(FLAVOR .EQ. 'Ensemble') THEN
	
	CALL CREATE_FLOW_ENSEM
	
	ELSE
	write(6,*) 'Unknown flavor, flow-field not created'
	
	endif

! get the initial energy in low wavenumbers
      CALL FFT_XZY_TO_PHYSICAL(CU1,U1)
      CALL FFT_XZY_TO_PHYSICAL(CU2,U2)
      CALL FFT_XZY_TO_PHYSICAL(CU3,U3)
      EK0=0.d0
      DO J=0,NYM
        DO K=0,NZM
          DO I=0,NXM
              EK0=EK0+U1(I,K,J)**2.d0+U2(I,K,J)**2.d0+U3(I,K,J)**2.d0
          END DO
        END DO
      END DO
      write(*,*) 'EK0: ',EK0
      IF (N_TH.gt.0) THEN
!      EPSILON_TARGET=((1.d0/DX(1))**4.d0)*(NU**3.d0)*(PR(1))**(-2.d0)
      EPSILON_TARGET=((1.d0/DX(1))**4.d0)*(NU**3.d0)*(100.d0)**(-2.d0)
      write(*,*) 'EPSILON_TARGET: ',EPSILON_TARGET
      write(*,*) 'TARGET KOLMOGOROV SCALE: ',
     &         (NU**3.d0/epsilon_target)**(0.25d0)
      END IF
      CALL FFT_XZY_TO_FOURIER(U1,CU1)
      CALL FFT_XZY_TO_FOURIER(U2,CU2)
      CALL FFT_XZY_TO_FOURIER(U3,CU3)


      CALL REM_DIV_PER
      CALL POISSON_P_PER

      CALL SAVE_STATS_PER(.FALSE.)

      RETURN
      END

C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      SUBROUTINE CREATE_TH_PER
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      INCLUDE 'header'
      INTEGER I,J,K,N

C Note, Since stratification is not permitted in the periodic flow field
C Any background stratification must be added to the governing equations

      DO N=1,N_TH
      IF (CREATE_NEW_TH(N)) THEN
        DO J=0,NYM
          DO K=0,NZM
            DO I=0,NXM
              IF (N.eq.1) THEN
C Nutrient concentration will be centered in the domain
         TH(I,K,J,N)=EXP(-((GX(I)-LX/2)*10.d0)**2.d0
     &                   -((GY(J)-LY/2)*10.d0)**2.d0
     &                   -((GZ(K)-LZ/2)*10.d0)**2.d0)
              ELSE
C Bacterial concentration will be offset
!       TH(I,K,J,N)=EXP(-((GX(I)-LX*3.d0/8.d0)/0.5d0)**2.d0*(2*PI)**2.d0
!     &                 -((GY(J)-LY/2.d0)/0.5d0)**2.d0*(2*PI)**2.d0
!     &                 -((GZ(K)-LZ/2.d0)/0.5d0)**2.d0*(2*PI)**2.d0)
! Uniform initial distribution
        TH(I,K,J,N)=1.d0

              END IF
            END DO
          END DO
        END DO
       CALL FFT_XZY_TO_FOURIER(TH(0,0,0,N),CTH(0,0,0,N)) 
      
       END IF
       END DO

       RETURN
       END
      

C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      SUBROUTINE CREATE_GRID_PER
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      INCLUDE 'header'
      INTEGER I,J,K

         IF (FLAVOR.NE.'Ensemble') WRITE (6,*) 'Fourier in X'
         DO I=0,NX
           GX(I)=(I*LX)/NX
           DX(I)=LX/NX
           IF (VERBOSITY .GT. 3) WRITE(6,*) 'GX(',I,') = ',GX(I)
         END DO
         IF (FLAVOR.NE.'Ensemble') WRITE (6,*) 'Fourier in Z'
         DO K=0,NZ
           GZ(K)=(K*LZ)/NZ
           DZ(K)=LZ/NZ
           IF (VERBOSITY .GT. 3) WRITE(6,*) 'GZ(',K,') = ',GZ(K)
         END DO
         IF (FLAVOR.NE.'Ensemble') WRITE (6,*) 'Fourier in Y'
         DO J=0,NY
           GY(J)=(J*LY)/NY
           DY(J)=LY/NY
           IF (VERBOSITY .GT. 3) WRITE(6,*) 'GY(',J,') = ',GY(J)
         END DO

         RETURN
         END


C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      SUBROUTINE INPUT_PER
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      INCLUDE 'header'
      REAL    VERSION, CURRENT_VERSION
      INTEGER N

! Read in input parameters specific for channel flow case
      OPEN (11,file='input_per.dat',form='formatted',status='old')
C Read input file.

      CURRENT_VERSION=1.0
      READ(11,*)
      READ(11,*)
      READ(11,*)
      READ(11,*)
      READ(11,*) VERSION
      IF (VERSION .NE. CURRENT_VERSION)
     &         STOP 'Wrong input data format input_chan'
      READ(11,*)
      READ(11,*) TIME_AD_METH
      READ(11,*)
      READ(11,*) LES_MODEL_TYPE
      READ(11,*)
      READ(11,*) IC_TYPE, KICK
      READ(11,*)
      READ(11,*) TRUCK, GUSTS
      DO N=1,N_TH
        READ(11,*)
        READ(11,*) BACKGROUND_GRAD(N)
        READ(11,*)
        READ(11,*) CHI(N)
      END DO

      RETURN
      END



C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      SUBROUTINE VIS_FLOW_PER
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|

      RETURN
      END

C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      SUBROUTINE SAVE_STATS_PER(FINAL)
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      INCLUDE 'header'
      CHARACTER*35 FNAME
      LOGICAL FINAL
          real*8 mean_u1(0:NYM)
          real*8 mean_u2(0:NYM)
          real*8 mean_u3(0:NYM)
          real*8 mean_th(0:NYM,1:N_TH)
          real*8 mean_p(0:NYM)


      integer i,j,k,n
      real*8 uc
	
	IF ((FLAVOR.EQ.'Basic').or.(FLAVOR.eq.'CHEMOTAXIS')) THEN

! Note that this routine uses CFi and CFTH for storage, so it should
! only be used between full R-K timesteps

      WRITE(6,*) 'Saving flow statistics.'

      if (FINAL) then
! We are done with the simulation
! Write out statistics to a file
        open(20,file='stats.txt',form='formatted',status='unknown')
        do j=0,NYM
          write(20,201) j,GYF(j),UBAR(j),VBAR(j),WBAR(j)
        end do
201     format(I3,',',F16.9,',',F16.9,',',F16.9,',',F16.9)
        do n=1,N_TH
        do j=0,NYM
          write(20,202) j,GYF(j),THBAR(j,n)
        end do
        end do
202     format(I3,',',F16.9,',',F16.9)

        else
! We are in the middle of a run, compile statistics

! Store the velocity in Fourier space in CRi
      do j=0,TNKY
        do k=0,TNKZ
          do i=0,NKX
            CR1(i,k,j)=CU1(i,k,j)
            CR2(i,k,j)=CU2(i,k,j)
            CR3(i,k,j)=CU3(i,k,j)
          end do
        end do
      end do

! Compute the vertical gradients in fourier space, store in CFi
      do j=0,TNKY
        do k=0,TNKZ
          do i=0,NKX
            CF1(i,k,j)=CIKY(j)*CU1(i,k,j)
            CF2(i,k,j)=CIKY(j)*CU2(i,k,j)
            CF3(i,k,j)=CIKY(j)*CU3(i,k,j)
          end do
        end do
      end do
! Save the  scalar in fourier space in CFTH
      do n=1,N_TH
        do j=0,TNKY
          do k=0,TNKZ
            do i=0,NKX
              CFTH(i,k,j,n)=CTH(i,k,j,n)
            end do
          end do
        end do
      end do

! Now, convert the velocity and vertical gradients to physical space
      CALL FFT_XZY_TO_PHYSICAL(CU1,U1)
      CALL FFT_XZY_TO_PHYSICAL(CU2,U2)
      CALL FFT_XZY_TO_PHYSICAL(CU3,U3)
      do n=1,N_TH
        CALL FFT_XZY_TO_PHYSICAL(CTH(0,0,0,n),TH(0,0,0,n))
      end do
      CALL FFT_XZY_TO_PHYSICAL(CF1,F1)
      CALL FFT_XZY_TO_PHYSICAL(CF2,F2)
      CALL FFT_XZY_TO_PHYSICAL(CF3,F3)

! First get the number of samples taken so far
      NSAMPLES=NSAMPLES+1
! Get the mean velocity

! First, get the mean in physical space
      do j=0,NYM
        mean_u1(j)=0.d0
        mean_u2(j)=0.d0
        mean_u3(j)=0.d0
        do n=1,N_TH
          mean_th(j,n)=0.d0
        end do
        mean_p(j)=0.d0
        do i=0,NXM
          do k=0,NZM
            mean_u1(j)=mean_u1(j)+U1(i,k,j)
            mean_u2(j)=mean_u2(j)+U2(i,k,j)
            mean_u3(j)=mean_u3(j)+U3(i,k,j)
            do n=1,N_TH
              mean_th(j,n)=mean_th(j,n)+TH(i,k,j,n)
            end do
            mean_p(j)=mean_p(j)+P(i,k,j)
          end do
        end do
        mean_u1(j)=mean_u1(j)/dble(NX*NZ)
        mean_u2(j)=mean_u2(j)/dble(NX*NZ)
        mean_u3(j)=mean_u3(j)/dble(NX*NZ)
         do n=1,N_TH
          mean_th(j,n)=mean_th(j,n)/dble(NX*NZ)
        end do
        mean_p(j)=mean_p(j)/dble(NX*NZ)
      end do
     
      do j=0,NYM
        UBAR(j)=(1./float(NSAMPLES))*mean_u1(j)
     &      +((float(NSAMPLES)-1.)/float(NSAMPLES))*UBAR(j)
        VBAR(j)=(1./float(NSAMPLES))*mean_u2(j)
     &      +((float(NSAMPLES)-1.)/float(NSAMPLES))*VBAR(j)
        WBAR(j)=(1./float(NSAMPLES))*mean_u3(J)
     &      +((float(NSAMPLES)-1.)/float(NSAMPLES))*WBAR(j)
        do n=1,N_TH
          THBAR(j,n)=(1./float(NSAMPLES))*mean_th(j,n)
     &      +((float(NSAMPLES)-1.)/float(NSAMPLES))*THBAR(j,n)
        end do
      end do

! Get the turbulent kinetic energy at each level 
      do j=0,NYM
        urms(j)=0.
        vrms(j)=0.
        wrms(j)=0.
      do k=0,NZM
      do i=0,NXM 
        urms(j)=urms(j)+(abs(U1(i,k,j)-mean_u1(j)))**2.
        vrms(j)=vrms(j)+(abs(U2(i,k,j)-mean_u2(j)))**2.
        wrms(j)=wrms(j)+(abs(U3(i,k,j)-mean_u3(j)))**2.
      end do
      end do
        urms(j)=sqrt(urms(j)/(float(NZ)*float(NX)))
        vrms(j)=sqrt(vrms(j)/(float(NZ)*float(NX)))
        wrms(j)=sqrt(wrms(j)/(float(NZ)*float(NX)))
      end do 
! Get the bulk rms value
      urms_b=0.
      do j=1,NYM
        urms_b=urms_b+0.5*(urms(j)+urms(j-1))*(GY(j)-GY(j-1))
      end do
      urms_b=urms_b/LY
! If we are in 2d:
      if (NY.eq.1) urms_b=urms(0)
       
! Compute the Reynolds stress and mean velocity gradient
      do j=0,NYM
        uv(j)=0. 
        uw(j)=0.
        wv(j)=0.
      do k=0,NZM
      do i=0,NXM
        uv(j)=uv(j)+(U1(i,k,j)-mean_u1(j))
     +    *(U2(i,k,j)-mean_u2(j))
        wv(j)=wv(j)+(U3(i,k,j)-mean_u3(j))
     +    *(U2(i,k,j)-mean_u2(j))
        uw(j)=uw(j)+(U1(i,k,j)-mean_u1(j))
     +    *(U3(i,k,j)-mean_u3(j))
      end do
      end do
        uv(j)=uv(j)/(float(NZ)*float(NX))
        uw(j)=uw(j)/(float(NZ)*float(NX))
        wv(j)=wv(j)/(float(NZ)*float(NX))
      end do
              
! Get the y-derivative of the mean velocity at GYF points
      do j=1,NY-2
        dudy(j)=(mean_u1(j+1)-mean_u1(j-1))/(GY(j+1)-GY(j-1))
        dwdy(j)=(mean_u3(j+1)-mean_u3(j-1))/(GY(j+1)-GY(j-1))
      end do
      j=0
        dudy(j)=(mean_u1(j+1)-mean_u1(NYM))/(2.d0*(GY(j+1)-GY(j)))
        dwdy(j)=(mean_u3(j+1)-mean_u3(NYM))/(2.d0*(GY(j+1)-GY(j)))
      j=NYM
        dudy(j)=(mean_u1(0)-mean_u1(j-1))/(2.d0*(GY(j)-GY(j-1)))
        dwdy(j)=(mean_u3(0)-mean_u3(j-1))/(2.d0*(GY(j)-GY(j-1)))

! Get the mean square shear
      do j=0,NYM
        shear(j)=0.d0
        do k=0,NZM
          do i=0,NXM
            shear(j)=shear(j)+F1(i,k,j)**2.d0+F3(i,k,j)**2.d0
          end do
        end do
        shear(j)=shear(j)/dble(NX*NZ)
      end do

! Write out the bulk rms velocity
      write(*,*) '<U_rms>: ',urms_b
      write(*,*) 'VERBOSITY: ',VERBOSITY

! Write out the mean statistics at each time
      open(40,file='mean.txt',form='formatted',status='unknown')
      write(40,*) TIME_STEP,TIME,DELTA_T,UBULK
      do j=0,NYM
        write(40,401) j,GY(J),mean_u1(j)
     +      ,mean_u2(j)
     +      ,mean_u3(j),urms(j),vrms(j),wrms(j)
     +      ,uv(j),uw(j),wv(j),dudy(j),dwdy(j),mean_p(j),shear(j)
      end do

401   format(I3,' ',14(F20.9,' '))



      if (MOVIE) then
! Output a 2d slice through the velocity field for animation in matlab
        open(79,file='movie_vel_xz.txt',status='unknown'
     &      ,form='formatted')
        do i=0,NXM
        do k=0,NZM
          write(79,*) U1(I,K,NY/2)**2.+U3(I,K,NY/2)**2.+U2(I,K,NY/2)**2.
        end do
        end do

        open(80,file='movie_vel_xy.txt',status='unknown'
     &        ,form='formatted')
        do i=0,NXM
        do j=0,NYM
          write(80,*) U1(I,NZ/2,J)**2.+U3(I,NZ/2,J)**2.+U2(I,NZ/2,J)**2.
        end do
        end do

! This file will contain a single plane and is used in conjunction with
! the matlab script 'realtime_movie' to visualize data during
! simulation
        open (76,file='temp.txt',status='unknown',form='formatted')
        do K=0,NZM
          write(76,*) gz(k)
        end do
        do I=0,NXM
        do K=0,NZM
          write(76,*) U1(I,K,NY/4)**2.+U3(I,K,NY/4)**2.+U2(I,K,NY/4)**2.
        end do
        end do
        close (76)
        CALL SYSTEM('mv temp.txt ../post_process/matlab/latest_slice.txt
     &')

      end if 

! Do over the number of passive scalars
      do n=1,N_TH

      do j=0,NYM
        thrms(j,n)=0.
      do k=0,NZM
      do i=0,NXM
        thrms(j,n)=thrms(j,n)+(abs(TH(i,k,j,n)-mean_th(j,n)))**2.
      end do
      end do
        thrms(j,n)=sqrt(thrms(j,n)/(float(NZ)*float(NX)))
      end do
! Compute the Reynolds stress and mean velocity gradient
      do j=0,NYM
        thv(j,n)=0.
      do k=0,NZM
      do i=0,NXM
       thv(j,n)=thv(j,n)+(TH(i,k,j,n)-mean_th(j,n))
     +    *(U2(i,k,n)-mean_u2(j))
      end do
      end do
      thv(j,n)=thv(j,n)/(float(NZ)*float(NX))
      end do

! Get the y-derivative of the mean velocity at GYF points
      do j=2,NY
        dthdy(j,n)=(CRTH(0,0,j+1,n)-CRTH(0,0,j-1,n))/(2.*DYF(j))
      end do
      do j=1,NY-2
        dthdy(j,n)=(mean_th(j+1,n)-mean_th(j-1,n))/(GY(j+1)-GY(j-1))
      end do
      j=0
       dthdy(j,n)=(mean_th(j+1,n)-mean_th(NYM,n))/(2.d0*(GY(j+1)-GY(j)))
      j=NYM
       dthdy(j,n)=(mean_th(0,n)-mean_th(j-1,n))/(2.d0*(GY(j)-GY(j-1)))

! Get the bacterial/nutrient correlation
      thth(n)=0.d0
      thvar(n)=0.d0     
      do j=0,NYM
      do k=0,NZM
      do i=0,NXM
        thth(n)=thth(n)+TH(i,k,j,n)*TH(i,k,j,1)*DX(I)*DY(J)*DZ(K)
        thvar(n)=thvar(n)+TH(i,k,j,n)*TH(i,k,j,n)*DX(I)*DY(J)*DZ(K)
      end do
      end do
      end do
      thth(n)=thth(n)
      thvar(n)=thvar(n)

      write(*,*) 'n,thth(n),thvar(n): ',n,thth(n),thvar(n)

      if (MOVIE) then
! Output a 2d slice through the scalar field for animation in matlab
        if (n.eq.1) then
! Chose which scalar is to be outputted
        open(75,file='movie1_xy.txt',status='unknown',form='formatted')
        do i=0,NXM
        do j=0,NYM
          write(75,*) TH(I,NZ/2,J,n)
        end do
        end do
        open(77,file='movie1_xz.txt',status='unknown',form='formatted')
        do i=0,NXM
        do k=0,NZM
          write(77,*) TH(I,K,NY/2,n)
        end do
        end do
        end if
        if (n.eq.2) then
! Chose which scalar is to be outputted
        open(85,file='movie2_xy.txt',status='unknown',form='formatted')
         
        do i=0,NXM
        do j=0,NYM
          write(85,*) TH(I,NZ/2,J,n)
        end do
        end do
        open(87,file='movie2_xz.txt',status='unknown',form='formatted')
        do i=0,NXM
        do k=0,NZM
          write(87,*) TH(I,K,NY/2,n)
        end do
        end do
        end if
        if (n.eq.3) then
! Chose which scalar is to be outputted
        open(95,file='movie3_xy.txt',status='unknown',form='formatted')
        do i=0,NXM
        do j=0,NYM
          write(95,*) TH(I,NZ/2,J,n)
        end do
        end do
        open(97,file='movie3_xz.txt',status='unknown',form='formatted')
        do i=0,NXM
        do k=0,NZM
          write(97,*) TH(I,K,NY/2,n)
        end do
        end do
        end if
      end if 


! End do over number of passive scalars, n
      end do

! Convert back to Fourier space
      do n=1,N_TH
        call fft_xzy_to_fourier(TH(0,0,0,n),CTH(0,0,0,n))
      end do

! Write out the mean statistics at each time
      open(41,file='mean_th.txt',form='formatted',status='unknown')
      write(41,*) TIME_STEP,TIME,DELTA_T,UBULK
      do n=1,N_TH 
      do j=0,NYM
        write(41,402) j,GYF(J),mean_th(j,n)
     &      ,dthdy(j,n),thrms(j,n),thv(j,n),pe_diss(j,n)
     &      ,thth(n),thvar(n)
      end do
      end do

402   format(I3,' ',8(F30.25,' '))

      write(*,*) 'VERBOSITY: ',VERBOSITY
      if (VERBOSITY.gt.4) then 
      write(*,*) 'Outputting info for gnuplot...'
      open (unit=10, file="solution")
      do i=2,NXM
        do j=2,NYM
          write (10,*) i, j, U1(i,0,j)
        end do
        write (10,*) ""
      end do
      close (10)
      call system ('gnuplot <gnuplot.in') 
      end if

C Convert velocity back to Fourier space
      call fft_xzy_to_fourier(U1,CU1)
      call fft_xzy_to_fourier(U2,CU2)
      call fft_xzy_to_fourier(U3,CU3)

      end if

 
      call tkebudget_per
	
	ELSE IF(FLAVOR.EQ.'Ensemble') THEN
	
	call save_stats_ensem(final)
	
	END IF


      RETURN
      END

C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      subroutine filter_per
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
C This subroutine applies a filter to the highest wavenumbers
C It should be applied to the scalars in Fourier space
C The filter used is a sharpened raised cosine filter

      include 'header'

      integer I,J,K,N

! Variables for horizontal filtering
      real*8 sigma0

C Set the filtering constants for the all directions
      DO N=1,N_TH
      DO i=0,NKX
       DO k=0,TNKZ
        DO j=0,TNKY
          sigma0=0.5d0*(1.d0+
     &       cos(sqrt((KX(i)*LX*1.d0/float(NX))**2.d0
     &            +(KZ(k)*LZ*1.d0/float(NZ))**2.d0
     &            +(KY(j)*LY*1.d0/float(NY))**2.d0)))
! Apply a sharpened raised cosine filter
          CTH(i,k,j,n)=CTH(i,k,j,n)*
     &          sigma0**4.d0*(35.d0-84.d0*sigma0
     &        +70.d0*sigma0**2.d0-20.d0*sigma0**3.d0)
        END DO
       END DO
      END DO
      END DO

       return
       end

C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      subroutine tkebudget_per
C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
! NOte, it is important to only run this routine after complete R-K
!  time advancement since F1 is overwritten which is needed between R-K steps

      include 'header'

      integer i,j,k
      real*8 epsilon_mean,k_eta 

! Compute the turbulent dissipation rate, epsilon=nu*<du_i/dx_j du_i/dx_j>
      do j=0,NYM
        epsilon(j)=0.
      end do
! Store du/dx in CS1
      do j=0,TNKY
      do k=0,TNKZ
      do i=0,NKX
        CS1(i,k,j)=CIKX(i)*CU1(i,k,j)
      end do
      end do
      end do
! Convert to physical space
      call fft_xzy_to_physical(CS1,S1)
      do j=0,NYM
      do k=0,NZM
      do i=0,NXM
        epsilon(j)=epsilon(j)+(S1(i,k,j)**2.0)
      end do
      end do
      end do
! Store dv/dx in CS1
      do j=0,TNKY
      do k=0,TNKZ
      do i=0,NKX
        CS1(i,k,j)=CIKX(i)*CU2(i,k,j)
      end do
      end do
      end do
! Convert to physical space
      call fft_xzy_to_physical(CS1,S1)
      do j=0,NYM
      do k=0,NZM
      do i=0,NXM
        epsilon(j)=epsilon(j)+0.5*(S1(i,k,j)**2.0)
      end do
      end do
      end do
! Compute du/dy at GYF gridpoints, note remove mean
      do j=0,TNKY
      do k=0,TNKZ
      do i=0,NKX
        CF1(i,k,j)=CIKY(j)*CU1(i,k,j)
      end do
      end do
      end do
! Convert to physical space
      call fft_xzy_to_physical(CF1,F1)
      do j=0,NYM
      do k=0,NZM
      do i=0,NXM
        epsilon(j)=epsilon(j)+0.5*(F1(i,k,j)**2.0)
! Cross term dvdx*dudy
        epsilon(j)=epsilon(j)+(S1(i,k,j)*F1(i,k,j))
      end do
      end do
      end do
! Store dw/dx in CS1
      do j=0,TNKY
      do k=0,TNKZ
      do i=0,NKX
        CS1(i,k,j)=CIKX(i)*CU3(i,k,j)
      end do
      end do
      end do
! Convert to physical space
      call fft_xzy_to_physical(CS1,S1)
      do j=0,NYM
      do k=0,NZM
      do i=0,NXM
        epsilon(j)=epsilon(j)+0.5*(S1(i,k,j)**2.0)
      end do
      end do
      end do
! Compute du/dz at GYF gridpoints, note remove mean
! Store du/dz in CS1
      do j=0,TNKY
      do k=0,TNKZ
      do i=0,NKX
        CF1(i,k,j)=CIKZ(k)*CU1(i,k,j)
      end do
      end do
      end do
! Convert to physical space
      call fft_xzy_to_physical(CF1,F1)
      do j=0,NYM
      do k=0,NZM
      do i=0,NXM
        epsilon(j)=epsilon(j)+0.5*(F1(i,k,j)**2.0)
! Cross term dudz*dwdx
        epsilon(j)=epsilon(j)+S1(i,k,j)*F1(i,k,j)
      end do
      end do
      end do
! Compute dv/dy at GYF gridpoints, note remove mean
      do j=0,TNKY
      do k=0,TNKZ
      do i=0,NKX
        CS1(i,k,j)=CIKY(j)*CU2(i,k,j)
      end do
      end do
      end do
! Convert to physical space
      call fft_xzy_to_physical(CS1,S1)
      do j=0,NYM
      do k=0,NZM
      do i=0,NXM
        epsilon(j)=epsilon(j)+(S1(i,k,j)**2.0)
      end do
      end do
      end do
! Compute dw/dy at GYF gridpoints, note remove mean
      do j=0,TNKY
      do k=0,TNKZ
      do i=0,NKX
        CS1(i,k,j)=CIKY(j)*CU3(i,k,j)
      end do
      end do
      end do
! Convert to physical space 
      call fft_xzy_to_physical(CS1,S1)
      do j=0,NYM
      do k=0,NZM
      do i=0,NXM
        epsilon(j)=epsilon(j)+0.5*(S1(i,k,j)**2.0)
      end do
      end do
      end do
! Store dv/dz in CF1
      do j=0,TNKY
      do k=0,TNKZ
      do i=0,NKX
        CF1(i,k,j)=CIKZ(k)*CU2(i,k,j)
      end do
      end do
      end do
! Convert to physical space
      call fft_xzy_to_physical(CF1,F1)
      do j=0,NYM
      do k=0,NZM
      do i=0,NXM
        epsilon(j)=epsilon(j)+0.5*(F1(i,k,j)**2.0)
! Cross term dvdz*dwdy
        epsilon(j)=epsilon(j)+S1(i,k,j)*F1(i,k,j)
      end do
      end do
      end do
! Store dw/dz in CS1
      do j=0,TNKY
      do k=0,TNKZ
      do i=0,NKX
        CS1(i,k,j)=CIKZ(k)*CU3(i,k,j)
      end do
      end do
      end do
! Convert to physical space
      call fft_xzy_to_physical(CS1,S1)
      do j=0,NYM
      do k=0,NZM
      do i=0,NXM
        epsilon(j)=epsilon(j)+(S1(i,k,j)**2.0)
      end do
      end do
      end do
      do j=0,NYM
        epsilon(j)=epsilon(j)/float(NX*NZ)
      end do


! Write out the bulk rms velocity
      write(*,*) '<U_rms>: ',urms_b


! Write out the mean statistics at each time
      open(45,file='tke.txt',form='formatted',status='unknown')
      write(45,*) TIME_STEP,TIME,DELTA_T
      do j=0,NYM
        write(45,401) j,GY(J),epsilon(j)
      end do
401   format(I3,' ',2(F20.9,' '))


! Get Kolmogorov wavelength
      epsilon_mean=NU*SUM(epsilon(0:NYM))/dble(NY) 

      k_eta=2.d0*PI*(NU**3.d0/epsilon_mean)**(-0.25d0)

      write(*,*) 'Kolmogorov scale: ',2.d0*PI/k_eta

      return
      end






