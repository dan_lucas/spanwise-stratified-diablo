C******************************************************************************|

C----*|--.---------.---------.---------.---------.---------.---------.-|-------|
      SUBROUTINE TIMESTEP_DIABLO
      INCLUDE 'header'
      INTEGER N,IY,NSTEPS
      LOGICAL FLAG
 
C A flag to determine if we are considering the first time-step
c      call WALL_TIME(START_TIME)
c      call MPI_BARRIER(MPI_COMM_WORLD,IERROR)
      H_BAR(1)=DELTA_T*(8.0/15.0)
      H_BAR(2)=DELTA_T*(2.0/15.0)
      H_BAR(3)=DELTA_T*(5.0/15.0)

      if(rank.eq.0) print*, 'timestepping with NSTEPS=' 
     &                    , NSTEPS,DELTA_T,DELTA_T*NSTEPS

      DO TIME_STEP = 1, NSTEPS
        DO RK_STEP=1,3
          IF (NUM_PER_DIR.EQ.3) THEN
            IF (TIME_AD_METH.EQ.1) CALL RK_PER_1
            IF (TIME_AD_METH.EQ.2) CALL RK_PER_2            
          ELSEIF (NUM_PER_DIR.EQ.2) THEN
            IF (TIME_AD_METH.EQ.1) CALL RK_CHAN_1
          ELSEIF (NUM_PER_DIR.EQ.1) THEN
            IF (TIME_AD_METH.EQ.1) CALL RK_DUCT_1
            IF (TIME_AD_METH.EQ.2) CALL RK_DUCT_2            
          ELSEIF (NUM_PER_DIR.EQ.0) THEN
            IF (TIME_AD_METH.EQ.1) CALL RK_CAV_1
            IF (TIME_AD_METH.EQ.2) CALL RK_CAV_2            
          END IF
        END DO
        TIME=TIME+DELTA_T
        FIRST_TIME=.FALSE.
c$$$        IF(RI_TAU(1).lt. 0.5)THEN
c$$$           RI_TAU(1) = RI_START+(TIME-TSTART)*0.005
c$$$           if(rank.eq.0) write(77,*) TIME,RI_TAU(1)
c$$$        ENDIF
! save statistics to an output file
        IF (MOD(TIME_STEP,SAVE_STATS_INT).EQ.0) THEN
            CALL SAVE_STATS(.FALSE.)
        END IF
! Save the flow to a restart file
        IF (MOD(TIME_STEP,SAVE_FLOW_INT).EQ.0) THEN
          CALL SAVE_FLOW(.FALSE.)
        END IF
! Filter the scalar field
        DO N=1,N_TH
          IF (FILTER_TH(N)
     &       .AND.(MOD(TIME_STEP,FILTER_INT(N)).EQ.0)) THEN
             IF (RANK.EQ.0) 
     &            write(*,*) 'Filtering...'
             CALL FILTER(N)
          END IF 
       END DO
      END DO


      END


C******************************************************************************|

