""" For handling sets of continuous runs.

.. currentmodule:: diablo_pack.sim

Classes
----------

.. autoclass:: SimDiablo
   :members:


Functions
----------

.. autofunction:: readObj

.. autofunction:: writeObj

"""


import numpy as np
import os

from diablo_pack.run      import RunDiablo 

from diablo_pack.func_dia import GetResolution
from diablo_pack.func_dia import ave_one_side

import glob

def readObj(namefile):
    """ Import object written on a file using the pickle package.
    
    **Args:**
       namefile : str  
          Name of the file to import 
    
    **Returns:**
       object : obj   
          Object read in   

    *Usage:*
       object=readObj(namefile);
    """

    import pickle
    with open(namefile,'r') as fid:
        out=pickle.load(fid)
    return out

def writeObj(namefile,var):
    """ Write an object to a file using the pickle package.    

    **Args:**
       namefile : str  
          Name of the file to import 
       var  : obj  
          Object to be written

    **Returns:**
       int.   The return code::
          
          0 -- Success!
          1 -- Error in the process

    *Usage:*
       err=writeObj(namefile,var);
    """

    import pickle
    try:
        with open(namefile,'w') as fid:
            pickle.dump(var,fid)
        return 0
    except:
        return 1

class SimDiablo:
    """ Object to manage continuation runs

    The different runs (with statistics, input files and grid files) are stored in folders with a increasing number. As the object for the simulation is generated, the folders are automatically read.  


    **Attributes**
        directory : str
           path to the directory of the run
        my_list_runs : list, str
           list with the name of the folders (runs) of the simulation
        runs : list, RunDiablo
           list of RunDiablo object pertaining to single runs (available after read_runs) 

    *Usage*::

        s=diablo_pack.sim.SimDiablo()
        # To then create the s.runs list with the different runs
        s.read_runs()

    """

    def __init__(self):
        """ Initialize the object and detect the folders (individual runs) which are presents
        """
        self.directory=os.getcwd()

        self.my_list_runs=glob.glob('[0-9]*')
        self.my_list_runs.sort()
        
        try:
            self.my_list_runs.remove('0000')
        except:
            pass

        print ' '
        print ' PATH                 : ' + self.directory
        print ' Number of runs found : ' + str(len(self.my_list_runs))

    def read_runs(self,parfile='grid_def.all'):
        """Read the statistics of individual runs

        This function scans all the folder and create a :meth:`~diablo_pack.run.RunDiablo` object which is stored in a list called runs. 

        **Args:**
           parfile : str, opt
              Name of the grid including the full grid (it is good convention in parallel simulation to call grid_def the grid file used to compile and grid_def.all the grid file having in NY the full number of points

        """
        # self.stats=list()
        self.runs =list()
        for ith in self.my_list_runs:
            os.chdir(ith)
            iRun=RunDiablo(parfile=parfile)
            self.runs.append(iRun)
            os.chdir('..')

    def get_all_stats(self, filename='mean.txt', filename_th='mean_th.txt'):
        """ Read the statistics for each run

        This function scans over the runs and reads the velocity statistics (given in *filename*) and the scalar statistics (given in *filename_th*) for each run. A statistic object :class:`~diablo_pack.stats.StatsDiablo is created for each run. 
        
        **Args:**
            filename : str
               Name of the file for the velocity statistics
            filename_th : str
               Name of the file for the scalar statistics

        """

        myHome=os.getcwd()
        for ith in self.runs:
            os.chdir(ith.home)
            ith.read_stat(filename,filename_th)
            
        os.chdir(myHome)


    def get_lam_turb_stats(self):
        """ Averages the laminar and turbulent statistics among runs. This assumes that you have already generated the separate statistics for laminar and turbulent regions using :meth:`~diablo_pack.sim.SimDiablo.compute_laminar_turbulent_stats` for single runs.

        This method generates two StatsDiablo objects: 
           1. s.st_lam : with the statistics for laminar regions
           2. s.st_tur : with the statistics for turbulent regions
        """

        myHome=os.getcwd()
        for ith in self.runs:
            os.chdir(ith.home)
            ith.lam_turb_stat()
            
        self.st_lam=get_lam_obj(self)
        self.st_tur=get_tur_obj(self)
        os.chdir(myHome)


    def compute_laminar_turbulent_stats(self,iRuns=None,lim=7.5):
        """ Computes for each run the turbulent and laminar statistics from the single flow fields that are present (the method :meth:`~diablo_pack.stats.DiabloStats.LamTurbStats of the :class:`~diablo_pack.stats.StatsDiablo` object is called).

        """
        import intermittency
        
        if iRuns==None:
            Runs=self.runs;
        else:
            Runs=list();
            for ith in iRuns:
                Runs.append(self.runs[ith]);

        for ith in Runs:
            ith.lam_turb_stat()

    def calc_intermittency_in_time(self,iRuns=None,lim=7.5):
        """ Computes the intermittency for the runs.

        **Args:**
           iRuns : list, int
              List of runs (indices of my_list_runs) to be used for calculating the intermittency in time. *default* value is *None*, meaning that all runs will be used
           lim : double
              Threshold used for differentiating between laminar and turbulent regions

        *Notes*

        This method generates files within each run folder with the local rms of shear stresses (see paper Deusebio et al. 2015) which is used to compute laminar and turbulent by thresholding, and hence the intermittency. A file "intermittency.dat" is written on the disk in each folder giving the intermittency at the upper, at the lower wall and the average  

        +--------+-----------------------+-----------------------+--------------------+
        |  Time  |  Intermittency upper  |  Intermittency upper  |  Avg Intermittency |
        +========+=======================+=======================+====================+
        |   ...  |         ...           |          ...          |         ...        |      
        +--------+-----------------------+-----------------------+--------------------+

        """

        def compute_and_write_boxRms(obj,ith):
            import intermittency
            import ioFiles
            
            try:
                os.chdir(ith)
            except:
                return

            print ' Run: ' + ith
            for iname in ['out.h5','end.h5']:
                if os.path.exists(iname):
                    print '   File: ' + iname
                    obj.runs[obj.my_list_runs.index(ith)].read_flow(iname)
                    if not os.path.exists('boxrms_l.txt.' + iname):
                        flow=obj.runs[obj.my_list_runs.index(ith)].flow[iname]
                        time=flow.t;
                        boxVar_l,boxVar_u=obj.runs[obj.my_list_runs.index(ith)].flow[iname].get_laminar_turbulent_patch(iOut=iname)
                    else:
                        time=float(ith);
                        boxVar_l=ioFiles.readFileColumns('boxrms_l.txt.' + iname).Data[2][1:]
                        boxVar_u=ioFiles.readFileColumns('boxrms_u.txt.' + iname).Data[2][1:]

                    dig_l  = np.array((boxVar_l>lim)*1.)
                    dig_u  = np.array((boxVar_u>lim)*1.)

                    with open('../intermittency.dat','a') as fid:
                        gamma1=np.sum(dig_l)*1./np.size(dig_l)
                        gamma2=np.sum(dig_u)*1./np.size(dig_u)
                        fid.write( str(time) + ' \t ' + str(gamma1) + ' \t ' + str(gamma2) + ' \n')
            os.chdir('..')


        if os.path.exists('intermittency.dat'):
            # os.remove('intermittency.dat')
            return

        if iRuns==None:
            for ith in self.my_list_runs:
                compute_and_write_boxRms(self,ith)
        else:
            for ith in iRuns:
                compute_and_write_boxRms(self,self.my_list_runs[ith]) 

    def GetSpanwiseStatistics(self,iRuns=None):
        """ Get spanwise statistics from single flow fields (using :meth:`~diablo_pack.flow.FlowDiablo.SpanwiseStats`) over all the runs.
        """
        import ioFiles

        if iRuns==None:
            iRuns=np.arange(0,len(self.my_list_runs))
            for ith in iRuns:
                os.chdir(self.my_list_runs[ith])
                for iname in ['out.h5','end.h5']:
                    if os.path.exists(iname):
                        self.runs[ith].read_flow(iname);
                        self.runs[ith].flow[iname].SpanwiseStats();
                os.chdir('..')
        
    def compute_spectra_2d(self,yc,nVar1,nVar2,iRuns=None):
        """ Compute 2d spectra or co-spectra from single flow fields for each run and write that out to disk 
        
        **Args:**
           yc : double
                Vertical height at which the 2D spectra should be calculated
           nVar1 : str
                Name of the first variable (*e.g.* U,V,W,TH1,TH ..)
           nVar2 : str     
                Name of the second variable (*e.g.* U,V,W,TH1,TH ..)
           iRuns : list, int
              List of runs (indices of my_list_runs) to be used for calculating the intermittency in time. *default* value is *None*, meaning that all runs will be used

        *Usage:*::

           # Get the spectra of U
           r.compute_spectra_2d(0.5,'U','U')
           # Get the co-spectra U of V
           r.compute_spectra_2d(0.5,'U','V')

        """
        if iRuns==None:
            iRuns=np.arange(0,len(self.my_list_runs))
            
        for ith in iRuns:
            os.chdir(self.my_list_runs[ith])
            self.runs[ith].read_flow('end.h5');
            filename=nVar1 + nVar2 + "_" + "%.2e" % yc + '_xzspec.dat';
            self.runs[ith].flow['end.h5'].specxz(yc,nVar1,nVar2,fileut=filename,ri='real');
            os.chdir('..')
        

    def plotHistBoxVar(self,iRuns=None):
        """ Plot the histogram of the rms of the local shear stresses, using the runsin *iRuns* (see figure in Deusebio et al. 2015 for examples)
        """
        import ioFiles

        if iRuns==None:
            iRuns=np.arange(0,len(self.my_list_runs))
            
        cumSum=list()
        for ith in iRuns:
            os.chdir(self.my_list_runs[ith])
            for iname in ['out.h5','end.h5']:
                for nname in ['l','u']:
                    if os.path.exists('boxrms_' + nname + '.txt.' + iname):
                        cumSum=cumSum+list(ioFiles.readFileColumns('boxrms_' + nname + '.txt.' + iname).Data[2][1:])
            os.chdir('..')
        
        return cumSum

    def join_statistics(self,iRuns=None,update=False):
        """
        To be used for parallel runs. Scan throught the folders and merge the statistics files with :meth:`~diablo_pack.run.join_stats`.
        
        **Args:**
           iRuns : list
              List of indices of runs to be merged. The default scan through all the runs.
           update : bool
              Forces to recompute the merged statistics files, even if they already exist.
        """

        iList=[];
        if iRuns==None:
            iList=self.runs
        else:
            for ith in iRuns:
                iList.append(self.runs[ith])

        for ith in iList:
            ith.join_stats(update=update)

    
    def show_info(self):
        """ Print a summary of all the runs to the screen 
        
        *Format*

        # Run             
            Time
            Resolution

        """
        print 

        for ith in np.arange(len(self.runs)):
            ist =self.runs[ith].st
            print 'Run: '+ str(ith).zfill(3) 
            print '    Time      : ' + str(min(ist.t)) + ' - ' + str(max(ist.t)) 
            print '    Resolution: ' + str(self.runs[ith].Resolution)

    
    def globalStat(self,Runs=None):
        """ Average the statistics file from the runs and if intermittency.dat files are available it merges them all

        **Args:**
           iRuns : list, int
              List of runs (indices of my_list_runs) to be used for calculating the intermittency in time. *default* value is *None*, meaning that all runs will be used
        
        """

        import ioFiles as io

        self.st=get_statistics_obj(self,Runs)
        self.st.compute_quantities()
        if os.path.exists(self.directory + '/intermittency.dat'):
            DataIn=io.readFileColumns(self.directory + '/intermittency.dat')
            self.st.gamma={'t'      :DataIn.Data[0],
                           'gamma_l':DataIn.Data[1],
                           'gamma_u':DataIn.Data[2],
                           'gamma'  :0.5*(DataIn.Data[1]+DataIn.Data[2])}



def get_statistics_obj(sim,Runs=None):
    if Runs==None:
        Runs=np.arange(len(sim.runs))
    out=sim.runs[Runs[0]].st
    for ith in np.arange(1,len(Runs)):
        out=out + sim.runs[Runs[ith]].st
    return out


def get_lam_obj(sim,Runs=None):
    if Runs==None:
        Runs=np.arange(len(sim.runs))
    N=0;
    for ith in np.arange(1,len(Runs)):
        nth=sim.runs[Runs[ith]]
        if hasattr(nth,'st_lam'):
            if N==0:
                out=nth.st_lam
            else:
                out=out + nth.st_lam
            N=N+1;
    return out

def get_tur_obj(sim,Runs=None):
    if Runs==None:
        Runs=np.arange(len(sim.runs))
    N=0;
    for ith in np.arange(1,len(Runs)):
        nth=sim.runs[Runs[ith]]
        if hasattr(nth,'st_tur'):
            if N==0:
                out=nth.st_tur
            else:
                out=out + nth.st_tur
            N=N+1;
    return out


# def ave_one_side(yf,var,sym=1):
#     """ Returns an average of the two sides of the channel

#     **Args:**
#         yf : array, double
#             Y-Grid (which ought to be symmetric with respect to 0)
#         var : array, double
#             Quantity to be averaged
#         sym : int
#             Symmetry flag:
#                 * +1  -  Symmetric function
#                 * -1  -  Anti-symmetric function 
#     """
#     N=yf.shape[0]
#     yf =np.reshape(yf ,N)
#     var=np.reshape(var,N)
#     maxErr=1E-8
#     if any(abs(yf[:N/2+1]+np.flipud(yf[-N/2:]))>maxErr):
#         print " Grid not symmetric "
#         raise SystemExit(0)
#     else:
#         varUt=0.5*(var[:N/2+1] + sym*np.flipud(var[-N/2:]))
#         yfUt =1-abs(yf [:N/2+1]) 
#     return (yfUt,varUt)

def get_richardson(st):
    import mypy

    yf=st.yf

    ume=np.mean(st.Var['ume'],0)
    du =mypy.dfdy(yf,ume)

    thme=np.mean(st.Var['thme'],0)
    dth=mypy.dfdy(yf,thme)

    uv  =np.mean(st.Var['uv'],0)
    thv =np.mean(st.Var['thv'],0)

    P=-uv*du
    B=st.Ri*thv

    Rif=-B/P
    Rig=st.Ri*dth/du**2

    sel=(yf>-0.25) & (yf<0.25)

    Rig_c=np.mean(Rig[sel])
    Rif_c=np.mean(Rif[sel])

    print ' Bulk Ri:     ' + str(st.Ri)
    print ' Gradient Ri: ' + str(Rig_c)
    print ' Flux Ri:     ' + str(Rif_c)
