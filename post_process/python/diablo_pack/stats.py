""" Modules containing the classes to handle statistics in DIABLO.

.. currentmodule:: diablo_pack.stats

It includes the definition of two classes:
    
    1. :class:`~diablo_pack.stats.StatsDiablo` 
    2. :class:`~diablo_pack.stats.StatsRetDiablo` 


Classes
--------

.. autoclass:: StatsDiablo
   :members:

.. autoclass:: StatsRetDiablo
   :members:


"""


import os

import numpy as np
import mypy

from   diablo_pack.func_dia import *


class StatsDiablo:
    """ Object to handle the statistics of a DIABLO run 

    The object contains all the statistics in the Var dictionary. Here there are two kind of quantities:
        *  1D array, containing temporal (dims NSAMPLES) evolution, e.g. 'tau'
        *  2D array, containing temporal-y (dims NSAMPLESxNY) evolution, e.g. 'ume'

    **Attributes:**

        NSAMPLES : int
            Number of time samples
        NY : int
            Number of points in y
        N_TH : int 
            Number of scalars (tested with one scalar only so far)
        Re : double
           Bulk Reynolds number
        Ri : double
           Bulk Richardson number  
        Pr : double
           Prandtl number
        Var : dict, double 
           Contains all the statistics (for further information, see below)
        pVar : dict, double 
           One-sided averaged quantities (available after running the one_side_average method)
        t : array, double
           Array with the time instant
        yf : array, double 
           Array with the y-location of the frac grid
        yg : array, double 
           Array with the y-location of the full grid

        
    .. note:: Below we report a summary of the variables found in the Var and pVar dictionaries

            1D fields of the Var dictionary :

                * ubulk : bulk velocity 
                * dt : time step 
                * tau_t : top-wall shear-stress
                * tau_b : lower-wall shear-stress
                * tau : average shear-stress
                * utau : friction velocity (after running compute_quantities)
                * Ttau : friction temperature (after running compute_quantities)
                * L : Monin-Obukhov lengthscale (after running compute_quantities)
                
            2D fields of the Var dictionary :
            
                *  ume : mean streamwise velocity
                *  vme : mean vertical velocity 
                *  wme : mean spanwise velocity
                *  urms : rms of streamwise velocity 
                *  vrms : rms of vertical velocity 
                *  wrms : rms of spanwise velocity 
                *  uv : uv correlation 
                *  uw : uw correlation 
                *  vw : vw correlation 
                *  dudy : vertical gradient of streamwise velocity 
                *  dwdy : vertical gradient of spanwise velocity 
                *  cp : mean pressure 
                *  shear : dudy**2 + dwdy**2
                *  omx : streamwise vorticity 
                *  omy : vertical vorticity 
                *  omz : spanwise vorticity 
                *  epst : turbulent kinetic energy dissipation 
                *  epsm : mean kinetic energy dissipation
                
            1D Fiels of pVar
                
                *  yf : grid (Dimension NY)
                *  uv : uv correlation 
                *  ume : mean streamwise velocity 
                *  du : vertical derivative of streamwise velocity 
                *  thme : mean temperature
                *  dth : mean temperature gradient 
                *  P : shear production term 
                *  vv : vv autocorrelation 
                *  thv : temperature-v correlation 
                *  G : conversion term (kinetic to buoyancy) 
                *  eps : turbulent kinetic energy dissipation 
                *  eta : Kolmogorov lengthscale 
                *  utau : friction velocity 
                *  Ttau : friction temperature 
                *  L : Monin-Obukhov lengthscale

    """

    def __init__(self,filename='mean.txt',filename_th='mean_th.txt',NY=None,N_TH=None,grid_def='grid_def',Re=0,Ri=0,Pr=1):
        """ Initialize a Stats object.   

        **Args:**
           filename : str
               Name of the file for the velocity statistics
           filename_th : str
               Name of the file for the scalar statistics
           NY : int, opt
               Number of points along Y (if nothing is provided, it is read from the grid_def file).
           N_TH : int, opt
               Number of scalars (if nothing is provided, it is read from the grid_def file).
           grid_def : str
               Name of the grid_def file where to take NY and N_TH
           Re : double, opt
               Reynolds number 
           Ri : double, opt 
               Richardson number 
           Pr : double, opt 
               Prandtl number 

        """

        filename_tke='tke.txt';

        if NY is None:
            NY=GetResolution('NY',grid_def)
        self.NY=NY

        if N_TH is None:
            N_TH=GetResolution('N_TH',grid_def)
        self.N_TH=N_TH

        self.Re=Re
        self.Ri=Ri
        self.Pr=Pr

        if (not os.path.exists(filename)):
            print(  filename + " do not exist in the current folder.")
            raise SystemError(1)            
        if (not os.path.exists(filename_th) and N_TH>0):
            print(  filename_th + " do not exist in the current folder.")
            raise SystemError(1)            

        # Number of line in the files
        NL=sum(1 for line in open(filename))
        self.NSAMPLES=NL/(NY+2)

        self.yf=np.zeros([self.NY])
        self.t=np.zeros([self.NSAMPLES])

        self.Var={}
        myList=["ubulk","dt","tau_t","tau_b","tau"]
        for ith in myList:
            self.Var[ith]=np.zeros([self.NSAMPLES])
   
        myList=["ume","vme","wme","urms","vrms","wrms","uv","uw","vw","dudy","dwdy","cp","shear","omx","omy","omz","epst","epsm"]
        if (N_TH>0) :
            myList.extend(["thme","dthdy","thrms","thv","epsp","thu","thw"])
        for ith in myList:
            self.Var[ith] =np.zeros([self.NSAMPLES,self.NY])

        # Opening files
        ifid   =open(filename   ,'r')
        if (N_TH>0) :
            ifid_th=open(filename_th,'r')
        # ifidtke=open(filename_tke   ,'r')
  
        for iSamp in np.arange(self.NSAMPLES):
            DataIn=ifid.readline()
            self.t[iSamp]=np.double(DataIn.split()[1].replace(',',''))
            self.Var["dt"][iSamp]=np.double(DataIn.split()[2])
            DataIn=ifid.readline()
            self.Var["ubulk"][iSamp]=np.double(DataIn.split()[0])
            if (N_TH>0) :
                DataIn=ifid_th.readline()
                DataIn=ifid_th.readline()
            
            # DataIn=ifidtke.readline()
            # DataIn=ifidtke.readline()

            # Write the different lines
            for iNY   in np.arange(NY):
                DataIn=ifid.readline() 
                
                if (iSamp==0):
                    self.yf[iNY]=np.double(DataIn.split()[1])
                self.Var["ume"][iSamp,iNY]=np.double(DataIn.split()[2])
                self.Var["vme"][iSamp,iNY]=np.double(DataIn.split()[3])
                self.Var["wme"][iSamp,iNY]=np.double(DataIn.split()[4])
                self.Var["urms"][iSamp,iNY]=np.double(DataIn.split()[5])
                self.Var["vrms"][iSamp,iNY]=np.double(DataIn.split()[6])
                self.Var["wrms"][iSamp,iNY]=np.double(DataIn.split()[7])
                self.Var["uv"][iSamp,iNY]=np.double(DataIn.split()[8])
                self.Var["uw"][iSamp,iNY]=np.double(DataIn.split()[9])
                self.Var["vw"][iSamp,iNY]=np.double(DataIn.split()[10])
                self.Var["dudy"][iSamp,iNY]=np.double(DataIn.split()[11])
                self.Var["dwdy"][iSamp,iNY]=np.double(DataIn.split()[12])
                self.Var["cp"][iSamp,iNY]=np.double(DataIn.split()[13])
                self.Var["shear"][iSamp,iNY]=np.double(DataIn.split()[14])
                self.Var["omx"][iSamp,iNY]=np.double(DataIn.split()[15])
                self.Var["omy"][iSamp,iNY]=np.double(DataIn.split()[16])
                self.Var["omz"][iSamp,iNY]=np.double(DataIn.split()[17])
                
                if N_TH>0:
                    DataIn=ifid_th.readline()
                    try:
                        self.Var["thme"][iSamp,iNY]=np.double(DataIn.split()[2])
                        self.Var["dthdy"][iSamp,iNY]=np.double(DataIn.split()[3])
                        self.Var["thrms"][iSamp,iNY]=np.double(DataIn.split()[4]) 
                        self.Var["thv"][iSamp,iNY]=np.double(DataIn.split()[5])
                        if len(DataIn.split())>7:
                            self.Var["thu"][iSamp,iNY]=np.double(DataIn.split()[6])
                            self.Var["thw"][iSamp,iNY]=np.double(DataIn.split()[7])
                            self.Var["epsp"][iSamp,iNY]=np.double(DataIn.split()[8])/(self.Re*self.Pr)
                        # self.Var["epsp_2"][iSamp,iNY]=np.double(DataIn.split()[9])/(self.Re*self.Pr)

                    except ValueError:
                        for ith in ["thme","dthdy","thv","thu","thw","epsp"]:
                            self.Var[ith][iSamp,iNY]=0.

                 
                # DataIn=ifidtke.readline()
                
                # self.Var["epsp_2"][iSamp,iNY]=np.double(DataIn.split()[2])/(self.Re*self.Pr)
  
            self.Var['epst'][iSamp,:]=(self.Var['omx'][iSamp,:]**2+self.Var['omy'][iSamp,:]**2+(self.Var['omz'][iSamp,:])**2- \
                   mypy.dfdy(self.yf,self.Var['ume'][iSamp,:])**2)/self.Re
            self.Var['epsm'][iSamp,:]=mypy.dfdy(self.yf,self.Var['ume'][iSamp,:])**2/self.Re


            self.Var["tau_b"][iSamp]=1./self.Re*np.sqrt(((self.Var['ume'][iSamp, 1]-self.Var['ume'][iSamp, 0])/(self.yf[ 1]-self.yf[ 0]))**2.+((self.Var['wme'][iSamp, 1]-self.Var['wme'][iSamp, 0])/(self.yf[ 1]-self.yf[ 0]))**2.)
            self.Var["tau_t"][iSamp]=1./self.Re*np.sqrt(((self.Var['ume'][iSamp,-2]-self.Var['ume'][iSamp,-1])/(self.yf[-2]-self.yf[-1]))**2.+((self.Var['wme'][iSamp,-2]-self.Var['wme'][iSamp,-1])/(self.yf[-2]-self.yf[-1]))**2.)
            self.Var["tau"  ][iSamp]=0.5*(self.Var['tau_t'][iSamp] + self.Var['tau_b'][iSamp])
        
            

    def read_ep(self,filename='mean_ep.txt'):
        """ Read statistics for the potential energy budget
        
        **Args:**
            filename : str, opt
                Name of the statistic file for the potential energy budget (*default*: mean_ep.txt'
        """
        
        import mypy
        
        if (not os.path.exists(filename)):
            print  filename + " do not exist in the current folder."
            raise SystemError(1)            

        tmp1=np.zeros([self.NY])
        tmp2=np.zeros([self.NY])

        myList=["ep","epfy"]
        for ith in myList:
            self.Var[ith]=np.zeros([self.NSAMPLES,self.NY])
   
        # Opening files
        ifid   =open(filename   ,'r')
  
        for iSamp in np.arange(self.NSAMPLES):
            DataIn=ifid.readline()
            DataIn=ifid.readline()
            # Write the different lines
            for iNY   in np.arange(self.NY):
                DataIn=ifid.readline()
                #try: 
                self.Var["ep"   ][iSamp,iNY]=np.double(DataIn.split()[2])
                self.Var["epfy" ][iSamp,iNY]=np.double(DataIn.split()[3])



    def read_ek(self,filename='mean_ek.txt'):
        """ Read statistics for the kinetic energy budget
        
        **Args:**
            filename : str, opt
                Name of the statistic file for the kinetic energy budget (*default*: mean_ek.txt)

        """
        import mypy

        if (not os.path.exists(filename)):
            print  filename + " do not exist in the current folder."
            raise SystemError(1)            

        tmp1=np.zeros([self.NY])
        tmp2=np.zeros([self.NY])

        myList=["pfy","ekfy"]
        for ith in myList:
            self.Var[ith]=np.zeros([self.NSAMPLES,self.NY])
   
        # Opening files
        ifid   =open(filename   ,'r')
  
        for iSamp in np.arange(self.NSAMPLES):
            DataIn=ifid.readline()
            DataIn=ifid.readline()
            # Write the different lines
            for iNY   in np.arange(self.NY):
                DataIn=ifid.readline()
                self.Var["ekfy" ][iSamp,iNY]=np.double(DataIn.split()[2])
                self.Var["pfy"  ][iSamp,iNY]=np.double(DataIn.split()[3])


    def read_vort(self):
        """ Read the time series of the x,y,z vorticity calculated in the full grid. 

        It adds the full grid (called *yg*) to the :class:`~diablo_pack.stats.StatsDiablo` and the quantities *omx_g*,*omy_g* and *omz_g* to the dictionary *st*.

        
        .. note:: It was noticed that for marginally resolved grids, dissipation ought to be calculated on the full grid in order to assure energy conservation. This in fact avoids to interpolated the vertical gradient of horizontal velocities which is the one accounting for the largest dissipation (Enrico Deusebio - 2015).
            
        """    
        
        import mypy

        filename='vort_full.txt'
        if (not os.path.exists(filename)):
            print  filename + " do not exist in the current folder."
            raise SystemError(1)            

        tmp1=np.zeros([self.NY])
        tmp2=np.zeros([self.NY])

        self.yg=np.zeros([self.NY])

        myList=["omx_g","omy_g","omz_g"]
        for ith in myList:
            self.Var[ith]=np.zeros([self.NSAMPLES,self.NY])
   
        # Opening files
        ifid   =open('vort_full.txt','r')
  
        for iSamp in np.arange(self.NSAMPLES):
            DataIn=ifid.readline()
            DataIn=ifid.readline()
            # Write the different lines
            for iNY   in np.arange(self.NY):
                DataIn=ifid.readline()
                if iSamp==0:
                    self.yg[iNY]=np.double(DataIn.split()[0])
                self.Var["omx_g" ][iSamp,iNY]=np.double(DataIn.split()[1])
                self.Var["omy_g"  ][iSamp,iNY]=np.double(DataIn.split()[2])
                self.Var["omz_g"  ][iSamp,iNY]=np.double(DataIn.split()[3])

        ifid.close()

        filename='vort_full.txt'
        if (not os.path.exists(filename)):
            print  filename + " do not exist in the current folder."
            raise SystemError(1)            


    def one_side_average(self):
        """ Computes one-sided averages and stores quantities in the dictionary pVar.
          
        """
        self.pVar={}

        self.pVar['yf'] =ave_one_side(self.yf,np.mean(self.Var['uv' ],0),sym=-1)[0]
        self.pVar['uv'] =ave_one_side(self.yf,np.mean(self.Var['uv' ],0),sym= 1)[1]
        self.pVar['ume']=ave_one_side(self.yf,np.mean(self.Var['ume'],0),sym=-1)[1]
        self.pVar['du'] =mypy.dfdy(self.pVar['yf'],self.pVar['ume'])
        self.pVar['thme']=ave_one_side(self.yf,np.mean(self.Var['thme'],0),sym=-1)[1]
        self.pVar['dth']=mypy.dfdy(self.pVar['yf'],self.pVar['thme'])

        self.pVar['P']  =-self.pVar['uv']*self.pVar['du']

        self.pVar['vv'] =ave_one_side(self.yf,np.mean(self.Var['vrms']**2,0),sym=+1)[1]
        self.pVar['thv']=ave_one_side(self.yf,np.mean(self.Var['thv' ]   ,0),sym=+1)[1]

        self.pVar['G']  =-self.Ri*self.pVar['thv']
        var=1./self.Re*np.mean(self.Var['omx']**2+self.Var['omy']**2+self.Var['omz']**2,0)
        self.pVar['eps']=ave_one_side(self.yf,var,sym=1)[1]
        self.pVar['eps']=self.pVar['eps']-1./self.Re*self.pVar['du']**2 

        self.pVar['eta']=(1./(self.Re**3*self.pVar['eps']))**(1./4.)

        # self.pVar['utau']=np.mean(-self.pVar['uv' ]+1/self.Re*self.pVar['du'])**0.5
        # self.pVar['Ttau']=np.mean(-self.pVar['thv']+1/(self.Re*self.Pr)*self.pVar['dth'])/self.pVar['utau']
        self.pVar['utau']=np.mean(-self.pVar['uv' ][0:5]+1/self.Re*self.pVar['du'][0:5])**0.5
        self.pVar['Ttau']=np.mean(-self.pVar['thv'][0:5]+1/(self.Re*self.Pr)*self.pVar['dth'][0:5])/self.pVar['utau']

        self.pVar['L']=self.pVar['utau']**2/(0.41*self.Ri*self.pVar['Ttau'])


    def __add__(self,other):
        """ Adding method for :meth:`~diablo_pack.stats.StatsDiablo` object. The time series are added one after each other. 

        The second addendum must have at least all the fields in the Var dictionary of the first.
        If the two statistics have different number of points, the statistics from the second object are interpolated

        *Example*::

             # Import first statistics
             st1 = diablo_pack.stats.StatsDiablo('mean.txt.1','mean.txt.2')
             # Import second statistics
             st2 = diablo_pack.stats.StatsDiablo('mean.txt.1','mean.txt.2')
             # Add
             st3 = st1 + st2
             # The variable in st3's Var are of dimensions (st1.NSAMPLES + st2.NSAMLES) x st1.NY
             # If the two statistics have different number of points, the statistics from the second object are interpolated
        """
        
        from numpy import append
        from copy  import deepcopy
        if isinstance(other,StatsDiablo):
            result=deepcopy(self)
            result.NSAMPLES=self.NSAMPLES+other.NSAMPLES
            result.t=append(self.t,other.t)

            for nField in result.Var.iterkeys():
                if len(result.Var[nField].shape) == 2:
                    if np.all(self.yf == other.yf):
                        tmp=other.Var[nField]
                    else:
                        tmp=np.zeros([other.NSAMPLES,self.NY])
                        for ith in np.arange(other.NSAMPLES):
                            tmp[ith,:]=np.interp(self.yf,other.yf,other.Var[nField][ith,:])
                    result.Var[nField]=append(result.Var[nField],tmp).reshape([result.NSAMPLES,result.NY])

                if len(result.Var[nField].shape) == 1:
                    result.Var[nField]=append(result.Var[nField],other.Var[nField]).reshape([result.NSAMPLES])
        
        return result

    def compute_quantities(self):
        """ Calculates the Monin-Obukhov lengthscale """

        # Initialize variables
        myList=["utau", "Ttau", "L"]
        for ith in myList:
            self.Var[ith]=np.zeros([self.NSAMPLES])

        uv =np.mean(self.Var['uv'],0)
        du =mypy.dfdy(self.yf,np.mean(self.Var['ume'],0))
        
        thv=np.mean(self.Var['thv'],0)
        dth=mypy.dfdy(self.yf,np.mean(self.Var['thme'],0))

        # self.eps=1./self.Re*np.mean(self.Var['omx']**2+self.Var['omy']**2+self.Var['omz']**2,0)-1./self.Re*du**2
        self.Var['utau']=np.mean(-uv+du/self.Re)**0.5
        self.Var['Ttau']=np.mean(-thv+1./(self.Re*self.Pr)*dth)/self.Var['utau']
        
        self.Var['L']=self.Var['utau']**2/(0.41*self.Ri*self.Var['Ttau'])


class StatsRetDiablo(StatsDiablo):
    """ Object to handle statistics in a constant friction Re run. 

    This class is inherited from the :class:`~diablo_pack.stats.StatsDiablo` object, but is specialized for runs with constant friction Ret. In those runs, Re changes with time and it is adjusted to keep Ret constant. 

    Here below we list only the differences with the :class:`~diablo_pack.stats.StatsDiablo` class
    
        1. *Attributes*
             Ret : friction Re
             Re  : time-averaged value of Re
    
        2. *Variable in Var*
             Re_b : Re giving the target Ret at the lower wall
             Re_t : Re giving the target Ret at the upper wall
             Re : value of Re giving the target Ret
             
             tau_b, tau_t, tau, epst, espm, epsp : defined with the instantaneus Re

    """

    def __init__(self,filename='mean.txt',filename_th='mean_th.txt',NY=None,N_TH=None,grid_def='grid_def',Ret=0,Ri=0,Pr=1):
        """ Initialize a RetStats object.   

        **Args:**
           filename : str
               Name of the file for the velocity statistics
           filename_th : str
               Name of the file for the scalar statistics
           NY : int, opt
               Number of points along Y (if nothing is provided, it is read from the grid_def file).
           N_TH : int, opt
               Number of scalars (if nothing is provided, it is read from the grid_def file).
           grid_def : str
               Name of the grid_def file where to take NY and N_TH
           Ret : double, opt
               friction Reynolds number 
           Ri : double, opt 
               Richardson number 
           Pr : double, opt 
               Prandtl number 

        """

        filename_tke='tke.txt';

        if NY is None:
            NY=GetResolution('NY',grid_def)
        self.NY=NY

        if N_TH is None:
            N_TH=GetResolution('N_TH',grid_def)
        self.N_TH=N_TH

        self.Ret=Ret
        self.Ri=Ri
        self.Pr=Pr

        if (not os.path.exists(filename)):
            print(  filename + " do not exist in the current folder.")
            raise SystemError(1)            
        if (not os.path.exists(filename_th) and N_TH>0):
            print(  filename_th + " do not exist in the current folder.")
            raise SystemError(1)            

        # Number of line in the files
        NL=sum(1 for line in open(filename))
        self.NSAMPLES=NL/(NY+2)

        self.yf=np.zeros([self.NY])
        self.t=np.zeros([self.NSAMPLES])

        self.Var={}
        myList=["ubulk","dt","tau_t","tau_b","tau","Re_t","Re_b","Re"]
        for ith in myList:
            self.Var[ith]=np.zeros([self.NSAMPLES])
   
        myList=["ume","vme","wme","urms","vrms","wrms","uv","uw","vw","dudy","dwdy","cp","shear","omx","omy","omz","epst","epsm"]
        if (N_TH>0) :
            myList.extend(["thme","dthdy","thrms","thv","epsp","thu","thw"])
        for ith in myList:
            self.Var[ith] =np.zeros([self.NSAMPLES,self.NY])

        # Opening files
        ifid   =open(filename   ,'r')
        if (N_TH>0) :
            ifid_th=open(filename_th,'r')
        # ifidtke=open(filename_tke   ,'r')
  
        for iSamp in np.arange(self.NSAMPLES):
            DataIn=ifid.readline()
            self.t[iSamp]=np.double(DataIn.split()[1].replace(',',''))
            self.Var["dt"][iSamp]=np.double(DataIn.split()[2])
            DataIn=ifid.readline()
            self.Var["ubulk"][iSamp]=np.double(DataIn.split()[0])
            if (N_TH>0) :
                DataIn=ifid_th.readline()
                DataIn=ifid_th.readline()
            
            # DataIn=ifidtke.readline()
            # DataIn=ifidtke.readline()

            # Write the different lines
            for iNY   in np.arange(NY):
                DataIn=ifid.readline() 
                
                if (iSamp==0):
                    self.yf[iNY]=np.double(DataIn.split()[1])
                self.Var["ume"][iSamp,iNY]=np.double(DataIn.split()[2])
                self.Var["vme"][iSamp,iNY]=np.double(DataIn.split()[3])
                self.Var["wme"][iSamp,iNY]=np.double(DataIn.split()[4])
                self.Var["urms"][iSamp,iNY]=np.double(DataIn.split()[5])
                self.Var["vrms"][iSamp,iNY]=np.double(DataIn.split()[6])
                self.Var["wrms"][iSamp,iNY]=np.double(DataIn.split()[7])
                self.Var["uv"][iSamp,iNY]=np.double(DataIn.split()[8])
                self.Var["uw"][iSamp,iNY]=np.double(DataIn.split()[9])
                self.Var["vw"][iSamp,iNY]=np.double(DataIn.split()[10])
                self.Var["dudy"][iSamp,iNY]=np.double(DataIn.split()[11])
                self.Var["dwdy"][iSamp,iNY]=np.double(DataIn.split()[12])
                self.Var["cp"][iSamp,iNY]=np.double(DataIn.split()[13])
                self.Var["shear"][iSamp,iNY]=np.double(DataIn.split()[14])
                self.Var["omx"][iSamp,iNY]=np.double(DataIn.split()[15])
                self.Var["omy"][iSamp,iNY]=np.double(DataIn.split()[16])
                self.Var["omz"][iSamp,iNY]=np.double(DataIn.split()[17])
                
                if N_TH>0:
                    DataIn=ifid_th.readline()
                    try:
                        self.Var["thme"][iSamp,iNY]=np.double(DataIn.split()[2])
                        self.Var["dthdy"][iSamp,iNY]=np.double(DataIn.split()[3])
                        self.Var["thrms"][iSamp,iNY]=np.double(DataIn.split()[4]) 
                        self.Var["thv"][iSamp,iNY]=np.double(DataIn.split()[5])
                        self.Var["thu"][iSamp,iNY]=np.double(DataIn.split()[6])
                        self.Var["thw"][iSamp,iNY]=np.double(DataIn.split()[7])
                        self.Var["epsp"][iSamp,iNY]=np.double(DataIn.split()[8])

                    except ValueError:
                        for ith in ["thme","dthdy","thv","thu","thw","epsp"]:
                            self.Var[ith][iSamp,iNY]=0.

            self.Var['epst'][iSamp,:]=(self.Var['omx'][iSamp,:]**2+self.Var['omy'][iSamp,:]**2+(self.Var['omz'][iSamp,:])**2- \
                   mypy.dfdy(self.yf,self.Var['ume'][iSamp,:])**2)
            self.Var['epsm'][iSamp,:]=mypy.dfdy(self.yf,self.Var['ume'][iSamp,:])**2

            self.Var["Re_b"][iSamp]=self.Ret**2/(self.Var['ume'][iSamp, 1]-self.Var['ume'][iSamp, 0])*(self.yf[ 1]-self.yf[ 0])
            self.Var["Re_t"][iSamp]=self.Ret**2/(self.Var['ume'][iSamp,-2]-self.Var['ume'][iSamp,-1])*(self.yf[-2]-self.yf[-1])
            self.Var["Re"  ][iSamp]=(0.5*(1./self.Var['Re_t'][iSamp] + 1./self.Var['Re_b'][iSamp]))**-1.

            self.Var["tau_b"][iSamp]=1./self.Var["Re"][iSamp]*np.sqrt(((self.Var['ume'][iSamp, 1]-self.Var['ume'][iSamp, 0])/(self.yf[ 1]-self.yf[ 0]))**2.+((self.Var['wme'][iSamp, 1]-self.Var['wme'][iSamp, 0])/(self.yf[ 1]-self.yf[ 0]))**2.)
            self.Var["tau_t"][iSamp]=1./self.Var["Re"][iSamp]*np.sqrt(((self.Var['ume'][iSamp,-2]-self.Var['ume'][iSamp,-1])/(self.yf[-2]-self.yf[-1]))**2.+((self.Var['wme'][iSamp,-2]-self.Var['wme'][iSamp,-1])/(self.yf[-2]-self.yf[-1]))**2.)
            self.Var["tau"  ][iSamp]=0.5*(self.Var['tau_t'][iSamp] + self.Var['tau_b'][iSamp])
        
        self.Re=np.mean(self.Var['Re'])
        self.Var["epst"] = self.Var["epst"]/self.Re;
        self.Var["epsm"] = self.Var["epsm"]/self.Re;
        self.Var["epsp"] = self.Var["epsp"]/(self.Re*self.Pr);





