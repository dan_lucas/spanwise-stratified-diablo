#!/bin/python

import os

import numpy as np
import mypy

from   diablo_pack.func_dia import *

class StatsRetDiablo:
    def __init__(self,filename='mean.txt',filename_th='mean_th.txt',NY=None,N_TH=None,grid_def='grid_def',Ret=0,Ri=0,Pr=1):
      
        if NY is None:
            NY=GetResolution('NY',grid_def)
        self.NY=NY

        if N_TH is None:
            N_TH=GetResolution('N_TH',grid_def)
        self.N_TH=N_TH

        self.Ret=Ret
        self.Ri=Ri
        self.Pr=Pr

        if (not os.path.exists(filename)):
            print(  filename + " do not exist in the current folder.")
            raise SystemError(1)            
        if (not os.path.exists(filename_th) and N_TH>0):
            print(  filename_th + " do not exist in the current folder.")
            raise SystemError(1)            

        # Number of line in the files
        NL=sum(1 for line in open(filename))
        self.NSAMPLES=NL/(NY+2)

        self.yf=np.zeros([self.NY])
        self.t=np.zeros([self.NSAMPLES])

        self.Var={}
        myList=["ubulk","dt","Re_t","Re_b","Re"]
        for ith in myList:
            self.Var[ith]=np.zeros([self.NSAMPLES])
   
        myList=["ume","vme","wme","urms","vrms","wrms","uv","uw","vw","dudy","dwdy","cp","shear","omx","omy","omz"]
	
        if (N_TH>0):
            myList.extend(["thme","dthdy","thrms","thv"])
        for ith in myList:
            self.Var[ith] =np.zeros([self.NSAMPLES,self.NY])

        # Opening files
        ifid   =open(filename   ,'r')
        if (N_TH>0):
            ifid_th=open(filename_th,'r')
  
        for iSamp in np.arange(self.NSAMPLES):
            DataIn=ifid.readline()
            self.t[iSamp]=np.double(DataIn.split()[1])
            self.Var["dt"][iSamp]=np.double(DataIn.split()[2])
            DataIn=ifid.readline()
            self.Var["ubulk"][iSamp]=np.double(DataIn.split()[0])

            if (N_TH>0):
                DataIn=ifid_th.readline()
                DataIn=ifid_th.readline()
            # Write the different lines
            for iNY   in np.arange(NY):
                DataIn=ifid.readline() 
                if iSamp==0:
                    self.yf[iNY]=np.double(DataIn.split()[1])
                self.Var["ume"][iSamp,iNY]=np.double(DataIn.split()[2])
                self.Var["vme"][iSamp,iNY]=np.double(DataIn.split()[3])
                self.Var["wme"][iSamp,iNY]=np.double(DataIn.split()[4])
                self.Var["urms"][iSamp,iNY]=np.double(DataIn.split()[5])
                self.Var["vrms"][iSamp,iNY]=np.double(DataIn.split()[6])
                self.Var["wrms"][iSamp,iNY]=np.double(DataIn.split()[7])
                self.Var["uv"][iSamp,iNY]=np.double(DataIn.split()[8])
                self.Var["uw"][iSamp,iNY]=np.double(DataIn.split()[9])
                self.Var["vw"][iSamp,iNY]=np.double(DataIn.split()[10])
                self.Var["dudy"][iSamp,iNY]=np.double(DataIn.split()[11])
                self.Var["dwdy"][iSamp,iNY]=np.double(DataIn.split()[12])
                self.Var["cp"][iSamp,iNY]=np.double(DataIn.split()[13])
                self.Var["shear"][iSamp,iNY]=np.double(DataIn.split()[14])
                self.Var["omx"][iSamp,iNY]=np.double(DataIn.split()[15])
                self.Var["omy"][iSamp,iNY]=np.double(DataIn.split()[16])
                self.Var["omz"][iSamp,iNY]=np.double(DataIn.split()[17])
                
                if N_TH>0:
                    DataIn=ifid_th.readline()
                    self.Var["thme"][iSamp,iNY]=np.double(DataIn.split()[2])
                    self.Var["dthdy"][iSamp,iNY]=np.double(DataIn.split()[3])
                    self.Var["thrms"][iSamp,iNY]=np.double(DataIn.split()[4]) 
                    self.Var["thv"][iSamp,iNY]=np.double(DataIn.split()[5])
                   
            self.Var["Re_b"][iSamp]=self.Ret**2/(self.Var['ume'][iSamp, 1]-self.Var['ume'][iSamp, 0])*(self.yf[ 1]-self.yf[ 0])
            self.Var["Re_t"][iSamp]=self.Ret**2/(self.Var['ume'][iSamp,-2]-self.Var['ume'][iSamp,-1])*(self.yf[-2]-self.yf[-1])
            self.Var["Re"  ][iSamp]=(0.5*(1./self.Var['Re_t'][iSamp] + 1./self.Var['Re_b'][iSamp]))**-1.
        
        self.Re=np.mean(self.Var["Re"])

    def compute_quantities(self):
        self.pVar={}

        self.pVar['yf'] =ave_one_side(self.yf,np.mean(self.Var['uv' ],0),sym=-1)[0]
        self.pVar['uv'] =ave_one_side(self.yf,np.mean(self.Var['uv' ],0),sym= 1)[1]
        self.pVar['ume']=ave_one_side(self.yf,np.mean(self.Var['ume'],0),sym=-1)[1]
        self.pVar['du'] =mypy.dfdy(self.pVar['yf'],self.pVar['ume'])
        self.pVar['thme']=ave_one_side(self.yf,np.mean(self.Var['thme'],0),sym=-1)[1]
        self.pVar['dth']=mypy.dfdy(self.pVar['yf'],self.pVar['thme'])

        self.pVar['P']  =-self.pVar['uv']*self.pVar['du']

        self.pVar['vv'] =ave_one_side(self.yf,np.mean(self.Var['vrms']**2,0),sym=+1)[1]
        self.pVar['thv']=ave_one_side(self.yf,np.mean(self.Var['thv' ]   ,0),sym=+1)[1]

        self.pVar['G']  =-self.Ri*self.pVar['thv']
        var=1./self.Re*np.mean(self.Var['omx']**2+self.Var['omy']**2+self.Var['omz']**2,0)
        self.pVar['eps']=ave_one_side(self.yf,var,sym=1)[1]
        self.pVar['eps']=self.pVar['eps']-1./self.Re*self.pVar['du']**2 

        self.pVar['eta']=(1./(self.Re**3*self.pVar['eps']))**(1./4.)

        # self.pVar['utau']=np.mean(-self.pVar['uv' ]+1/self.Re*self.pVar['du'])**0.5
        # self.pVar['Ttau']=np.mean(-self.pVar['thv']+1/(self.Re*self.Pr)*self.pVar['dth'])/self.pVar['utau']
        self.pVar['utau']=np.mean(-self.pVar['uv' ][0:5]+1/self.Re*self.pVar['du'][0:5])**0.5
        self.pVar['Ttau']=np.mean(-self.pVar['thv'][0:5]+1/(self.Re*self.Pr)*self.pVar['dth'][0:5])/self.pVar['utau']

        self.pVar['L']=self.pVar['utau']**2/(0.41*self.Ri*self.pVar['Ttau'])


    def __add__(self,other):
            from numpy import append
            from copy  import deepcopy
            if isinstance(other,StatsRetDiablo):
                result=deepcopy(self)
                result.NSAMPLES=self.NSAMPLES+other.NSAMPLES
                result.t=append(self.t,other.t)
                for nField in result.Var.iterkeys():
                    if len(result.Var[nField].shape) == 2:
                        result.Var[nField]=append(result.Var[nField],other.Var[nField]).reshape([result.NSAMPLES,result.NY])
                    if len(result.Var[nField].shape) == 1:
                        result.Var[nField]=append(result.Var[nField],other.Var[nField]).reshape([result.NSAMPLES])
                        
            return result

    def calc_moin_obukhov(self):
        uv =np.mean(self.Var['uv'],0)
        du =mypy.dfdy(self.yf,np.mean(self.Var['ume'],0))
        
        thv=np.mean(self.Var['thv'],0)
        dth=mypy.dfdy(self.yf,np.mean(self.Var['thme'],0))
        
        self.eps=1./self.Re*np.mean(self.Var['omx']**2+self.Var['omy']**2+self.Var['omz']**2,0)-1./self.Re*du**2
        self.utau=np.mean(-uv+du/self.Re)**0.5
        self.Ttau=np.mean(-thv+1./(self.Re*self.Pr)*dth)/self.utau
        
        self.L=self.utau**2/(0.41*self.Ri*self.Ttau)


