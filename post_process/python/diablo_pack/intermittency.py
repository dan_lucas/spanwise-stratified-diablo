#!/bin/python

import h5
import numpy as np

def get_dudy(y,u):
    import mypy
    # a1=sum(y*y)
    # a2=sum(y*u)
    # a=a2/a1
    a = np.interp(0,y,mypy.dfdy(y,u))
    return a

def meanfilter(varin,Nf):
    Dims=varin.shape
    varin_ex=expand(varin,Nf,Nf)
    out=np.zeros([Dims[0],Dims[1]])
    xzgrid = ( (i,j) for i in np.arange(Dims[0]) for j in np.arange(Dims[1]) )
    for (i,j) in xzgrid:
        out[i,j]=np.mean(varin_ex[i:i+2*Nf+1,j:j+2*Nf+1])
    return out

def boxrms(varin,nx,nz):
    Dims=varin.shape
    varin_ex=expand(varin,nx,nz)
    out=np.zeros([Dims[0],Dims[1]])
    xzgrid = ( (i,j) for i in np.arange(Dims[0]) for j in np.arange(Dims[1]) )
    for (i,j) in xzgrid:
        out[i,j]=np.sqrt(np.var(varin_ex[i:i+2*nx+1,j:j+2*nz+1]))
    return out

def boxmean(varin,nx,nz):
    Dims=varin.shape
    varin_ex=expand(varin,nx,nz)
    out=np.zeros([Dims[0],Dims[1]])
    xzgrid = ( (i,j) for i in np.arange(Dims[0]) for j in np.arange(Dims[1]) )
    for (i,j) in xzgrid:
        out[i,j]=np.mean(varin_ex[i:i+2*nx+1,j:j+2*nz+1])
    return out

    
def expand(varin,N0,N1):
    # Put gosth cells
    Dims=varin.shape
    out=np.zeros([Dims[0]+2*N0,Dims[1]+2*N1])
    out[N0:Dims[0]+N0,N1:Dims[1]+N1]=varin
    out[N0:Dims[0]+N0,:N1]=varin[:,-N1:]
    out[:N0,N1:Dims[1]+N1]=varin[-N0:,:]
    out[N0:Dims[0]+N0,Dims[1]+N1:Dims[1]+2*N1]=varin[:,:N1]
    out[Dims[0]+N0:Dims[0]+2*N0,N1:Dims[1]+N1]=varin[:N0,:]
    # Corners
    out[:N0,:N1]=varin[-N0:,-N1:]
    out[:N0,Dims[1]+N1:Dims[1]+2*N1]=varin[-N0:,:N1]
    out[Dims[0]+N0:Dims[0]+2*N0,:N1]=varin[:N0,-N1:]
    out[Dims[0]+N0:Dims[0]+2*N0,Dims[1]+N1:Dims[1]+2*N1]=varin[:N0,:N1]

    return out

def writeData(varin,namefile):
    import ioFiles
    import struct

    with open(namefile,'wb') as fid:
        Dims=varin.shape
        
        ioFiles.wbinfile([len(Dims)*4],fid,'i')
        ioFiles.wbinfile(Dims,fid,'i')
        ioFiles.wbinfile([len(Dims)*4],fid,'i')

        ioFiles.wbinfile([np.size(varin)*8],fid,'i')
        ioFiles.wbinfile(varin,fid,'d')
        ioFiles.wbinfile([np.size(varin)*8],fid,'i')

def readData(namefile):
    import ioFiles
    import struct

    with open(namefile,'rb') as fid:        
        nd=ioFiles.rbinfile(fid,'i')    
        Dims=ioFiles.rbinfile(fid,'i',nd[0]/4)
        chk=ioFiles.rbinfile(fid,'i')

        data=np.zeros(Dims)
        if chk != nd:
            print( ' File corrupted')
            raise SystemExit(0)

        chk=ioFiles.rbinfile(fid,'i')
        if chk[0]/8. != np.array(Dims).prod():
            print( ' File corrupted')
            raise SystemExit(0)
        data=ioFiles.rbinfile(fid,'d',np.array(Dims).prod())
        data=np.reshape(data,Dims)
        return data

    
def digitalize(varin,lim):
    BinaryData=(varin>lim)*1.
    perc=np.sum(BinaryData)*1./(1.*np.size(BinaryData))
    return perc


# def wall_normal_intermittency(namefile):
#     import shutil
#     import ioFiles
    
#     import os

#     run=diablo.run()

#     # Wall-normal intermittency based on dissipation
#     if not os.path.exists('om.h5'):
#         shutil.copyfile(namefile,'om.h5')
#         print( " Computation of the vorticity field ...")
#         flow=diablo.flow('om.h5',gridfile='grid.h5',BoxSize=run.BoxSize)
#         flow.computeVorticity()
#     else:
#         flow=diablo.flow('om.h5',gridfile='grid.h5',BoxSize=run.BoxSize)
        
#     NX=flow.Resolution[0]
#     NY=flow.Resolution[1]
#     NZ=flow.Resolution[2]
    
#     gamma_t=np.zeros(NY)
#     gamma_l=np.zeros(NY)

#     print( " Calculation of the intermittency ...")
#     for yi in np.arange(NY):
#         omx=h5.h5read('om.h5','/Timestep/Omx',[0,yi,0],count=[NZ,1,NX])
#         omy=h5.h5read('om.h5','/Timestep/Omy',[0,yi,0],count=[NZ,1,NX])
#         omz=h5.h5read('om.h5','/Timestep/Omz',[0,yi,0],count=[NZ,1,NX])
#         eps=omx**2+omy**2+omz**2
#         print( eps.shape)
#         eps.transpose([1,0])
        
#         epsmean=np.mean(eps)

#         epsrms=boxrms(eps,10,10)
    
#         lim=0.2*epsmean
#         print( " Threshold based on 0.2*epsmean " + str(lim))
#         dig_t  =(epsrms>lim)*1.

#         gamma_t[yi]=np.sum(dig_t)*1./np.size(dig_t)
#         gamma_l[yi]=1-gamma_t[yi]
    
#     Variables={0:flow.yf,1:gamma_l,2:gamma_t}
#     ioFiles.writeFileColumns(namefile + '.int',Variables)

