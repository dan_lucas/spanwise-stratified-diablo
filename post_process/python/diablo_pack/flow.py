""" Modules containing the classes to handle flow fields (written out in HDF5 format) in DIABLO.

.. currentmodule:: diablo_pack.flow

Classes
--------

.. autoclass:: FlowDiablo
   :members:


Functions
---------

.. autofunction:: GetStatistics

.. autofunction:: get_spectrum_2d


"""

import os

import numpy as np
import mypy
import h5

from diablo_pack.func_dia import *

class FlowDiablo:
    """ Class containing a flow field of DIABLO. 

    **Attributes:**
    
       BoxSize : array, double
          Array with the dimension of the box
       Fields : list, str
          List of the variable present in the file (e.g. *U,V,W,TH1,...*)
       Resolution : array, int
          Array with the number of points
       filename : str
          Name of the file
       gridfile : str
          Name of the HDF5 file containing the grid
       home : str
          String with the path of the folder
       t : double 
          Time stamp of the flow field
       x : array, double 
          array containing x-coordinates    
       y : array, double 
          array containing y-coordinates in full grid    
       yf : array, double 
          array containing y-coordinates in fractional grid    
       z : array, double 
          array containing z-coordinates

    """
    def __init__(self,filename='out.h5',BoxSize=None,gridfile='grid.h5'):
        """ Initialize FlowDiablo object. 

        **Args:**
        
           filename : str
              Name of the file
           BoxSize : array, double 
              Dimension of the box. If nothing is given, 6.28 x 2 x 6.28 is assumed.
           gridfile : str
              Name of the file containing the grid

        .. note :: When the object is initialized, no data (apart from the headers) is read. The data are read only when needed. 

        """

        self.home     = os.getcwd()

        self.filename = os.getcwd() + '/' + filename
        self.gridfile = os.getcwd() + '/' + gridfile

        self.Resolution=h5.h5readatt(self.filename,'/','Resolution',ishow=False)
        self.t=h5.h5readatt(self.filename,'/Timestep','Time',ishow=False)

        if (BoxSize is None):
            if 'BoxSize' in h5.h5GetListAtt(self.filename,'/'):
                h5.h5readatt(self.filename,'/','BoxSize')
            else:
                BoxSize=np.array([2*np.pi,2.,2*np.pi])

        xl=BoxSize[0]
        yl=BoxSize[1]
        zl=BoxSize[2]

        NX=self.Resolution[0]
        NY=self.Resolution[1]
        NZ=self.Resolution[2]

        (self.y,self.yf)=ReadGrid(self.gridfile)

        self.x=xl/NX*np.arange(NX)
        self.z=zl/NZ*np.arange(NZ)

        self.BoxSize=np.zeros([3])
        self.BoxSize[0]=xl
        self.BoxSize[1]=np.max(self.yf)-np.min(self.yf)
        self.BoxSize[2]=zl

        # Get the name of the variables
        self.Fields=h5.h5GetListObj(self.filename,'/Timestep')
      

    def rFlowField(self,nVar,start = [0,0,0],count = None,stride= [1,1,1]):
        """ Reads the data given by *nVar* from the flow field

        *start*, *count* and *stride* allow to read subsections of the flow. This is reccomended when large flow fields are to be read. 

        **Args:**
        
           nVar : str
              Name of the variable to be read
           start : array, int
              Array with the indices in each direction of where should be started to read
           count : array, int
              Number of points to be read in each direction
           stride : array, int
              Stride between the points to be read

        """

        from time import time as ctim
        
        if count is None:
            count = [ self.Resolution[0]  , self.Resolution[1]    ,\
                      self.Resolution[2]  ]            
        # Transform to integer
        count = [int(count1) for count1 in count] 

        count.reverse()
        stride.reverse()
        start.reverse()

        flow=h5.h5read(self.filename,'/Timestep/'+nVar,start=start,count=count,stride=stride)
        flow=flow.transpose((2,1,0))
        return flow


    def wall_normal_intermittency(self):
        """ Compute the fluctuation of the dissipation in a horizontal subdomain.

        This subroutine was used in *Deusebio et al. 2015* to assess the variation of intermittency in the wall-normal direction. The intermittency is dumped onto a file *namefile* ``.int``

        .. note:: In order to compute the dissipation, the subroutine computes the vorticity first. This takes long time for big flow fields. 

        """
        import diablo_pack.intermittency as intermittency

        import shutil
        import ioFiles
        
        NX=self.Resolution[0]
        NY=self.Resolution[1]
        NZ=self.Resolution[2]
    
        gamma_t=np.zeros(NY)
        gamma_l=np.zeros(NY)

        if (('Omx' in self.Fields) and
            ('Omy' in self.Fields) and
            ('Omz' in self.Fields) ) :
            self.compute_vorticity()

        print( " Calculation of the intermittency ...")
        for yi in np.arange(NY):
            omx=h5.h5read(self.filename,'/Timestep/Omx',[0,yi,0],count=[NZ,1,NX])
            omy=h5.h5read(self.filename,'/Timestep/Omy',[0,yi,0],count=[NZ,1,NX])
            omz=h5.h5read(self.filename,'/Timestep/Omz',[0,yi,0],count=[NZ,1,NX])
            eps=omx**2+omy**2+omz**2
            eps=np.transpose(eps,[1,0])
        
            epsmean=np.mean(eps)

            epsrms=intermittency.boxrms(eps,10,10)
    
            lim=0.2*epsmean
            # print( " Threshold based on 0.2*epsmean " + str(lim))
            dig_t  =(epsrms>lim)*1.

            gamma_t[yi]=np.sum(dig_t)*1./np.size(dig_t)
            gamma_l[yi]=1-gamma_t[yi]
    
        Variables={0:self.yf,1:gamma_l,2:gamma_t}
        ioFiles.writeFileColumns(self.filename + '.int',Variables)


    def get_laminar_turbulent_patch(self,iOut=None):
        """ Compute the fluctuations of shear-stresses at the wall.
        
        **Args:**
           iOut : str, opt
              Name of the output file

        This subroutine was used in *Deusebio et al. 2015* to find laminar and turbulent region according to the criterion introduced in the paper. When *iOut* is given, a file with the fluctuations is written out for the upper (boxVaru.txt.) and lower (boxVarl.txt.) wall separately. 

        """

        import diablo_pack.intermittency as intermittency

        Nybox=4

        boxup=h5.h5read(self.filename,'/Timestep/U',[0,0,0],count=[self.Resolution[2],Nybox,self.Resolution[0]])
        boxup=np.transpose(boxup,[2,1,0])
        
        boxwp=h5.h5read(self.filename,'/Timestep/W',[0,0,0],count=[self.Resolution[2],Nybox,self.Resolution[0]])
        boxwp=np.transpose(boxwp,[2,1,0])

        xz_grid = ( (i,j) for i in np.arange(self.Resolution[0]) for j in np.arange(self.Resolution[2]) )
        dudy=np.zeros([self.Resolution[0],self.Resolution[2]])
        dwdy=np.zeros([self.Resolution[0],self.Resolution[2]])
        for (i,j) in xz_grid:
            dudy[i,j]=intermittency.get_dudy(self.yf[:Nybox]+1,boxup[i,:Nybox,j])
            dwdy[i,j]=intermittency.get_dudy(self.yf[:Nybox]+1,boxwp[i,:Nybox,j])

        boxVar_l=intermittency.boxrms(dudy**2+dwdy**2,10,10)

        boxup=h5.h5read(self.filename,'/Timestep/U',[0,self.Resolution[1]-Nybox,0],count=[self.Resolution[2],Nybox,self.Resolution[0]])
        boxup=np.transpose(boxup,[2,1,0])

        boxwp=h5.h5read(self.filename,'/Timestep/W',[0,self.Resolution[1]-Nybox,0],count=[self.Resolution[2],Nybox,self.Resolution[0]])
        boxwp=np.transpose(boxwp,[2,1,0])

        xz_grid = ( (i,j) for i in np.arange(self.Resolution[0]) for j in np.arange(self.Resolution[2]) )
        dudy=np.zeros([self.Resolution[0],self.Resolution[2]])
        dwdy=np.zeros([self.Resolution[0],self.Resolution[2]])
        for (i,j) in xz_grid:
            dudy[i,j]=intermittency.get_dudy(self.yf[:Nybox]-1,boxup[i,:Nybox,j])
            dwdy[i,j]=intermittency.get_dudy(self.yf[:Nybox]-1,boxwp[i,:Nybox,j])

        boxVar_u=intermittency.boxrms(dudy**2+dwdy**2,10,10)

        if iOut is not None:
            NX=len(self.x)
            NZ=len(self.z)

            X,Y=np.meshgrid(self.x,self.z)
        
            data=np.zeros([3,X.shape[0],X.shape[1]])
            data[0,:,:]=X
            data[1,:,:]=Y
        
            data[2,:,:]=np.zeros([NX,NZ])
            data[2,:,:]=boxVar_l
            writeFile(data,filename='boxVarl.txt.' + iOut)
            data[2,:,:]=boxVar_u
            writeFile(data,filename='boxVaru.txt.' + iOut)

        return boxVar_l,boxVar_u

    def LamTurbStats(self,lim=7.5):
        """ Compute separate statistics for larminar and turbulent regions. 
        
        **Args:**
            The threshold *lim* is used to divide between laminar/turbulent regions depending on the wall-shear stress. 

        Two files (``mean.txt.lam`` and ``mean.txt.tur``) are written out which are compatible to be read in a :class:`~diablo_pack.stats.StatsDiablo` object.
        
        """
        
        import diablo_pack.intermittency as intermittency
        
        boxVar_l,boxVar_u=self.get_laminar_turbulent_patch(iOut=None)
        dig_l  =(boxVar_l>lim)*1.
        dig_u  =(boxVar_u>lim)*1.
        self.ComputeStats(dig_l,dig_u)


    def SpanwiseStats(self,post='.span'):
        """ Compute spanwise statistics and write them out in a file ending by what given in *post*

        The file is compatible to be read in a :class:`~diablo_pack.stats.StatsDiablo` object.
        """

        def GetStatisticsSpan(u,v,w,th):

            out=dict();

            out['ume']  = np.mean(u ,axis=1) 
            out['vme']  = np.mean(v ,axis=1)
            out['wme']  = np.mean(w ,axis=1)
            out['thme'] = np.mean(th,axis=1)

            out['uu']  = np.mean(u**2,axis=1)-out['ume']**2
            out['vv']  = np.mean(v**2,axis=1)-out['vme']**2
            out['ww']  = np.mean(w**2,axis=1)-out['wme']**2

            out['urms']  = np.sqrt(out['uu'])
            out['vrms']  = np.sqrt(out['vv'])
            out['wrms']  = np.sqrt(out['ww'])

            out['thth']  = np.mean(th**2,axis=1)-out['thme']**2

            out['thrms'] = np.sqrt(out['thth'])

            out['uv']  = np.mean(u*v,axis=1)-out['ume']*out['vme']
            out['uw']  = np.mean(u*w,axis=1)-out['ume']*out['wme']
            out['vw']  = np.mean(v*w,axis=1)-out['vme']*out['wme']

            out['thv'] = np.mean(th*v,axis=1)-out['thme']*out['vme']

            return out


        for ith in np.arange(self.Resolution[2]):
            u=h5.h5read(self.filename,'/Timestep/U',[ith,0,0],[1,self.Resolution[1],self.Resolution[0]]).squeeze()
            v=h5.h5read(self.filename,'/Timestep/V',[ith,0,0],[1,self.Resolution[1],self.Resolution[0]]).squeeze()
            w=h5.h5read(self.filename,'/Timestep/W',[ith,0,0],[1,self.Resolution[1],self.Resolution[0]]).squeeze()
            th1=h5.h5read(self.filename,'/Timestep/TH1',[ith,0,0],[1,self.Resolution[1],self.Resolution[0]]).squeeze()

            # Turbulent statistics
            out=GetStatisticsSpan(u,v,w,th1)

            fid=open('mean.txt' + post,'a')
            fidth=open('mean_th.txt' + post,'a')
    
            for ifid in [fid,fidth]:
                ifid.write('NaN \t ' + str(self.z[ith]) + '\t NaN\n')
                ifid.write(str(self.t) + ' \n')
            
            for ith in np.arange(self.Resolution[1]):
                
                my_str=str(ith+1) + '\t' + str(self.yf[ith]) + '\t' + str(out['ume'][ith]) + '\t' + str(out['vme'][ith]) + '\t' + str(out['wme'][ith]) + '\t' + str(out['urms'][ith]) + '\t' + str(out['vrms'][ith]) + '\t' + str(out['wrms'][ith]) + '\t' + str(out['uv'][ith]) + '\t' + str(out['uw'][ith]) + '\t' + str(out['vw'][ith]) + '\t' + 'NaN\tNaN\tNaN\tNaN\tNaN\tNaN\tNaN\n'
                fid.write(my_str)
                my_str=str(ith+1) + '\t' + str(self.yf[ith]) + '\t' + str(out['thme'][ith]) + '\tNaN\t' + str(out['thrms'][ith]) + '\t' + str(out['thv'][ith]) + '\tNaN\n' 
                fidth.write(my_str)
                
            fid.close()
            fidth.close()

        
        # for ith in ['mean.txt.span','mean_th.txt.span']:
        #     if os.path.exists(ith):
        #         os.remove(ith)

        # for ith in np.arange(self.Resolution[2]):
        #     dig_l=np.zeros([self.Resolution[2],self.Resolution[0]])
        #     dig_l[ith,:]=1.;
        #     dig_u       =dig_l;
        #     self.ComputeStats(dig_l,dig_u,filel='.temp',filet='.span')

        # for ith in ['mean.txt.temp','mean_th.txt.temp']:
        #     os.remove(ith)

    def ComputeStats(self,dig_l,dig_u,filel='.lam',filet='.tur'):
        """ Compute statistics with the conditional averaging provided by *dig_l* and *dig_u* and write out statistics files.

        This subroutine was used in *Deusebio et al. 2015* to find the statistics for laminar and turbulent regions separately. When *iOut* is given, a file with the fluctuations is written out for the upper (boxVaru.txt.) and lower (boxVarl.txt.) wall separately. 

        **Args:**
           dig_l : array, double
               2D array with dimensions NX x NZ. It defines which points are used to provide the average for the *lower-half* and has value of 1 (for turbulent regions) and 0 (for laminar regions).
           dig_u : array, double
               2D array with dimensions NX x NZ. It defines which points are used to provide the average for the *upper-half* and has value of 1 (for turbulent regions) and 0 (for laminar regions). 
           filel : str
               Name of statistics file for laminar regions
           filet : str    
               Name of statistics file for turbulent regions
               
        """

        fidl=open('mean.txt' + filel,'a')
        fidt=open('mean.txt' + filet,'a')
        fidthl=open('mean_th.txt' + filel,'a')
        fidtht=open('mean_th.txt' + filet,'a')
    
        for ith in [fidl,fidt,fidthl,fidtht]:
            ith.write('NaN \t ' + str(self.t) + '\t NaN\n')
            ith.write('NaN \n')

        for ith in np.arange(self.Resolution[1]):
            u=h5.h5read(self.filename,'/Timestep/U',[0,ith,0],[self.Resolution[2],1,self.Resolution[0]]).squeeze()
            v=h5.h5read(self.filename,'/Timestep/V',[0,ith,0],[self.Resolution[2],1,self.Resolution[0]]).squeeze()
            w=h5.h5read(self.filename,'/Timestep/W',[0,ith,0],[self.Resolution[2],1,self.Resolution[0]]).squeeze()
            th1=h5.h5read(self.filename,'/Timestep/TH1',[0,ith,0],[self.Resolution[2],1,self.Resolution[0]]).squeeze()

            if self.yf[ith]>0:
                dig=dig_u
            else:
                dig=dig_l

            # Turbulent statistics
            out=GetStatistics(u,v,w,th1,dig)
            my_str=str(ith+1) + '\t' + str(self.yf[ith]) + '\t' + str(out['ume']) + '\t' + str(out['vme']) + '\t' + str(out['wme']) + '\t' + str(out['urms']) + '\t' + str(out['vrms']) + '\t' + str(out['wrms']) + '\t' + str(out['uv']) + '\t' + str(out['uw']) + '\t' + str(out['vw']) + '\t' + 'NaN\tNaN\tNaN\tNaN\tNaN\tNaN\tNaN\n'
            fidt.write(my_str)
            my_str=str(ith+1) + '\t' + str(self.yf[ith]) + '\t' + str(out['thme']) + '\tNaN\t' + str(out['thrms']) + '\t' + str(out['thv']) + '\tNaN\n' 
            fidtht.write(my_str)

        # Laminar statistics
            out=GetStatistics(u,v,w,th1,1-dig)
            my_str=str(ith+1) + '\t' + str(self.yf[ith]) + '\t' + str(out['ume']) + '\t' + str(out['vme']) + '\t' + str(out['wme']) + '\t' + str(out['urms']) + '\t' + str(out['vrms']) + '\t' + str(out['wrms']) + '\t' + str(out['uv']) + '\t' + str(out['uw']) + '\t' + str(out['vw']) + '\t' + 'NaN\tNaN\tNaN\tNaN\tNaN\tNaN\tNaN\n'
            fidl.write(my_str)
            my_str=str(ith+1) + '\t' + str(self.yf[ith]) + '\t' + str(out['thme']) + '\tNaN\t' + str(out['thrms']) + '\t' + str(out['thv']) + '\tNaN\n' 
            fidthl.write(my_str)

        for ith in [fidl,fidt,fidthl,fidtht]:
            ith.close()


    def plotcontourf(self,data):

        import matplotlib
        matplotlib.use('agg')
        import matplotlib.pyplot as plt
        from   mpl_toolkits.mplot3d import Axes3D

        # Create the plot
        fig=plt.figure()
        # Axes3D(fig)
        plt.contourf(data[0,:,:],data[1,:,:],data[2,:,:])
        plt.axis('equal')
        plt.azim=-90
        plt.elev=90
        plt.show()
 
        
    def specx (self,yc,nVar1,nVar2,fileut='xspec.dat',ri='real'):
        """ Calculates the one-dimensional streamwise spectra (or co-spectra) at a certain height.

        **Args:**
           yc : double
              Vertical height at which the spectra is calculated 
           nVar1 : str
              Name of the first variable 
           nVar2 : str
              Name of the second variable 
           fileut : str, opt
              Name of the output file 
           ri : str
              Flag which can be *real* or *imag* in order to write out only either the real part of the imaginary part of *nVar1*conj(nVar2)*

        *Usage:*::

           # Get the spectra of U
           flow.specx(0.5,'U','U')
           # Get the co-spectra U of V
           flow.specx(0.5,'U','V')      

        """
        import ioFiles
        from operator import itemgetter

        yi=min(enumerate(abs(self.yf-yc)),key=itemgetter(1))[0]
        print " y-plane at y=%7.5f (error=%7.5f)" % \
            (self.yf[yi],abs(self.yf[yi]-yc))
        
        NX=self.Resolution[0]
        NZ=self.Resolution[2]
        
        En=get_spectrum_2d(self.filename,yi,nVar1,nVar2,ri)

        En1d=np.sum(En,0);
        Kx1d=np.arange(NX/2)*2*np.pi/self.BoxSize[0];

        Var={0:Kx1d,1:En1d};
        ioFiles.writeFileColumns(fileut,Var)
        
        return (Kx1d,En1d)
    

    def specz (self,yc,nVar1,nVar2,fileut='zspec.dat',ri='real'):
        """ Calculates the one-dimensional spanwise spectra (or co-spectra) at a certain height.

        **Args:**
           yc : double
              Vertical height at which the spectra is calculated  
           nVar1 : str
              Name of the first variable 
           nVar2 : str
              Name of the second variable 
           fileut : str, opt
              Name of the output file 
           ri : str
              Flag which can be *real* or *imag* in order to write out only either the real part of the imaginary part of *nVar1*conj(nVar2)*

        """

        import ioFiles
        from operator import itemgetter

        yi=min(enumerate(abs(self.yf-yc)),key=itemgetter(1))[0]
        print " y-plane at y=%7.5f (error=%7.5f)" % \
            (self.yf[yi],abs(self.yf[yi]-yc))
        
        NX=self.Resolution[0]
        NZ=self.Resolution[2]
        
        En=get_spectrum_2d(self.filename,yi,nVar1,nVar2,ri)

        En1d=np.sum(En,1);
        Kz1d=np.arange(NZ/2)*2*np.pi/self.BoxSize[2];

        Var={0:Kz1d,1:En1d};
        ioFiles.writeFileColumns(fileut,Var)
        
        return (Kz1d,En1d)


    def specxz(self,yc,nVar1,nVar2,fileut='xzspec.dat',ri='real'):
        """ Calculates the two-dimensional streamwise spectra (or co-spectra) at a certain height.

        **Args:**
           yc : double 
              Vertical height at which the spectra is calculated
           nVar1 : str
              Name of the first variable 
           nVar2 : str
              Name of the second variable 
           fileut : str, opt
              Name of the output file 
           ri : str
              Flag which can be *real* or *imag* in order to write out only either the real part of the imaginary part of *nVar1*conj(nVar2)*

        .. note:: The contribution in the negative wavenumbers are mirrored to the positive wavenumbers.

        *Usage:*::

           # Get the spectra of U
           flow.specxz(0.5,'U','U')
           # Get the co-spectra U of V
           flow.specxz(0.5,'U','V')      

        """
        from operator import itemgetter

        yi=min(enumerate(abs(self.yf-yc)),key=itemgetter(1))[0]
        print " y-plane at y=%7.5f (error=%7.5f)" % \
            (self.yf[yi],abs(self.yf[yi]-yc))
        
        NX=self.Resolution[0]
        NZ=self.Resolution[2]
        
        En=get_spectrum_2d(self.filename,yi,nVar1,nVar2,ri)
        
        X,Y=np.meshgrid(np.arange(NX/2)*2*np.pi/self.BoxSize[0],np.arange(NZ/2)*2*np.pi/self.BoxSize[2])
        
        data=np.zeros([3,X.shape[0],X.shape[1]])
        data[0,:,:]=X
        data[1,:,:]=Y
        data[2,:,:]=En
        writeFile(data,fileut)

        return data

    
    def plotxz(self,yc,nVar,fileut='xzplane.dat'):
        """ Plot the contour of xz-slice at a *yc* for the *nVar* variable.

        **Args:**
           yc : double
              Vertical height of the slice
           nVar : str
              Name of the variable
           fileut : str, opt
              Name of the output file 

        """
        from operator import itemgetter

        NX=self.Resolution[0]
        NZ=self.Resolution[2]

        yi=min(enumerate(abs(self.yf-yc)),key=itemgetter(1))[0]
        print( " y-plane at y=%7.5f (error=%7.5f)" % \
              (self.yf[yi],abs(self.yf[yi]-yc)) )
        
        X,Y=np.meshgrid(self.x,self.z)
        
        data=np.zeros([3,X.shape[0],X.shape[1]])
        data[0,:,:]=X
        data[1,:,:]=Y
        
        data[2,:,:]=np.zeros([NX,NZ])
        data[2,:,:]=h5.h5read(self.filename,'/Timestep/'+nVar,[0,yi,0],[NZ,1,NX])
        writeFile(data,filename=fileut)
        self.plotcontourf(data)


    def compute_vorticity(self,NXc=10,NZc=10):
        """ Compute the vorticity and add it to the file 

        **Args:**
           NXc : int, opt
              Dimension of sub-blocks (in x) read-in simultaneously 
           NXc : int, opt
              Dimension of sub-blocks (in z) read-in simultaneously 

        .. note:: This might takes long time for big flow fields. Varying *NXc* and *NZc* can have significant impact on performances. 

        """
#        Omx=np.zeros(self.Resolution)
#        Omy=np.zeros(self.Resolution)
#        Omz=np.zeros(self.Resolution)

        NX=self.Resolution[0]
        NY=self.Resolution[1]
        NZ=self.Resolution[2]

        Kx1d=2*np.pi/self.BoxSize[0]*mypy.vectorize([np.arange(NX/2),np.arange(-NX/2,0)])
        Kz1d=2*np.pi/self.BoxSize[2]*mypy.vectorize([np.arange(NZ/2),np.arange(-NZ/2,0)])

        [Kx2d,Kz2d]=np.meshgrid(Kx1d,Kz1d)

        # Create the datasets
        fid = h5.h5py.File(self.filename,'a')
        for nVar in ['Omx','Omy','Omz']:
            if not nVar in fid['/Timestep'].keys():
                dset = fid.create_dataset('/Timestep/'+nVar,[NZ,NY,NX])
        fid.close()

        # Contributions in spectral space
        for yi in np.arange(NY):
            UU=h5.h5read(self.filename,'/Timestep/U',[0,yi,0],[NZ,1,NX])
            VV=h5.h5read(self.filename,'/Timestep/V',[0,yi,0],[NZ,1,NX])
            WW=h5.h5read(self.filename,'/Timestep/W',[0,yi,0],[NZ,1,NX])
            UU=np.fft.fftn(UU)
            VV=np.fft.fftn(VV)
            WW=np.fft.fftn(WW)
            Omx=-1j* Kz2d*VV.squeeze()
            Omy=+1j*(Kz2d*UU.squeeze()-Kx2d*WW.squeeze())
            Omz=+1j* Kx2d*VV.squeeze()
            Omx=np.fft.ifftn(Omx).real
            Omy=np.fft.ifftn(Omy).real
            Omz=np.fft.ifftn(Omz).real
            h5.h5write(self.filename,Omx,'/Timestep/Omx',[0,yi,0],[NZ,1,NX])
            h5.h5write(self.filename,Omy,'/Timestep/Omy',[0,yi,0],[NZ,1,NX])
            h5.h5write(self.filename,Omz,'/Timestep/Omz',[0,yi,0],[NZ,1,NX])            
        # Contribution in physical space
        for zi in np.arange(0,NZ,NZc):
            for xi in np.arange(0,NX,NXc):
                xn=min(NXc,NX-xi)
                zn=min(NZc,NZ-zi)
                UU=h5.h5read(self.filename,'/Timestep/U',[zi,0,xi],[zn,NY,xn])
                WW=h5.h5read(self.filename,'/Timestep/W',[zi,0,xi],[zn,NY,xn])
                Omx=h5.h5read(self.filename,'/Timestep/Omx',[zi,0,xi],[zn,NY,xn])
                Omz=h5.h5read(self.filename,'/Timestep/Omz',[zi,0,xi],[zn,NY,xn])
                xzgrid=[(ix,iz) for ix in np.arange(xn) for iz in np.arange(zn)]
                for (ix,iz) in xzgrid:
                    Omx[iz,0:NY,ix]=Omx[iz,0:NY,ix]+mypy.dfdy(self.yf,WW[iz,0:NY,ix])
                    Omz[iz,0:NY,ix]=Omz[iz,0:NY,ix]-mypy.dfdy(self.yf,UU[iz,0:NY,ix])
                
                h5.h5write(self.filename,Omx,'/Timestep/Omx',[zi,0,xi],[zn,NY,xn])
                h5.h5write(self.filename,Omz,'/Timestep/Omz',[zi,0,xi],[zn,NY,xn])

    def insert_grid_visit(self):
        """ Create VISIT grid and insert it in the file. 

        .. note:: This is done in order to allow the file to be properly read into VISIT. For big files, this operation may take long time.
 
        """
        # First write the grid in the file
        self.create_grid_visit(self.filename)
        # Link to the timestep folder
        fid=h5.h5py.File(self.filename)
        gid=fid['Timestep']
        gid.attrs['coords']=['/grid_visit/xcoord\0','/grid_visit/ycoord\0','/grid_visit/zcoord\0']
        fid.close()


    def clone_to_pixie(self,filename='pixie.h5',gridfile=None):
        """ Create a new file (obeying the PIXIE format) to be read into VISIT. 

        **Args:**
           filename : str
              Name of the new PIXIE file 
           gridfile : str
              Name of the file containing the full VISIT grid. 
        
        The new file *filename* is very light and only links to the variables in the current HDF5 file containing the flow field. It also contains links to the VISIT grid stored in the *gridfile*. Note that if the latter file does not exist, it is created, which may take long time for high resolutions.

        *Usage*::
        
            # Import first file
            flow1=diablo_pack.flow.FlowDiablo('out.h5')
            # Import second file
            flow2=diablo_pack.flow.FlowDiablo('end.h5')
            # Create grid (!!!THIS MAY BE SLOW!!!)
            flow.create_grid_visit('grid_visit.h5')
            # Create first PIXIE (Very fast - it only links)
            flow1.clone_to_visit('out.pixie.h5','grid_visit.h5')
            # Create second PIXIE (Very fast - it only links)
            flow2.clone_to_visit('end.pixie.h5','grid_visit.h5')

        """
        if gridfile==None:
            gridfile=self.gridfile

        filename = self.home + '/' + filename
        gridfile = self.home + '/' + gridfile

        # If the file already exists, cancel that
        if os.path.exists(filename):
            os.remove(filename)
            
        # If necessary create the grid
        if not os.path.exists(gridfile):
            self.create_grid_visit(gridfile)

        # Create the cloned pixie file
        h5.h5writeatt(filename,'/','Resolution',self.Resolution)
        
        # Add time stamp
        h5.h5writeatt(filename,'/Timestep','Time',self.t)

        fid=h5.h5py.File(filename)
        gid=fid['Timestep']
        for ith in self.Fields:
            fid['/Timestep/'+ith]=h5.h5py.ExternalLink(self.filename,'/Timestep/'+ith)
        gid.attrs['coords']=['/node_coords/x\0','/node_coords/y\0','/node_coords/z\0']
        fid['/node_coords/x']=h5.h5py.ExternalLink(gridfile,'/grid_visit/xcoord')
        fid['/node_coords/y']=h5.h5py.ExternalLink(gridfile,'/grid_visit/ycoord')
        fid['/node_coords/z']=h5.h5py.ExternalLink(gridfile,'/grid_visit/zcoord')
        fid.close()   

    def create_grid_visit(self,gridfile=None):
        """ Create the VISIT grid and store it in *gridfile*.
        """

        # Generates the three coordinates systems
        NX=self.Resolution[0]
        NY=self.Resolution[1]
        NZ=self.Resolution[2]

        # Default values
        if gridfile==None:
            gridfile=self.gridfile

        # Create the datasets
        if os.path.exists(gridfile):
            if any([x=='grid_visit' for x in h5.h5GetListObj(gridfile,'/')]):
                h5.h5del(gridfile,'/grid_visit/')

        fid = h5.h5py.File(gridfile,'a')
        dset = fid.create_dataset('/grid_visit/xcoord',[NZ,NY,NX])
        dset = fid.create_dataset('/grid_visit/ycoord',[NZ,NY,NX])
        dset = fid.create_dataset('/grid_visit/zcoord',[NZ,NY,NX])
        fid.close()

        # Y-grid is the fractional one
        x2d=np.zeros([NZ,1,NX])
        y2d=np.zeros([NZ,1,NX])
        z2d=np.zeros([NZ,1,NX])
        for y in np.arange(NY):
            y2d=np.ones([NZ,1,NX])*self.yf[y]
            for z in np.arange(NZ):
                x2d[z,0,:]=self.x[:]
                z2d[z,0,:]=np.ones(NX)*self.z [z]
            
            h5.h5write(gridfile,x2d,'/grid_visit/xcoord',[0,y,0],[NZ,1,NX])
            h5.h5write(gridfile,y2d,'/grid_visit/ycoord',[0,y,0],[NZ,1,NX])
            h5.h5write(gridfile,z2d,'/grid_visit/zcoord',[0,y,0],[NZ,1,NX])
            
    
    def link_to_grid(self,gridfile=None):
        """ Create a link in the current file to the VISIT grid contained in *gridfile*. 

        .. note :: This may allow the file to be directly imported into VISIT. 
        """
        # Default values
        if gridfile==None:
            gridfile=self.gridfile
        
        if any([x=='node_coords' for x in h5.h5GetListObj(self.filename,'/')]):
            h5.h5del(self.filename,'node_coords')
        
        fid=h5.h5py.File(self.filename)
        gid=fid['Timestep']
        gid.attrs['coords']=['/node_coords/x\0','/node_coords/y\0','/node_coords/z\0']
        fid['/node_coords/x']=h5.h5py.ExternalLink(gridfile,'/grid_visit/xcoord')
        fid['/node_coords/y']=h5.h5py.ExternalLink(gridfile,'/grid_visit/ycoord')
        fid['/node_coords/z']=h5.h5py.ExternalLink(gridfile,'/grid_visit/zcoord')
        # gid.attrs['coords']=['/grid_visit/xcoord\0','/grid_visit/ycoord\0','/grid_visit/zcoord\0']
        fid.close()

    def change_resolution_y(self,nameout,gridfile,NXc=10,NZc=10):
        """ Create a new flow field where quantities are interpolated to a new grid in the vertical direction.

        **Args:**
           nameout : str
              Name of the new file
           gridfile : str
              Name of the file containing the new grid
           NXc : int, opt
              Dimension of sub-blocks (in x) read-in simultaneously 
           NXc : int, opt
              Dimension of sub-blocks (in z) read-in simultaneously 

        .. note:: This might takes long time for big flow fields. Varying *NXc* and *NZc* can have significant impact on performances. 

        *Usage*::
        
            # Import first file
            flow=diablo_pack.flow.FlowDiablo('out.h5','grid.h5')
            # Create new file interpolated to new grid
            flow.change_resolution_y('new.h5','grid2.h5')

        """


        NX=self.Resolution[0]
        NY=self.Resolution[1]
        NZ=self.Resolution[2]

        DataIn=np.zeros([NZc,NY,NXc])

        # Delete file if necessary 
        if os.path.exists(nameout):
            os.remove(nameout)

        # Get the new grid
        yn=h5.h5read(gridfile,'/grids/y')
        NYn=len(yn)-1
        yfn=np.zeros(NYn)
        for yi in np.arange(NYn):
            yfn[yi]=0.5*(yn[yi]+yn[yi+1])

        # Create the file
        h5.h5writeatt(nameout,'/','Resolution',[NX,NYn,NZ],dtype='<i4')
        h5.h5writeatt(nameout,'/Timestep','Time',self.t,dtype='<f8')
        
        for nth in self.Fields:
            print( " Computing " + nth + " ...")
            # First create the dataset with the right dimensions
            fid = h5.h5py.File(nameout,'a')
            dset = fid.create_dataset('/Timestep/' + nth,[NZ,NYn,NX])
            fid.close()

            # xzgrid=[(xi,zi) for xi in np.arange(NX) for zi in np.arange(NZ)]
            # for (xi,zi) in xzgrid:
            for zi in np.arange(0,NZ,NZc):
                for xi in np.arange(0,NX,NXc):
                    # print "Chunk: (" + str(xi) + ',' + str(zi) + ')' 
                    xn=min(NXc,NX-xi)
                    zn=min(NZc,NZ-zi)
                    # print "Elements: (" + str(xn) + ',' + str(zn) + ')' 
                    DataIn=h5.h5read(self.filename,'/Timestep/'+nth,[zi,0,xi],[zn,NY,xn])
                    
                    DataUt=np.zeros([zn,NYn,xn])
                    xzgrid=[(ix,iz) for ix in np.arange(xn) for iz in np.arange(zn)]
                    for (ix,iz) in xzgrid:
                        DataUt[iz,0:NYn,ix]=np.interp(yfn,self.yf,DataIn[iz,0:NY,ix])
                    
                    h5.h5write(nameout,DataUt.reshape([zn,NYn,xn]),'/Timestep/'+nth,[zi,0,xi],[zn,NYn,xn])

    def change_resolution_xz(self,nameout,newres):
        """ Create a new flow field where quantities are interpolated to a new resolution in the horizontal direction.

        **Args:**
           nameout : str
              Name of the new file
           newres : array, int
              Array [NX,NZ] with the new resolution. 

        .. note:: This might takes long time for big flow fields. Varying *NXc* and *NZc* can have significant impact on performances. 

        *Usage*::
        
            # Import first file
            flow=diablo_pack.flow.FlowDiablo('out.h5','grid.h5')
            # New resolution (double in both directions) 
            NXnew=flow.Resolution[0]*2
            NZnew=flow.Resolution[2]*2
            # Create new file interpolated to new grid
            flow.change_resolution_xz('new.h5',[NXnew,NZnew])

        """


        NX=self.Resolution[0]
        NY=self.Resolution[1]
        NZ=self.Resolution[2]

        DataIn=np.zeros(NZ,NX)
        DataUt=np.zeros([newres[1],1,newres[0]])

        # Delete file if necessary 
        if os.path.exists(nameout):
            os.remove(nameout)

        # Create the file
        h5.h5writeatt(nameout,'/','Resolution',[newres[0],self.Resolution[1],newres[1]],dtype='<i4')
        h5.h5writeatt(nameout,'/Timestep','Time',self.t,dtype='<f8')
        
        
        for nth in self.Fields:
            print( " Computing " + nth + " ...")
            # First create the dataset with the right dimensions
            fid = h5.h5py.File(nameout,'a')
            dset = fid.create_dataset('/Timestep/' + nth,[newres[1],NY,newres[0]])
            fid.close()

            for yi in np.arange(NY):
                DataIn=h5.h5read(self.filename,'/Timestep/'+nth,[0,yi,0],[NZ,1,NX])
                DataUt=interpolate_fft(DataIn,[newres[1],newres[0]]).reshape( [newres[1],1,newres[0]])
                h5.h5write(nameout,DataUt.real,'/Timestep/'+nth,[0,yi,0],[newres[1],1,newres[0]])


        
def GetStatistics(u,v,w,th,dig):
    """ Calculates the statistics averaging on the points where ``dig=1``
    
    **Args:**
       u : array, double 
          2D xz-plane with streamwise velocity 
       v : array, double 
          2D xz-plane with vertial velocity 
       w : array, double 
          2D xz-plane with spanwise velocity 
       th : array, double 
          2D xz-plane with temperature
       dig : array, double
          2D array with the points that are used in the averaging. dig has value of 1 (averaging points) and 0 (points to be discarded)
    
    """

    out=dict()
    if np.sum(dig)==0: 
        nEl=1; 
    else: 
        nEl=np.sum(dig);
    
    out['ume']  = np.sum(u*dig)  /nEl
    out['vme']  = np.sum(v*dig)  /nEl
    out['wme']  = np.sum(w*dig)  /nEl
    out['thme'] = np.sum(th*dig) /nEl

    out['uu']  = np.sum(u**2*dig)/nEl-out['ume']**2
    out['vv']  = np.sum(v**2*dig)/nEl-out['vme']**2
    out['ww']  = np.sum(w**2*dig)/nEl-out['wme']**2

    out['urms']  = np.sqrt(out['uu'])
    out['vrms']  = np.sqrt(out['vv'])
    out['wrms']  = np.sqrt(out['ww'])

    out['thth']  = np.sum(th**2*dig)/nEl-out['thme']**2
    
    out['thrms'] = np.sqrt(out['thth'])

    out['uv']  = np.sum(u*v*dig) /nEl-out['ume']*out['vme']
    out['uw']  = np.sum(u*w*dig) /nEl-out['ume']*out['wme']
    out['vw']  = np.sum(v*w*dig) /nEl-out['vme']*out['wme']

    out['thv']  = np.sum(th*v*dig) /nEl-out['thme']*out['vme']

    return out


def get_spectrum_2d(filename,yi,nVar1,nVar2,ri):
    """ Get the two-dimensional streamwise spectra (or co-spectra) at a certain height for the flow field stored in *filename*.

        **Args:**
           filename : str
              Name of the file
           yc : double 
              Vertical height at which the spectra is calculated
           nVar1 : str
              Name of the first variable 
           nVar2 : str
              Name of the second variable 
           fileut : str, opt
              Name of the output file 
           ri : str
              Flag which can be *real* or *imag* in order to write out only either the real part of the imaginary part of *nVar1*conj(nVar2)*

        .. note:: The contribution in the negative wavenumbers are mirrored to the positive wavenumbers.

    """
    from time     import time       as ctim

    NX=h5.h5readatt(filename,'/','Resolution',ishow=False)[0]  
    NZ=h5.h5readatt(filename,'/','Resolution',ishow=False)[0]  

    uu1=np.zeros([NZ,NX])+1j*np.zeros([NZ,NX])
    uu1=h5.h5read(filename,'/Timestep/'+nVar1,[0,yi,0],[NZ,1,NX])
    uu1=np.fft.fftn(uu1).squeeze()/(NX*NZ)

    uu2=np.zeros([NZ,NX])+1j*np.zeros([NZ,NX])
    if nVar1 is nVar2:
        uu2=uu1;
    else:
        uu2=h5.h5read(self.filename,'/Timestep/'+nVar2,[0,yi,0],[NZ,1,NX])
        uu2=np.fft.fftn(uu2).squeeze()/(NX*NZ)

    if ri is 'real' :
        tmp=np.real(uu1*np.conj(uu2))
    elif ri is 'imag':
        tmp=np.imag(uu1*np.conj(uu2))

    spe2d=np.zeros([NZ/2,NX/2]);spe2d[0,0]=tmp[0,0];
    spe2d[0,1:NX/2]=tmp[0,1:NX/2]+np.flipud(tmp[0,NX/2+1:]);
    spe2d[1:NZ/2,0]=tmp[1:NZ/2,0]+np.flipud(tmp[NZ/2+1:,0]);

    spe2d[1:NZ/2,1:NX/2]=tmp[1:NZ/2,1:NX/2]+np.fliplr(np.flipud(tmp[NZ/2+1:,NX/2+1:]))+np.fliplr(tmp[1:NZ/2,NX/2+1:])+np.flipud(tmp[NZ/2+1:,1:NX/2]);

    return spe2d



