""" containing general useful functions for post-processing DIABLO simulations.

.. currentmodule:: diablo_pack.func_dia

Functions
=========

.. autofunction:: GetResolution

.. autofunction:: ReadGrid

.. autofunction:: ave_one_side

.. autofunction:: interpolate_fft

.. autofunction:: joinStat

.. autofunction:: spectral_derivative

.. autofunction:: writeFile


"""

import numpy as np

import h5

def ReadGrid(gridfile):
    """ Returns a tuple with the full grid and the fractional grid contained in *gridfile*.

    **Args:**
        gridfile : str
           Name of the file containing the grid in the vertical 
           
        
    *Returns*
        (y,yf) where *y* is the full grid and *yf* is the fractional grid

    """
    # Import from grid.h5 
    y=h5.h5read(gridfile,'/grids/y')
    NY=np.size(y)-1;
    yf=np.zeros(NY);
    for yi in np.arange(NY):
        yf[yi]=0.5*(y[yi]+y[yi+1])
    return (y,yf)  

def GetResolution(sDir,parfile='grid_def'):
    """ Get the number of points specified by *sDir* 

    **Args:**
       sDir : str
          String specifying the information to be retrieved (e.g. 'NX')
       parfile : str
          grid_def file

    """
    with open(parfile,'r') as fid:
        tmp=fid.readlines()
        for nStr in tmp:
            if (sDir + '=') in nStr:
                ist = nStr.find(sDir + '=')
                ied = nStr.find(')'   ,ist)
                out = np.int(nStr[ist:ied].replace(sDir + '=', ''))
        
    if ist+ied > 0:
        return out
    else:
        raise Exception(" ERROR! " + sDir + " not found.")
    

def writeFile(data,filename='out.txt'):
    """ Writes out multi-dimensional data to a file  

    General framework to write out a *n*-dimensional array to a ASCII file. *n* need not to be specified. 

    **Args:**
       data : array, double 
           n-dimensional array to be exported
       filename : str
           name of the output file


    .. note:: Data can then be imported into MATLAB using iDataUtPython.m function.

    """

    # Reshape data
    Nel=data.shape[1]
    for ith in np.arange(2,len(data.shape)): Nel=Nel*data.shape[ith]

    fid=open(filename,'w')

    for ith in data.shape: fid.write(str(ith) + '\t')
    fid.write('\n')
    
    data=data.reshape([data.shape[0],Nel]);

    for ith in np.arange(Nel):
        for jth in np.arange(data.shape[0]): fid.write(str(data[jth,ith])+'\t')
        fid.write('\n')
                                                          
    fid.close()


def joinStat(prefix='mean',NSMAX=np.inf):
    """ Merges parallel statistics files into one file to be read with :class:`~diablo_pack.stats.StatsDiablo`.

    **Args:**
       prefix : str
          Prefix of the files to be merged (e.g. 'mean' to merge the files into 'mean.txt')
       NSMAX : int
          Maximum number of time samples to be included. 

    *Usage*::
       
        # join velocity statistics
        joinStat('mean')
        # join temperature statistics
        joinStat('mean_th')
        # join kinetic energy statistics
        joinStat('mean_ek')

    """
    import re
    import os

    # Get the name of the files prefix*
    myFiles = [f for f in os.listdir('.') if re.match(prefix + '[0-9]+.txt', f)]
    nameut=re.sub('[0-9]','',myFiles[0])
    if os.path.exists(nameut): 
        os.remove(nameut)
        #myFiles.remove(nameut)
    myFiles.sort()

    # Number of files (thus processes used)
    NP=np.size(myFiles)
    # Number of line in the files
    NL=sum(1 for line in open(myFiles[0]))
    # Number of points in the vertical 
    NY=GetResolution('NY','grid_def')
    
    NSAMPLES=NL/(NY+2)
    UBULK=0.0

    # Opening files
    ifid=list()
    for iFile in myFiles:
        ifid.append(open(iFile,'r'))
    ufid = open(nameut,'w')

    for iSamp in np.arange(np.min([NSAMPLES,NSMAX])):
        UBULK=0.
        for iFile in myFiles:
            DataIn=ifid[myFiles.index(iFile)].readline()
            if myFiles.index(iFile) == 0:
                ufid.write(DataIn)
            DataIn=ifid[myFiles.index(iFile)].readline()
            
            try: 
                UBULK=UBULK+np.double(DataIn.strip())
            except:
                print( ' Corrupted file  : ' + iFile )
                print( ' Corrupted sample: ' + str(iSamp))
                raise SystemExit(iSamp)
            if myFiles.index(iFile) == NP-1:
                ufid.write(str(UBULK) + '\n')

        for iFile in myFiles:
            # Write the point NY if we are the last proc
            DataIn=ifid[myFiles.index(iFile)].readline()
            
            if myFiles.index(iFile) == 0:
                ufid.write(DataIn)
            # Write the different lines
            for iNY   in np.arange(NY-1):
                DataIn=ifid[myFiles.index(iFile)].readline()
                ufid.write(DataIn)
            
    # Close files
    for iFile in ifid: iFile.close()
    ufid.close()


def ave_one_side(yf,var,sym=1):
    """ Returns an average of the two sides of the channel

    **Args:**
        yf : array, double
            Y-Grid (which ought to be symmetric with respect to 0)
        var : array, double
            Quantity to be averaged
        sym : int
            Symmetry flag:
                1. "+1"  Symmetric function
                2. "-1"  Anti-symmetric function 

    *Returns*

        (yf,var)  where *yf* is the grid on half channel and *var* is the averaged variable. 
    """

    N=yf.shape[0]
    yf =np.reshape(yf ,N)
    var=np.reshape(var,N)
    maxErr=1E-8
    if any(abs(yf[:N/2+1]+np.flipud(yf[-N/2:]))>maxErr):
        print( " Grid not symmetric ")
        raise SystemExit(0)
    else:
        varUt=0.5*(var[:N/2+1] + sym*np.flipud(var[-N/2:]))
        yfUt =1-abs(yf [:N/2+1]) 
    return (yfUt,varUt)




def spectral_derivative(x,fx):
    """ Compute the derivative in spectral space

    **Args:**
       x : array, double 
          equi-spaced grid
       fx : array, double 
          function values
          
    *Returns*
       dfdx : array, double 
          Derivative at the *x* points

    """
    import mypy

    # Check that the size/shape is the same for x and fx
    if not x.shape == fx.shape:
        print( " x and fx vector have two different shapes/sizes ")
        raise SystemExit(0)
    dims=len(x)

    # Check that the x are equispaced and get extent
    if np.var(np.diff(x))>1E-10:
        print( "No regularly-spaced grid")

    # Generates wavenumbers
    kip=2*np.pi/x[-1]*np.arange(dims/2)
    kin=2*np.pi/x[-1]*np.arange(-dims/2,0)
    ki=mypy.vectorize([kip,kin])

    dfx=np.fft.ifft(1j*ki*np.fft.fft(fx))
    if not any(np.iscomplex(fx)):
        dfx=dfx.real
    return dfx
    
def interpolate_fft(DataIn,shapeOut):
    """ Interpolates values in a 2D matrix to a new grid with dimensions given by *shapeOut*

    **Args:**
       DataIn : array, double 
          2D array with data in a equidistant grid
       shapeOut : array, int
          [N0,N1] array specifying the new dimension of the new array

    *Returns*
       DataOut : array, double 
          2D array of dimension *shapeOut[0] x shapeOut[1]*

    """

    import operator
    shapeIn=np.array(DataIn.shape)
    shapeOut=np.array(shapeOut)
    DataUt=np.zeros(shapeOut)+1j*np.zeros(shapeOut)
    tmp=np.fft.fftshift(np.fft.fftn(DataIn))
    ist=(shapeOut-shapeIn)/2
    DataUt[ist[0]:ist[0]+shapeIn[0],ist[1]:ist[1]+shapeIn[1]]=tmp
    return np.fft.ifftn(np.fft.ifftshift(DataUt))*reduce(operator.mul,shapeOut)/reduce(operator.mul,shapeIn)

