""" For handling a DIABLO run. 

In includes the :class:`~diablo_pack.run.RunDiablo` class which handle the interaction with the run as well as useful functions to be used within the folder of a DIABLO run.

.. currentmodule:: diablo_pack.run

Classes
--------

.. autoclass:: RunDiablo
   :members:

Functions
---------

"""

import numpy as np

import os

from diablo_pack.flow      import FlowDiablo
from diablo_pack.stats     import StatsDiablo
from diablo_pack.stats_ret import StatsRetDiablo

from diablo_pack.func_dia  import GetResolution

class RunDiablo():
    """ Object to control and handle operations of a single run in Diablo

    **Attributes**
        BoxSize : array, double
           Array with the dimension of the box
        Re : double
           Bulk Reynolds number
        Ri : double
           Bulk Richardson number
        Pr : double
           Prandtl number
        Resolution : array, int
           Array with the number of points
        home : str
           String with the path of the folder
        st : StatsDiablo
           Object containing all the statistics (available after calling the read_stat method)
        flow : dict, FlowDiablo
           Dictionary including FlowDiablo objects ordered by the name of the flow fields (available after calling the read_flow method)

    *Usage*::

        s=diablo_pack.run.RunDiablo('grid_def','input.dat')
        # To then create the s.runs list with the different runs
        s.read_stat()

    """
    def __init__(self,parfile='grid_def',simfile='input.dat'):
        """ Initialize the object.

        **Args:**
           parfile : str, opt
              name of the grid including the full grid (it is good convention in parallel simulation to call grid_def the grid file used to compile and grid_def.all the grid file having in NY the full number of points
           simfile : str, opt
              name of the input file of DIABLO

        """
        # Check if the file exists
        if not os.path.exists(parfile):
            print(" ")
            print(" File " + parfile + " does not exist in the current folder")
            print(" ")
            raise SystemExit(0)
        if not os.path.exists(simfile):
            print(" ")
            print(" File " + simfile + " does not exist in the current folder")
            print(" ")
            raise SystemExit(0)
        
        self.parfile=parfile;

        self.home=os.getcwd();

        # Resolution
        self.Resolution=np.zeros(3)
        self.Resolution[0]=GetResolution('NX',parfile=parfile)
        self.Resolution[1]=GetResolution('NY',parfile=parfile)
        self.Resolution[2]=GetResolution('NZ',parfile=parfile)

        # Simulations parameter
        with open(simfile,'r') as fid:
            tmp=fid.readlines()

        dtmp=[np.double(x) for x in tmp[8].split()]
        self.Re      = 1./dtmp[0]
        self.BoxSize = dtmp[1:4]

        dtmp=[np.double(x) for x in tmp[21].split()]
        self.Ri      = dtmp[0]
        self.Pr      = dtmp[1]
        
        if not os.path.exists('grid.h5'):
            print(" ")
            print(" Grid file grid.h5 not found")
            print(" ")
            raise SystemExit(0)
 
        self.gridfile='grid.h5'
        
        self.flow    = {}

    def read_flow(self,filename):
        """ Create a new :class:`~diablo_pack.flow.FlowDiablo` object and store it in a dictionary with the name of the file.
        
        **Args:**
            filename : str
                Name of the file to be read

        *Usage*::

           # Read the header of the flow
           r.read_flow('end.h5')
           # Read U velocity field
           r.flow['end.h5'].rFlowField('U')

        """
        my_path=os.getcwd();
        os.chdir(self.home);
        self.flow[filename]=FlowDiablo(filename,self.BoxSize,gridfile=self.gridfile);
        os.chdir(my_path);
        
    def lam_turb_stat(self):
        """ Compute (or read it from mean.txt.lam and mean.txt.turb if the files already exists) the laminar and turbulent statistics and create the :class:`~diablo_pack.stats.StatsDiablo` objects st_lam and st_tur. 
        
        """
        my_path=os.getcwd();
        os.chdir(self.home);

        if (not os.path.exists('mean.txt.lam'))  or (not os.path.exists('mean_th.txt.lam')):
            for iname in ['out.h5','end.h5']:
                if os.path.exists(iname):
                    self.read_flow(iname);
                    self.flow[iname].LamTurbStats(lim=lim);

        if self.Re>1:
            self.st_lam=StatsDiablo('mean.txt.lam','mean_th.txt.lam',Re=self.Re,Ri=self.Ri,Pr=self.Pr,grid_def=self.parfile)
            self.st_tur=StatsDiablo('mean.txt.tur','mean_th.txt.tur',Re=self.Re,Ri=self.Ri,Pr=self.Pr,grid_def=self.parfile)
        else:
            self.st_lam=StatsRetDiablo('mean.txt.lam','mean_th.txt.lam',Ret=1./self.Re,Ri=self.Ri,Pr=self.Pr,grid_def=self.parfile)
            self.st_tur=StatsRetDiablo('mean.txt.tur','mean_th.txt.tur',Ret=1./self.Re,Ri=self.Ri,Pr=self.Pr,grid_def=self.parfile)
        os.chdir(my_path);
        

    def read_stat(self,filename='mean.txt',filename_th='mean_th.txt',update=False):
        """ Import the statistics and create a :class:`~diablo_pack.stats.StatsDiablo` object. 

        **Args:**
            filename : str
               Name of the file for the velocity statistics
            filename_th : str
               Name of the file for the scalar statistics
            update : bool
              Forces to recompute the merged statistics file, even if it already exists.

        """

        my_path=os.getcwd();
        os.chdir(self.home);
        if os.path.exists(filename) or os.path.exists(filename_th):
            self.join_stats(update=update)

        if self.Re>1:
            self.st=StatsDiablo(filename,filename_th,Re=self.Re,Ri=self.Ri,Pr=self.Pr,grid_def=self.parfile)
        else:
            self.st=StatsRetDiablo(filename,filename_th,Ret=1./self.Re,Ri=self.Ri,Pr=self.Pr,grid_def=self.parfile)
        os.chdir(my_path);
      
    def join_stats(self,update=False):
        """ To be used for parallel runs. 

        Merge the files produced by each process (or communicator along Z-direction for 2d parallelisation) in order to obtain a compatible statistic file to be import in a :class:`~diablo_pack.stats.StatsDiablo` object.

        **Args**:
           update : bool
              Forces to recompute the merged statistics files, even if they already exist.

        """

        from func_dia import joinStat as joinStat
        import re

        my_path=os.getcwd();
        # Go to the run folder
        os.chdir(self.home);

        # if (update is False               and  
        #     os.path.exists('mean.txt')    and  
        #     os.path.exists('mean_th.txt')     ):
        #     os.chdir(my_path);
        #     return
        
        #try:
        ListFiles=['mean','mean_th','mean_ek','mean_ep','tke','vort_full','vort_frac']
        for iFile in ListFiles:
            for filename in os.listdir('.'):
                if re.match(iFile + '[0-9]+.txt',filename):
                    joinStat(iFile)
                    break 
        # except SystemExit as iRecord:
        #     print ' ---> Problem on reading run ' + self.home
        #     print iRecord.code
        #     joinStat('mean'   ,NSMAX=iRecord.code-1)
        #     joinStat('mean_th',NSMAX=iRecord.code-1)
                        
        # Go back 
        os.chdir(my_path);
        return


