#!/urs/bin/python

import sys
sys.path.append('/cfs/klemming/nobackup/d/deusebio/lib/h5py-2.1.0/lib/python2.6/site-packages/')

import numpy             as np
from   numpy import pi   as pi
import h5py

def h5disp(filename):
    fid = h5py.File(filename,'r')
    showContent(fid)
    fid.close()

def showContent(hmem,sindent=""):

    sindent = sindent + "     "
    
    print( sindent + " ")
    print( sindent +" Attributes ")
    print( sindent +" ---------------------")
    print( sindent +" ")

    for attName,attValue in hmem.attrs.items():
        print( sindent + str(attName) + ':    ' + str(attValue))

    print( sindent + " ")
    print( sindent + " Objects ")
    print( sindent + " ---------------------")
    print( sindent + " ")

    for nameObj,typeObj in hmem.items():
        print( sindent + str(nameObj) + ':    ' + str(typeObj))
        print( sindent + " --------> ")
        gid = hmem.get(nameObj)
        if not hasattr(gid,'dtype'):
            showContent(gid,sindent)

    print( sindent + " ")


def h5GetListObj(filename,nameGr):
    fid = h5py.File(filename,'r')
    gid = fid.get(nameGr)
    myList=gid.keys()
    fid.close()
    return myList


def h5GetListAtt(filename,nameGr):
    fid = h5py.File(filename,'r')
    gid = fid.get(nameGr)
    myList=gid.attrs.keys()
    fid.close()
    return myList
    

def h5readatt(filename,nameGr,nameAtt,ishow=True):
    fid = h5py.File(filename,'r')
    gid = fid.get(nameGr)
    val = gid.attrs.get(nameAtt)

    if (ishow):
        print( " ")
        print( nameAtt + ' :    ' + str(val))

    fid.close()
    return val

def h5writeatt(filename,nameGr,nameAtt,val,dtype='>f8'):
    fid = h5py.File(filename,'a')

    gid = fid.get(nameGr)
    if gid==None:
        gid=fid.create_group(nameGr)
    if not nameAtt in gid:
        gid.attrs.create(nameAtt,val,dtype=dtype)
    else:
        gid.attrs[nameAtt]=val

    fid.close()


def h5read(filename,nameDset,start=None,count=None,stride=None):
    fid = h5py.File(filename,'r')

    val = fid[nameDset]
    if start is None:
        start=[0 for x in val.shape]
    if count is None:
        count=[x for x in val.shape]
    if stride is None:
        stride=[1 for x in val.shape]

    SEL=[]
    for ith in np.arange(np.alen(start)): 
        SEL.append(slice(start[ith],start[ith]+count[ith],stride[ith]))

    Data = val[:]
    out  = Data[SEL]

    fid.close()

    return out.squeeze()



def h5write(filename,VarIn,nameDset,start=None,count=None,stride=None,
            dtype='>f8'):
    fid = h5py.File(filename,'a')

    # Check if the dataset exist
    if not nameDset in fid:
        dset = fid.create_dataset(nameDset, VarIn.shape,dtype)
    else:
        dset = fid[nameDset]

    if start is None:
        start=[0 for x in dset.shape]
    if count is None:
        count=[x for x in dset.shape]
    if stride is None:
        stride=[1 for x in dset.shape]

    SEL=[]
    for ith in np.arange(np.alen(start)): 
        SEL.append(slice(start[ith],start[ith]+count[ith],stride[ith]))

    dset[tuple(SEL)]=VarIn.reshape(dset[tuple(SEL)].shape)

    fid.close()

#     dset

#     Data = val[:]
#     out  = Data[SEL]

#     fid.close()

#     return out.squeeze()

def h5del(filename,nameDset):

    import os

    fid = h5py.File(filename,'a')

    del fid[nameDset]

    fid.close()

    os.system('h5repack -i %s -o temp.h5' % filename)
    os.system('mv temp.h5 %s ' % filename)
    
