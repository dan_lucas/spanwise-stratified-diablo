#!/bin/python

import numpy as np

from struct import pack
from struct import unpack

def rbinfile(fid,itype,nn=1,iend='<'):
                
    import struct

    if itype is 'd':
        nByt=8
    elif itype is 'i':
        nByt=4
    elif itype is 'c':
        nByt=1

    chk = fid.read(nByt*nn)
    out = struct.unpack(iend + repr(nn) + itype, chk)
    return np.array(out)
        

def wbinfile(data,fid,itype):
                
    import struct
    
    if itype is 'd':
        nByt=8
    elif itype is 'i':
        nByt=4
    elif itype is 'c':
        nByt=1

    nn=np.size(data)
    data=np.reshape(data,nn)
    out = struct.pack('>' + repr(nn) + itype, *data)
    chk = fid.write(out)

def writeFileColumns(namefile,Variables):
    # Check consistency
    nLines = Variables[list(Variables.keys())[0]].size
    for iField in Variables.keys():
        if Variables[iField].size != nLines:
            print(" Object " + iField + ' not consistent.')
            raise Error
        
    with open(namefile,'w') as fid:
        for yi in np.arange(nLines):
            out=''
            for iField in Variables.keys():
                out=out+str(Variables[iField][yi])+'\t'
            out=out+'\n'
            fid.write(out)

def writeLatexTable(variables,fields=None,idisp=False):
    if idisp is False:
        import sys
        sys.path.append('/Users/deusebio/PhD/lib/python/packages/pyperclip-1.5.4/')
        import pyperclip

    if fields is None: 
        fields=list(variables.keys());

    # Check consistency
    nLines = variables[fields[0]].size
    for iField in fields:
        if variables[iField].size != nLines:
            print(" Object " + iField + ' not consistent.')
            raise Error
    
    strout=""
    # Start by putting the header
    strout=strout+"\\begin{table}\n"
    strout=strout+"\\begin{center}\n"
    strout=strout+"\\def~{hphantom{0}}\n"
    strout=strout+"\\begin{tabular*}{0.9\\textwidth}{@{\\extracolsep{\\fill}} l"
    for iField in fields[1:]:
        strout=strout+" c "
    strout=strout+"}\n"

    # Name of the columns
    strout=strout + str(iField[0]);
    for iField in fields[1:]:
        strout=strout + " &  " + str(iField);
    strout=strout+" \\\\ \n";

    # Put the data
    for yi in np.arange(nLines):
        strout=strout + str(variables[iField[0]][yi]);
        for iField in fields[1:]:
            strout=strout+" & "+str(variables[iField][yi]);
        strout=strout+" \\\\ \n"

    # Put the footer
    strout=strout+"\\end{tabular*}\n"
    strout=strout+"\\caption{Write here the caption}\n"
    strout=strout+"\\label{tab:table}\n"
    strout=strout+"\\end{center*}\n"
    strout=strout+"\\end{table*}\n"
    
    if idisp is True:
        print strout
    else:
        pyperclip.copy(strout)
#     with open(namefile,'w') as fid:
#         for yi in np.arange(nLines):
#             out=''
#             for iField in Variables.keys():
#                 out=out+str(Variables[iField][yi])+'\t'
#             out=out+'\n'
#             fid.write(out)


            
class readFileColumns:

    def __init__(self,filename,Variables=None,headers=0,chrsplit='\t'):

        if Variables==None:
            with open(filename,'r') as fid:
                # Read the headers
                for ith in np.arange(headers):
                    line=fid.readline()
                line=fid.readline()
                Nc=np.shape(line.strip().split(chrsplit))[0]
                Variables={}
                for ith in np.arange(Nc):
                    Variables[ith]=ith
        self.Variables=Variables
        self.fileName=filename
        self.Nrows=file_len(self.fileName)
        self.Data={}
        self.headers=list()
        for nVar in Variables.values():
            #exec('self.'+nVar+'=np.zeros('+str(self.Nrows)+')')
            self.Data[nVar]=np.zeros(self.Nrows-headers)
            
        with open(self.fileName,mode='r') as fid:
            for nLine in np.arange(self.Nrows):    
                line=fid.readline()
                if nLine < headers:
                    self.headers.append(line)
                else:
                    In=map(float,line.split())
                    for nVar in Variables.iteritems():
                        self.Data[nVar[1]][nLine-len(self.headers)]=In[np.int(nVar[0])]

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
        return i + 1


def get_data_figure(fh,figurename='myfig',izfill=0,rewrite=False):
    from os import mkdir
    from os import chdir   as cd
    from shutil import rmtree  as rmdir
    from os.path import exists as exist
    
    myAxis=fh.get_axes()
    
    if exist(figurename):
        if rewrite==False:
            print(" Folder " + figurename + " already exists!")
            raise 
        else:
            rmdir(figurename)

    mkdir(figurename)
    cd(figurename)
    
    Data={}

    for iAxis in myAxis:
        axFold='subplot'+str(myAxis.index(iAxis)).zfill(izfill)
        mkdir(axFold)
        cd(axFold)
        myLines=myAxis[0].lines
        
        for iLine in myLines:
            lineNamn='line'+str(myLines.index(iLine)).zfill(izfill)
            Data[0]=iLine.get_xdata()
            Data[1]=iLine.get_ydata()
            writeFileColumns(lineNamn,Data)
        
        cd('..')
        
    cd('..')
        
