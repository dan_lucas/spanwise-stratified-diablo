#!/bin/python

import sys
sys.path.append('/Users/deusebio/PhD/prog/fortran/diablo/diablo/post_process/python')

import visit as vi

import diablo 

class VisIt:
    def __init__(self,filename,host=None,noWindow=False):
        self.filename=filename
        path_to_file=self.filename
        if host is not None:
            self.host=host
            path_to_file=self.host+':'+path_to_file
        if noWindow:
            vi.LaunchNowin()
        else:
            vi.Launch()
        vi.OpenDatabase(path_to_file)

    def __del__(self):
        vi.Close()

    def plotxz(self,nVar,yc):
        out=SliceXZ(nVar,yc)
        return out

    def saveImage(self,filename=None,format=None):
        # Set the save window attributes.
        s = vi.SaveWindowAttributes()

        if filename is not None:
            s.fileName = filename
            s.family = 0
            s.progressive = 1

        if format is not None:
            exec('s.format = s.' + format )
        
        vi.SetSaveWindowAttributes(s)
        vi.SaveWindow()



class SliceXZ:
    def __init__(self,nVar,yc,cmap="jet"):
        vi.AddPlot("Pseudocolor", "Timestep/" + nVar)
        vi.AddOperator("Slice")

        # -----------------
        # p attributes
        # -----------------
        p = vi.PseudocolorAttributes()
        p.colorTableName = cmap
        p.opacity = 0.5
        vi.SetPlotOptions(p)
        self.p=p

        # -----------------
        # a attributes
        # -----------------
        a = vi.SliceAttributes()
        a.SetNormal((0,-1,0))
        a.SetUpAxis((0,0,1))
        a.SetOriginIntercept(yc)
        a.GetProject2d(1)
        vi.SetOperatorOptions(a)
        self.a=a

        vi.DrawPlots()

    def change_yc(self,yc):
        a = self.a
        a.SetOriginIntercept(yc)
        a.GetProject2d(1)
        vi.SetOperatorOptions(a)
        vi.DrawPlots()

    def caxis(self,crange):
        p = self.p
        p.SetMinFlag(1)
        p.SetMin(crange[0])
        p.SetMaxFlag(1)
        p.SetMax(crange[1])
        vi.SetPlotOptions(p)
