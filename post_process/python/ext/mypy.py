#!/bin/python

import sys
sys.path.append('/cfs/klemming/nobackup/d/deusebio/Ekman/python/')
sys.path.append('/afs/pdc.kth.se/home/d/deusebio/Private/lib/python2.6/site-packages/')

import numpy as np

def get_whos(my_string,my_list_obj):
    import IPython.core.ipapi
    import re

    ipython=IPython.core.ipapi.get()
    my_list=ipython.magic('who_ls')

    my_sel =[nth for nth in my_list if re.match(my_string,nth) if np.diff(re.match(my_string,nth).span()) == len(nth) ]
    print(my_sel)
    my_obj = list()
    for nth in my_sel:
        my_obj.append(my_list_obj[nth]) 
    return my_obj

def vectorize(A):
    out=list()
    for nth in A:
        if np.isscalar(nth):
            out=out+list([nth])
        else:
            out=out+list(nth)
        
    out=np.array(out)
    out=np.reshape(out,out.size)
    return out


def dfdy(y,f,iMethod='FiniteDifferences'):
    # Derivative at central points
    if iMethod is 'FiniteDifferences':
        df1 = np.diff(f)*1./np.diff(y)
        y1  = (y[0:-1] + y[1:])*0.5
        df  = np.interp(y,y1,df1)
    elif iMethod is 'SpectralCheb':
        print('Not implemented yet')
        
    return df


def smooth(x,window_len=11,window='hanning'):
    """smooth the data using a window with requested size.
    
    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal 
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.
    
    input:
        x: the input signal 
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal
        
    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)
    
    see also: 
    
    np.hanning, np.hamming, np.bartlett, np.blackman, np.convolve
    scipy.signal.lfilter
 
    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise ValueError ("Input vector needs to be bigger than window size.")


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError ("Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")


    s=np.r_[x[window_len-1:0:-1],x,x[-1:-window_len:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=np.ones(window_len,'d')
    else:
        w=eval('np.'+window+'(window_len)')

    y=np.convolve(w/w.sum(),s,mode='valid')
    return y[(window_len-1)/2:-(window_len-1)/2]

