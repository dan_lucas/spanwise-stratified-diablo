#!/usr/bin/env python
#coding=utf8

import numpy as np
import matplotlib.pyplot as plt
import commands
import sys

class CreateFigArticles(object):
    """A class for creating figures."""

    def __init__(self, short_name_article=None, 
                 path_save_wo_base=None,
                 SAVE_FIG=False, 
                 FOR_BEAMER=False, 
                 fontsize=18
                 ):

        self.SAVE_FIG = SAVE_FIG

        out = commands.getstatusoutput('hostname')
        self.computer = out[1]

        if self.computer == 'dhcp62.mech.kth.se':
            self.path_base_dir = '/home/deusebio'
        elif self.computer == 'pelvoux':
            self.path_base_dir = '/scratch/augier'
        else:
            raise ValueError('Unknown computer...')

        if short_name_article is None and path_save_wo_base is None:
            raise ValueError(
                'short_name_article or path_save_wo_base has to be given.')
        elif short_name_article is None and path_save_wo_base is not None:
            self.dir_save_fig = self.path_base_dir+'/'+path_save_wo_base
        elif short_name_article == 'SW1l':
            self.dir_save_fig = (
                self.path_base_dir+
                '/Recherche/Articles_en_travaux/AugierLindborgSW1lwaves/Figs'
                )
        else:
            raise ValueError('Bad short_name_article.')


        params = {
            'backend': 'ps',
            'axes.labelsize': fontsize,
            'text.fontsize': fontsize,
            'legend.fontsize': fontsize,
            'xtick.labelsize': fontsize,
            'ytick.labelsize': fontsize,
            'text.usetex': True,
            'font.family': 'serif',
            'font.serif': 'Computer Modern Roman',
            'font.sans-serif': 'Computer Modern Roman',
            'ps.usedistiller': 'xpdf',
         }

        if FOR_BEAMER:
            params['font.family'] = 'sans-serif'
            preamble = r'''\usepackage[cm]{sfmath}'''
            plt.rc('text.latex', preamble=preamble)

        plt.rcParams.update(params)



    def figure_axe(self, num_fig=None, 
                   fig_width_mm=200, fig_height_mm=150,
                   size_axe=None, 
                   name_file=None):

        if size_axe is None:
            size_axe = [0.124, 0.1, 0.85, 0.86]

        one_inch_in_mm = 25.4
        fig_width_inches = float(fig_width_mm)/one_inch_in_mm
        fig_height_inches = float(fig_height_mm)/one_inch_in_mm
        fig_size =  [fig_width_inches,fig_height_inches]

        dpi_latex = 72.27

        fig = plt.figure(num=num_fig, figsize=fig_size, dpi=dpi_latex)

        title = 'Fig '+str(fig.number)

        if name_file is not None:
            fig.name_file = name_file
            title += ' '+name_file

        fig.clf()
        fig.canvas.set_window_title(title)
        ax1 = fig.add_axes(size_axe)
        ax1.hold(True)

        return fig, ax1



    def save_fig(self, name_file=None, SAVE_FIG=None,
                 fig=None, format='eps'):
        if SAVE_FIG is None:
            SAVE_FIG = self.SAVE_FIG

        if fig is None:
            fig = plt.gcf()

        if name_file is None:
            try:
                name_file = fig.name_file
            except AttributeError:
                raise ValueError('No name given...')

        if self.SAVE_FIG:
            name_file = name_file+'.'+format
            print 'Save figure in file '+name_file

            plt.savefig(self.dir_save_fig+'/'+name_file, 
                        format=format)

            # 'pdftops -eps '+self.dir_save_fig+'/'+name_file


    def show(self):
        version = sys.version
        if version[:3]=='2.7':
            plt.show(block=True)
        elif version[:3]=='2.6':
            plt.show()

