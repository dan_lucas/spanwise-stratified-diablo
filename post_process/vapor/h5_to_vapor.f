      program res_to_vapor
! This program converts a diablo restart file to a format readable by vapor
! A specified number of wavelet transforms are performed to produce
! compressed versions of the data 
! Note, as of version 1.1.2, Vapor cannot handle stretched grids

! grid_def should contain the grid size and number of scalars
      include 'header'

      character*55 FNAME
      character*55 FNAME_GRID
      character*55 FNAME_TH(1:N_TH)

! Define Variables
      integer i,j,k,n
      real*4 buffer2(0:NX-1,0:NZ-1,1:NY)
      real*4 buffer3(0:NX-1,0:NZ-1,0:NY-1)
! The sizes of these strings may need to be changed
      character*30 size_str
      character*50 len_str
      character*10 sNX,sNZ,sNY

! **** User Input *****
! Name of restart file(s)
      FNAME='end.h5'
      FNAME_GRID='grid.h5'
      DO N=1,N_TH
        FNAME_TH(N)='diablo_th'
     &        //CHAR(MOD(N,100)/10+48)
     &        //CHAR(MOD(N,10)+48) // '.saved'
      END DO
! Number of periodic directions used in the simulation
      NUM_PER_DIR=2
! This string should contain the size of the buffer array
      
      !
      write(sNX,'(1I8)') NX
      write(sNY,'(1I8)') NY
      write(sNZ,'(1I8)') NZ
      write(size_str,*) trim(adjustl(sNX)),'x',
     &     trim(adjustl(sNZ)),'x',
     &     trim(adjustl(sNY))


      call ReadHDF5BoxSize(FNAME)

      write(sNX,'(1F6.1)') LX
      write(sNZ,'(1F6.1)') LZ
      write(len_str,*) '0.0:0.0:-1.0:',
     &     trim(adjustl(sNX)),':',
     &     trim(adjustl(sNZ)),':1.0'

      write(*,*) 'len_str: ',len_str
      write(*,*) 'LX,sNX: ',LX,sNX
      write(*,*) 'LZ,sNZ: ',LZ,sNZ

!      len_str='0.0:0.0:-1.0:12.8:6.4:1.0'

      NXM=NX-1
      NYM=NY-1
      NZM=NZ-1

      call init_fft
      call ReadHDF5(FNAME)
      call ReadGridHDF5(FNAME_GRID,2)

      if (.FALSE.) then      
      
      WRITE(6,*)   'Reading flow from ',FNAME

      OPEN(UNIT=10,FILE=FNAME,STATUS="OLD",FORM="UNFORMATTED")
      READ (10) NX_T, NY_T, NZ_T, NUM_PER_DIR_T, TIME, TIME_STEP

      IF ((NX .NE. NX_T) .OR. (NY .NE. NY_T) .OR. (NZ .NE. NZ_T)) then
         write(*,*) 'NX,NY,NZ,NX_T,NY_T,NZ_T:',NX,NY,NZ,NX_T,NY_T,NZ_T
         STOP 'Error: old flowfield wrong dimensions. '
      END IF
      IF (NUM_PER_DIR .NE. NUM_PER_DIR_T)
     *     STOP 'Error: old flowfield wrong NUM_PER_DIR. '

      write(*,*) 'READING FLOW'
      IF (NUM_PER_DIR.EQ.3) THEN
        READ (10) (((CU1(I,K,J),I=0,NKX),K=0,TNKZ),J=0,TNKY),
     *            (((CU2(I,K,J),I=0,NKX),K=0,TNKZ),J=0,TNKY),
     *            (((CU3(I,K,J),I=0,NKX),K=0,TNKZ),J=0,TNKY)
        DO N=1,N_TH
          OPEN(UNIT=11,FILE=FNAME_TH(N),STATUS="OLD"
     &           ,FORM="UNFORMATTED")
          READ (11) NX_T, NY_T, NZ_T, NUM_PER_DIR_T, TIME, TIME_STEP
          READ (11) (((CTH(I,K,J,N)
     &           ,I=0,NKX),K=0,TNKZ),J=0,TNKY)
         CLOSE(11)
        END DO
      ELSEIF (NUM_PER_DIR.EQ.2) THEN
        READ (10) (((CU1(I,K,J),I=0,NKX),K=0,TNKZ),J=0,NY),
     *            (((CU2(I,K,J),I=0,NKX),K=0,TNKZ),J=1,NY),
     *            (((CU3(I,K,J),I=0,NKX),K=0,TNKZ),J=0,NY)
        DO N=1,N_TH
          OPEN(UNIT=11,FILE=FNAME_TH(N),STATUS="OLD"
     &           ,FORM="UNFORMATTED")
          READ (11) NX_T, NY_T, NZ_T, NUM_PER_DIR_T, TIME, TIME_STEP
          READ (11) (((CTH(I,K,J,N)
     &           ,I=0,NKX),K=0,TNKZ),J=0,NY)
         CLOSE(11)
        END DO
         OPEN (30,file='./ygrid.txt',form='formatted',status='old')
         READ (30,*) NY_T
C Check to make sure that grid file is the correct dimensions
         IF (NY_T.ne.NY) THEN
           WRITE(6,*) 'NY, NY_T',NY,NY_T
           STOP 'Error: ygrid.txt wrong dimensions'
         END IF         
         DO J=1,NY+1
           READ(30,*) GY(j)
         END DO
         DO J=1,NY
           READ(30,*) GYF(j)
         END DO
         CLOSE(30)
         DO I=0,NX
           GX(I)=(I*LX)/NX
         END DO
         DO K=0,NZ
           GZ(K)=(I*LZ)/NZ
         END DO

      ELSEIF (NUM_PER_DIR.EQ.1) THEN
        READ (10) (((CU1(I,K,J),I=0,NKX),K=1,NZ  ),J=1,NY  ),
     *            (((CU2(I,K,J),I=0,NKX),K=1,NZ  ),J=1,NYM ),
     *            (((CU3(I,K,J),I=0,NKX),K=1,NZM ),J=1,NY  )
      ELSEIF (NUM_PER_DIR.EQ.0) THEN
        READ (10) (((CU1(I,K,J),I=1,NXM),K=1,NZ  ),J=1,NY  ),
     *            (((CU2(I,K,J),I=1,NX ),K=1,NZ  ),J=1,NYM ),
     *            (((CU3(I,K,J),I=1,NX ),K=1,NZM ),J=1,NY  )
      END IF
      CLOSE(10)
      CLOSE(11)

      write(*,*) 'Done reading restart file'

      end if

       IF (NUM_PER_DIR.eq.2) THEN 
       do j=1,NY
         write(22,*) J,GYF(J),CU1(0,0,J)
         U1_BAR(j)=CU1(0,0,J)
         DO N=1,N_TH
           TH_BAR(J,N)=CTH(0,0,J,N)
         END DO
       end do

! Calculate the vorticity
        do j=1,NY
        do k=0,TNKZ
        do i=0,NKX
          CS1(i,k,j)=CIKX(i)*CU3(I,K,J)-CIKZ(k)*CU1(I,K,J)
        end do
        end do
        end do
        write(*,*) 'CIKX(10):',CIKX(10)

        call fft_xz_to_physical(CU1,U1,0,NY+1)
        call fft_xz_to_physical(CU2,U2,0,NY+1)
        call fft_xz_to_physical(CU3,U3,0,NY+1)
        call fft_xz_to_physical(CS1,S1,0,NY+1)
        DO N=1,N_TH
          CALL FFT_XZ_TO_PHYSICAL(CTH(0,0,0,N),TH(0,0,0,N),0,NY+1)
        END DO
        ELSE IF (NUM_PER_DIR.eq.3) THEN
          CALL fft_xzy_to_physical(CU1,U1)
          CALL fft_xzy_to_physical(CU2,U2)
          CALL fft_xzy_to_physical(CU3,U3)
          DO N=1,N_TH
            CALL fft_xzy_to_physical(CTH(0,0,0,N),TH(0,0,0,N))
          END DO
        END IF

! Now we are ready to create a vdf (header) file

      IF (NUM_PER_DIR.eq.2) THEN

      if (N_TH.eq.0) then
        call SYSTEM('vdfcreate -dimension '
     &//trim(size_str)//' -extents '//trim(len_str)//' -periodic 1:1:0
     &    -numts 1 -level 3 -vars3d U1:U2:U3:ELEVATION
     &     vapor.vdf')
      else if (N_TH.eq.1) then
        call SYSTEM('vdfcreate -gridtype layered -dimension '
     &//trim(size_str)//' -extents '//trim(len_str)//' -periodic 1:1:0
     &    -numts 1 -level 3 -vars3d U1:U2:U3:TH1:TH1P:DW:OMEGA:ELEVATION
     &      vapor.vdf')
      else if (N_TH.eq.2) then 
        call SYSTEM('vdfcreate -gridtype layered -dimension '
     &//trim(size_str)//' -extents '//trim(len_str)//' -periodic 1:1:0
     &    -numts 1 -level 3 -vars3d U1:U2:U3:TH1:TH2:TH1P:ELEVATION
     &      vapor.vdf')
      else if (N_TH.eq.3) then
        call SYSTEM('vdfcreate -gridtype layered -dimension '
     &//trim(size_str)//' -extents '//trim(len_str)//' -periodic 1:1:0
     &    -numts 1 -level 3 -vars3d 
     &     U1:U2:U3:TH1:TH2:TH3:TH1P:ELEVATION
     &      vapor.vdf')
      end if

      do i=0,NX-1
      do j=1,NY
      do k=0,NZ-1
        buffer2(i,k,j)=GYF(J)*1.d0
      end do
      end do
      end do

!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer2

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY 
        write(22,REC=j) buffer2(0:NX-1,0:NZ-1,j)
      end do

!      open (22,file='temp.raw',form='UNFORMATTED')
!      write(22) buffer2
      close(22)

      call SYSTEM('raw2vdf -ts 0 -varname ELEVATION vapor.vdf
     &  temp.raw')


      ELSE IF (NUM_PER_DIR.eq.3) THEN


      if (N_TH.eq.0) then
        call SYSTEM('vdfcreate -dimension '
     &//trim(size_str)//' -extents '//trim(len_str)//' -periodic 1:1:1
     &    -numts 1 -level 3 -varnames U1:U2:U3
     &     vapor.vdf')
      else if (N_TH.eq.1) then
        call SYSTEM('vdfcreate -dimension '
     &//trim(size_str)//' -extents '//trim(len_str)//' -periodic 1:1:1
     &    -numts 1 -level 3 -varnames U1:U2:U3:TH1
     &      vapor.vdf')
      else if (N_TH.eq.2) then 
        call SYSTEM('vdfcreate -dimension '
     &//trim(size_str)//' -extents '//trim(len_str)//' -periodic 1:1:1
     &    -numts 1 -level 3 -varnames U1:U2:U3:TH1:TH2
     &      vapor.vdf')
      else if (N_TH.eq.3) then
        call SYSTEM('vdfcreate -dimension '
     &//trim(size_str)//' -extents '//trim(len_str)//' -periodic 1:1:1
     &    -numts 1 -level 3 -varnames U1:U2:U3:TH1:TH2:TH3
     &      vapor.vdf')
      end if
      END IF 



      IF (NUM_PER_DIR.eq.2) THEN
      do i=0,NX-1
      do j=1,NY
      do k=0,NZ-1
        buffer2(i,k,j)=real(U1(i,k,j))
      end do
      end do
      end do
!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer2

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY 
        write(22,REC=j) buffer2(0:NX-1,0:NZ-1,j)
      end do

      close(22)

      ELSE IF (NUM_PER_DIR.eq.3) THEN
      do i=0,NX-1
      do j=0,NY-1
      do k=0,NZ-1
        buffer3(i,k,j)=real(U1(i,k,j))
      end do
      end do
      end do
!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer3

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY-1 
        write(22,REC=j) buffer3(0:NX-1,0:NZ-1,j)
      end do

      close(22)
      END IF
 

      call SYSTEM('raw2vdf -ts 0 -varname U1 vapor.vdf temp.raw')

      IF (NUM_PER_DIR.eq.2) THEN

      do i=0,NX-1
      do j=1,NY
      do k=0,NZ-1
        buffer2(i,k,j)=real(U2(i,k,j+1)-U2(i,k,j))/(gy(j+1)-gy(j))
      end do
      end do
      end do
!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer2

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY 
        write(22,REC=j) buffer2(0:NX-1,0:NZ-1,j)
      end do

!      open (22,file='temp.raw',form='UNFORMATTED')
!      write(22) buffer2
      close(22)
      call SYSTEM('raw2vdf -ts 0 -varname DW vapor.vdf temp.raw')

      do i=0,NX-1
      do j=1,NY
      do k=0,NZ-1
        buffer2(i,k,j)=real(S1(i,k,j))
      end do
      end do
      end do
!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer2

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY 
        write(22,REC=j) buffer2(0:NX-1,0:NZ-1,j)
      end do

!      open (22,file='temp.raw',form='UNFORMATTED')
!      write(22) buffer2
      close(22)
      call SYSTEM('raw2vdf -ts 0 -varname OMEGA vapor.vdf temp.raw')

      END IF

      IF (NUM_PER_DIR.eq.2) THEN
      do i=0,NX-1
      do j=1,NY
      do k=0,NZ-1
        buffer2(i,k,j)=real(U2(i,k,j)+U2(i,k,j+1))*0.5d0
      end do
      end do
      end do
      call SYSTEM('rm -f temp.raw') 

!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer2

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY 
        write(22,REC=j) buffer2(0:NX-1,0:NZ-1,j)
      end do

!      open (22,file='temp.raw',form='UNFORMATTED')
!      write(22) buffer2
      close(22)
      ELSE IF (NUM_PER_DIR.eq.3) THEN
      do i=0,NX-1
      do j=0,NY-1
      do k=0,NZ-1
        buffer3(i,k,j)=real(U2(i,k,j)+U2(i,k,j+1))*0.5d0
      end do
      end do
      end do
      call SYSTEM('rm -f temp.raw') 
!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer3

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY-1
        write(22,REC=j) buffer3(0:NX-1,0:NZ-1,j)
      end do

!      open (22,file='temp.raw',form='UNFORMATTED')
!      write(22) buffer3
      close(22)
      END IF 

      call SYSTEM('raw2vdf -ts 0 -varname U3 vapor.vdf temp.raw')

      IF (NUM_PER_DIR.eq.2) THEN
      do i=0,NX-1
      do j=1,NY
      do k=0,NZ-1
        buffer2(i,k,j)=real(U3(i,k,j))
      end do
      end do
      end do
      call SYSTEM('rm -f temp.raw') 
!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer2

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY
        write(22,REC=j) buffer2(0:NX-1,0:NZ-1,j)
      end do

!      open (22,file='temp.raw',form='UNFORMATTED')
!      write(22) buffer2
      close(22)

      ELSE IF (NUM_PER_DIR.eq.3) THEN

      do i=0,NX-1
      do j=0,NY-1
      do k=0,NZ-1
        buffer3(i,k,j)=real(U3(i,k,j))
      end do
      end do
      end do
      call SYSTEM('rm -f temp.raw') 
!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer3

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY-1
        write(22,REC=j) buffer3(0:NX-1,0:NZ-1,j)
      end do

!      open (22,file='temp.raw',form='UNFORMATTED')
!      write(22) buffer3
      close(22)
      END IF

      call SYSTEM('raw2vdf -ts 0 -varname U2 vapor.vdf temp.raw')

      if (N_TH.ge.1) then

      IF (NUM_PER_DIR.eq.2) THEN
      do i=0,NX-1
      do j=1,NY
      do k=0,NZ-1
        buffer2(i,k,j)=real(TH(i,k,j,1))
      end do
      end do
      end do
      call SYSTEM('rm -f temp.raw')
!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer2

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY
        write(22,REC=j) buffer2(0:NX-1,0:NZ-1,j)
      end do

!      open (22,file='temp.raw',form='UNFORMATTED')
!      write(22) buffer2
      close(22)
      
      ELSE IF (NUM_PER_DIR.eq.3) THEN
      do i=0,NX-1
      do j=0,NY-1
      do k=0,NZ-1
        buffer3(i,k,j)=real(TH(i,k,j,1))
      end do
      end do
      end do
      call SYSTEM('rm -f temp.raw')
!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer3

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY-1
        write(22,REC=j) buffer3(0:NX-1,0:NZ-1,j)
      end do

!      open (22,file='temp.raw',form='UNFORMATTED')
!      write(22) buffer3
      close(22)
      END IF

      call SYSTEM('raw2vdf -ts 0 -varname TH1 vapor.vdf temp.raw')

      IF (NUM_PER_DIR.eq.2) THEN
      do i=0,NX-1
      do j=1,NY
      do k=0,NZ-1
        buffer2(i,k,j)=real(TH(i,k,j,1)-TH_BAR(j,1))
      end do
      end do
      end do

!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer2

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY
        write(22,REC=j) buffer2(0:NX-1,0:NZ-1,j)
      end do

      call SYSTEM('raw2vdf -ts 0 -varname TH1P vapor.vdf temp.raw')
      END IF    
 
      end if

      if (N_TH.ge.2) then 

      IF (NUM_PER_DIR.eq.2) THEN
      do i=0,NX-1
      do j=1,NY
      do k=0,NZ-1
        buffer2(i,k,j)=real(TH(i,k,j,2))
      end do
      end do
      end do
      call SYSTEM('rm -f temp.raw')
!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer2

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY
        write(22,REC=j) buffer2(0:NX-1,0:NZ-1,j)
      end do

!      open (22,file='temp.raw',form='UNFORMATTED')
!      write(22) buffer2
      close(22)
      ELSE IF (NUM_PER_DIR.eq.3) THEN
      do i=0,NX-1
      do j=0,NY-1
      do k=0,NZ-1
        buffer3(i,k,j)=real(TH(i,k,j,2))
      end do
      end do
      end do

!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer3

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY-1
        write(22,REC=j) buffer3(0:NX-1,0:NZ-1,j)
      end do

      close(22)
      END IF
    

      call SYSTEM('raw2vdf -ts 0 -varname TH2 vapor.vdf temp.raw')

      end if

      if (N_TH.ge.2) then

      IF (NUM_PER_DIR.eq.2) THEN
      do i=0,NX-1
      do j=1,NY
      do k=0,NZ-1
        buffer2(i,k,j)=real(TH(i,k,j,3))
      end do
      end do
      end do
      call SYSTEM('rm -f temp.raw')

!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer2

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY
        write(22,REC=j) buffer2(0:NX-1,0:NZ-1,j)
      end do

      close(22)
      ELSE IF (NUM_PER_DIR.eq.3) THEN
      do i=0,NX-1
      do j=0,NY-1
      do k=0,NZ-1
        buffer3(i,k,j)=real(TH(i,k,j,3))
      end do
      end do
      end do

!      open (22,file='temp.raw',form='UNFORMATTED',
!     &   ACCESS='DIRECT',RECL=NX*NY*NZ*4)
!      write(22,REC=1) buffer3

      open (22,file='temp.raw',form='UNFORMATTED',
     &   ACCESS='DIRECT',RECL=NX*NZ*4)
      do j=1,NY-1
        write(22,REC=j) buffer3(0:NX-1,0:NZ-1,j)
      end do

      close(22)
      END IF
   

      call SYSTEM('raw2vdf -ts 0 -varname TH3 vapor.vdf temp.raw')

      end if


      call SYSTEM('rm -f temp.raw')

!     We are done
      stop
      end

