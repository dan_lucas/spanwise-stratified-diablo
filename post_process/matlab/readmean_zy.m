% Run after readmean.m
% clear all;
LX=12.0;
NX=256;
LZ=4.0;
NZ=256;
LY=2;
NY=129;
NR = 1;

x=linspace(0,LX,NX);
z=linspace(0,LZ,NZ);

nk0=0;
for id=1:4
filename=['./run' num2str(id) '/stats_zy.h5'];
%filename=['stats_zy.h5'];
file_info=h5info(filename);
att_info=file_info.Groups.Attributes;
nk=nk0+att_info.Value;

for t=(nk0+1):nk
timestep = t-nk0;
file_number = sprintf('%04d', timestep);
%disp(['timestep: ', num2str(timestep)]);

varname_ume=['/ume_zy/' file_number];
varname_vme=['/vme_zy/' file_number];
varname_wme=['/wme_zy/' file_number];
varname_thme=['/thme_zy/' file_number];
varname_urms=['/urms_zy/' file_number];
varname_vrms=['/vrms_zy/' file_number];
varname_wrms=['/wrms_zy/' file_number];
varname_thrms=['/thrms_zy/' file_number];
varname_uv=['/uv_zy/' file_number];
varname_wv=['/wv_zy/' file_number];
varname_uw=['/uw_zy/' file_number];
varname_thu=['/thu_zy/' file_number];
varname_thv=['/thv_zy/' file_number];
varname_thw=['/thw_zy/' file_number];

ume_zy_temp = h5read(filename,varname_ume);
vme_zy_temp = h5read(filename,varname_vme);
wme_zy_temp = h5read(filename,varname_wme);
thme_zy_temp = h5read(filename,varname_thme);
urms_zy_temp = h5read(filename,varname_urms);
vrms_zy_temp = h5read(filename,varname_vrms);
wrms_zy_temp = h5read(filename,varname_wrms);
thrms_zy_temp = h5read(filename,varname_thrms);
uv_zy_temp = h5read(filename,varname_uv);
wv_zy_temp = h5read(filename,varname_wv);
uw_zy_temp = h5read(filename,varname_uw);
thu_zy_temp = h5read(filename,varname_thu);
thv_zy_temp = h5read(filename,varname_thv);
thw_zy_temp = h5read(filename,varname_thw);

ume_zy(:,:,t) = ume_zy_temp;
vme_zy(:,:,t) = vme_zy_temp; 
wme_zy(:,:,t) = wme_zy_temp;
thme_zy(:,:,t,:) = thme_zy_temp;
urms_zy(:,:,t) = urms_zy_temp;
vrms_zy(:,:,t) = vrms_zy_temp;
wrms_zy(:,:,t) = wrms_zy_temp;
thrms_zy(:,:,t,:) = thrms_zy_temp;
uv_zy(:,:,t) = uv_zy_temp;
wv_zy(:,:,t) = wv_zy_temp;
uw_zy(:,:,t) = uw_zy_temp;
thu_zy(:,:,t,:) = thu_zy_temp;
thv_zy(:,:,t,:) = thv_zy_temp;
thw_zy(:,:,t,:) = thw_zy_temp;
 

%  pcolor(z/1000,y,ume_zy(:,:,nk_tii,1)'); shading interp; view(0,90);
%  set(gca,'FontName','Times','FontSize',14);
%  xlabel('$x\ (\textrm{km})$','interpreter','latex'); 
%  ylabel('$y\ (\textrm{km})$','interpreter','latex'); 
%  title([num2str(tii(nk_tii)/3600/24,2), ' days'],'Interpreter','latex');
%  c = colorbar;
%  ylabel(c,'$ume''\ (\textrm{m}\textrm{s}^{-1})$','Interpreter','latex');
%  colormap(jet(500));
%  axis tight
%  
%  M(floor(nk_tii/20)+1)=getframe(gcf);
%  pause(0.002);
% %  pause;
%  clf;

end
nk0=nk;
end
disp('done readmean_zy');



Ubar=trapz(tii(50:nk-1),ume_zy(:,:,50:(nk-1)),3)/(tii(nk-1)-tii(50));
Vbar=trapz(tii(50:nk-1),vme_zy(:,:,50:(nk-1)),3)/(tii(nk-1)-tii(50));
Wbar=trapz(tii(50:nk-1),wme_zy(:,:,50:(nk-1)),3)/(tii(nk-1)-tii(50));
THbar=trapz(tii(50:nk-1),thme_zy(:,:,50:(nk-1)),3)/(tii(nk-1)-tii(50));

fileU = fopen('Umean.txt','w');
fileV = fopen('Vmean.txt','w');
fileW = fopen('Wmean.txt','w');
fileTH = fopen('THmean.txt','w');

dx=LX/NX;
dz=LZ/NZ;
for iz=1:NZ
  for iy=1:NY
    fprintf(fileU,'%e %e %e \n',gyf(iy),iz*dz,Ubar(iz,iy));
    fprintf(fileV,'%e %e %e \n',gyf(iy),iz*dz,Vbar(iz,iy));
    fprintf(fileW,'%e %e %e \n',gyf(iy),iz*dz,Wbar(iz,iy));
    fprintf(fileTH,'%e %e %e \n',gyf(iy),iz*dz,THbar(iz,iy));
end
    fprintf(fileU,'   \n');
    fprintf(fileV,'   \n');
    fprintf(fileW,'   \n');
    fprintf(fileTH,'   \n');

end

