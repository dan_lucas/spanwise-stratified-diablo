
for k=1,nk_th
for j=1:NY
  temp(j,k)=thv(1,j,k);
end
end

surf(tii(1:nk_th),gyf(1:NY),temp,'EdgeColor','none'),view(0,90)
axis tight;
colorbar;
xlabel('Time');
ylabel('z/h');
title('<\Theta_1''v''>');


