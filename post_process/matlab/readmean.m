% User settings....
% Reads in statistics outputted by diablo 
% Set the grid and domain size in the y-direction
NY=input('Insert the number of points in the vertical: ');
LY=input('Insert the domain size in the vertical: ');
% Enter the number of scalars
N_TH=1;
% Define input files
turbo=dlmread('mean.txt');
if (N_TH>0)
  turbo_th=dlmread('mean_th.txt');
end

% Enter the viscosity
NU=0.001;

% Enter the richardson number for each scalar
RI(1)=0;

% Set the starting time in code units for start of averaging
tstart=0;

% Determine the number of records in the file based on its length
nk=floor(length(turbo(:,1))/(NY+3))

NYM=NY-1;

row=1;
for k=1:nk
  tii(k)=turbo(row,2);
  dt(k)=turbo(row,3);
  row=row+1;
  ubulk(k)=turbo(row,1);
  row=row+1;
  for j=1:NY
    gyf(j)=turbo(row,2);
    ume(j,k)=turbo(row,3);
    vme(j,k)=turbo(row,4);
    wme(j,k)=turbo(row,5);
    urms(j,k)=turbo(row,6);
    vrms(j,k)=turbo(row,7);
    wrms(j,k)=turbo(row,8);
    uv(j,k)=turbo(row,9);
    uw(j,k)=turbo(row,10);
    wv(j,k)=turbo(row,11);
    dudy(j,k)=turbo(row,12);
    dwdy(j,k)=turbo(row,13);
    cp(j,k)=turbo(row,14);
    shear(j,k)=turbo(row,15);
    row=row+1;
  end
end


% Now read in the geophysical variables
if (N_TH>0)

% Determine the number of records in the file based on its length
  nk_th=floor(length(turbo_th(:,1))/(N_TH*(NY+3)))
else
  nk_th=0;
end

nk_th=min(nk,nk_th)
nk=min(nk,nk_th)

row=1;
for k=1:nk_th
  tii(k)=turbo_th(row,2);
  dt(k)=turbo_th(row,3);
  row=row+1;
  ubulk(k)=turbo_th(row,1);
  row=row+1;
  for n=1:N_TH
  for j=1:NY
    thme(j,k,n)=turbo_th(row,3);
    dthdy(j,k,n)=turbo_th(row,4); % Add one here if a background was subtracted
    thrms(j,k,n)=turbo_th(row,5);
    thv(j,k,n)=turbo_th(row,6); 
    if (RI(n) ~= 0) 
      pe_diss(j,k,n)=turbo_th(row,7)*NU/RI(n);
    else
      pe_diss(j,k,n)=0;
    end
    row=row+1;
  end
  end
end



% Compute secondary quantities
for k=1:nk
  for j=1:NY
    tke(j,k)=0.5*(urms(j,k)^2.+vrms(j,k)^2.+wrms(j,k)^2.);
    if (dudy(j,k)~=0)
      nu_t(j,k)=-uv(j,k)/dudy(j,k);
    else
      nu_t(j,k)=0;
    end
% Calculate the vertical taylor scale
    if (shear(j,k)~=0) 
      taylor(j,k)=sqrt((ume(j,k)^2.+wme(j,k)^2.+urms(j,k)^2.+wrms(j,k)^2.)/shear(j,k));
    else
      taylor(j,k)=0;
    end
    if (N_TH > 0)
      for n=1:N_TH
        brunt(j,k,n)=sqrt(RI(n)*(dthdy(j,k,n))); 
        if (shear(j,k)~=0) 
          grarich(j,k,n)=brunt(j,k,n)^2./shear(j,k); 
        else
          grarich(j,k,n)=0;
        end
        if (dthdy(j,k,n)~=0)
          kappa_t(j,k,n)=-thv(j,k,n)/dthdy(j,k,n);
        else
          kappa_t(j,k,n)=0;
        end 
      end

    end
  end
end

% Get the time index based on start time
kstart=0;
for k=1:nk
  if (tii(k) <= tstart)
     kstart=k;
  end
end
if (kstart == 0)
  kstart=1;
end
'Start of time average: ',tii(kstart)

for j=1:NY
  ume_mean(j)=mean(ume(j,kstart:nk));
  vme_mean(j)=mean(vme(j,kstart:nk));
  wme_mean(j)=mean(wme(j,kstart:nk));
  urms_mean(j)=mean(urms(j,kstart:nk));
  vrms_mean(j)=mean(vrms(j,kstart:nk));
  wrms_mean(j)=mean(wrms(j,kstart:nk));
  dudy_mean(j)=mean(dudy(j,kstart:nk));
  dwdy_mean(j)=mean(dwdy(j,kstart:nk));
  tke_mean(j)=mean(tke(j,kstart:nk));
  uv_mean(j)=mean(uv(j,kstart:nk));
  wv_mean(j)=mean(wv(j,kstart:nk));
  cp_mean(j)=mean(cp(j,kstart:nk));
  if (dudy_mean(j)~=0) 
    nu_t_mean(j)=-uv_mean(j)/dudy_mean(j);
  else
    nu_t_mean(j)=0;
  end
  for n=1:N_TH
    thv_mean(j,n)=mean(thv(j,kstart:nk_th,n));
    dthdy_mean(j,n)=mean(dthdy(j,kstart:nk_th,n));
    thrms_mean(j,n)=mean(thrms(j,kstart:nk_th,n));
    thbar(j,n)=mean(thme(j,kstart:nk_th,n));
    pe_diss_mean(j,n)=mean(pe_diss(j,kstart:nk_th,n));
    if (dthdy_mean(j,n)~=0) 
      kappa_t_mean(j,n)=-thv_mean(j,n)/dthdy_mean(j,n);
    else
      kappa_t_mean(j,n)=0;
    end 
  end
  shear_mean(j)=mean(shear(j,kstart:nk));
end
  
%for j=1:NYM
%for k=1:nk
%  eta(j,k)=epsilon(j,k)^(-1/4)*(1/sqrt(400));
%end
%end
   
for j=2:NY
  gy(j)=(gyf(j)+gyf(j-1))/2;    
end
for j=2:NYM
  dyf(j)=(gy(j+1)-gy(j));
end
for j=2:NY
  dy(j)=gyf(j)-gyf(j-1);
end

for j=2:NY-1
  if ((gyf(j)-gyf(j-1))~=0)
    ry(j)=(gyf(j+1)-gyf(j))/(gyf(j)-gyf(j-1));
  else
    ry(j)=1.0;
  end
end


