% Reads in statistics outputted by diablo

turbo=dlmread('tke.txt');

% Set the domain size
NY=256;
NYM=NY-1;

% Enter the viscosity
NU=1/100;

% Set the starting time in code units for start of averaging
tstart=1;

% Determine the number of records in the file based on its length
nk=floor(length(turbo(:,1))/(NY+1))

row=1;
for k=1:nk
  tii(k)=turbo(row,2);
  dt(k)=turbo(row,3);
  ubulk(k)=turbo(row,4);
  row=row+1;
  for j=1:NY
%    gyf(j)=turbo(row,2);
    epsilon(j,k)=turbo(row,3); 
    row=row+1;
  end
end

epsilon=epsilon*NU;

% Get the time index based on start time
kstart=0;
for k=1:nk
  if (tii(k) <= tstart)
     kstart=k;
  end
end
if (kstart == 0)
  kstart=1;
end
'Start of time average: ',tii(kstart)

for j=1:NY
for k=1:nk
  eta(j,k)=(NU^3/epsilon(j,k))^(1/4);
end
end

for j=1:NY
  epsilon_mean(j)=mean(epsilon(j,kstart:nk));
  eta_mean(j)=mean(eta(j,kstart:nk));
end
  
   
%for j=2:NY
%  gy(j)=(gyf(j)+gyf(j-1))/2;    
%end
%gy(1)=(gyf(1)+0)/2;
%gy(NY)=(1+gyf(NYM))/2;
%for j=1:NYM
%  dyf(j)=(gy(j+1)-gy(j));
%end


