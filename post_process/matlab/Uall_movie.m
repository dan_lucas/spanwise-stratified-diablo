LX=12.0;
NX=256;
LZ=4.0;
NZ=256;
LY=2;
NY=129;
NR = 1;

x=linspace(0,LX,NX);
z=linspace(0,LZ,NZ);
time=0.0;
zy = zeros(NY,NZ);
yz = zeros(NY,NZ);
zx = zeros(NX,NZ);
for iz=1:NZ
    zy(:,iz)=LZ-z(iz);
    zx(:,iz)=LZ-z(iz);
end
for iy=1:NY
    yz(iy,:)=gy(iy);
end
figure('Visible','Off')
set(gcf,'Position',[10 10 1034 522])
for id=4:4
base_dir=['./run' num2str(id) '/'];
%base_dir=['./'];
filename=[base_dir,'movie.h5'];

file_info=h5info(filename);
att_info=file_info.Groups.Attributes;
nk=att_info.Value;


for k=1:5:nk
%kk=k;
kk=k+(id-4)*nk
if (k<10)
  timename=['000' int2str(k)];
elseif (k<100)
timename=['00' int2str(k)];
elseif (k<1000)
timename=['0' int2str(k)];
 else
 timename=[int2str(k)];
end
if (kk<10)
  timename2=['000' int2str(kk)];
elseif (kk<100)
timename2=['00' int2str(kk)];
elseif (kk<1000)
timename2=['0' int2str(kk)];
 else
 timename2=[int2str(kk)];
end

varname=['/w_zy/' timename];

A=h5read(filename,varname);

P(1)=subplot(1,2,1);
pcolor(gy,z,A);shading interp;
				%    title(['t=',num2str(tii(k)-tii(1),3)])
%caxis([-0.5 0.5]); 
%caxis([0 LZ]); 
				% axis([LX/2 LX LZ/2 LZ]);
%colormap(flipud(gray));
map=cmocean('deep'); 
colormap(map);
				%    xlabel('x'), ylabel('y')
colorbar
axis equal
axis tight
%hold on
%plot(th_avg(kk,:)',z)                             %
%hold off

varname=['/u_xz/' timename];
figname=['fig_' timename2 '.png']

A=h5read(filename,varname);
P(2)=subplot(1,2,2);
%temp=LX*(th_avg(kk,:)+1.0)/2.0;
pcolor(x,z,A'); shading interp;
colormap(map);
time=tii(kk);
title(time)
axis equal
axis tight
%caxis([-1. 1.]); 
%caxis([-0.2 0.2]); 
colorbar
%hold on
%plot(temp',z)                             %
%hold off
linkaxes(P,'y');

saveas(gcf,figname);
%M(k)=getframe(gcf);
end
end

% end

%movie2avi(M,'movie.avi','FPS',1);
