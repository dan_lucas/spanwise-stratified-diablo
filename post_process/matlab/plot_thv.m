% Create a new figure
set(gcf,'Renderer','opengl')

for n=1:N_TH

% Store each scalar in a temprorary matrix for plotting
for j=1:NY
for k=1:nk_th
  temp(j,k)=thv(n,j,k);
end
end

% Get the max of this scalar, used to make peak theta opaque
maxtemp=max(max(temp));
mintemp=min(min(temp));

% Get a matrix of colors to cycle through for each scalar
colormat=get(gca,'ColorOrder');
for i=1:3
  colorvec(i)=colormat(mod(n,7),i);
end

clear label_matrix
% Surface plot
surf(tii(1:nk_th),gyf(1:NY),temp,'EdgeColor','none','FaceAlpha','flat','AlphaDataMapping','scaled','AlphaData',(temp-mintemp)/(maxtemp-mintemp),'FaceColor',colorvec);
label_matrix(n,1:9)='<\Theta''_';
if (n>=10) 
  label_matrix(n,10)=int2str(floor(n/10));
  label_matrix(n,11)=int2str(mod(n,10));
  label_matrix(n,12:15)=' v''>';
else 
  label_matrix(n,10)=int2str(n);
  label_matrix(n,11:14)=' v''>';
end

hold on

end

view(0,90);
axis tight;
legend(label_matrix);
xlabel('Time');
ylabel('Y');
title('Vertical turbulent scalar flux');
