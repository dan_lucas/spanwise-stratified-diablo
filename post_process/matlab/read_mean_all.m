% Loop over all processes
NP=2;
NY_S=65;
N_TH=1;
RI=0.7;
Pr = 1.0;
tstart=0.0;
NU=1.0/10000.0;
kappa=NU;
nk0=0;
for id=1:4
base_dir=['./run' num2str(id) '/'];
%base_dir=['./'];

for proc=1:NP
    
    % Define input files
    turbo=dlmread([base_dir 'mean' num2str(proc) '.txt']);
    if (N_TH>0)
        turbo_th=dlmread([base_dir 'mean_th' num2str(proc) '.txt']);
    end
    
    % Set the start and end indices corresponding to the full size array in y
    if proc == 1
        jstart = (NY_S-1)*(proc-1)+1
        jend   = jstart+NY_S-1
    else
        jstart = (NY_S-1)*(proc-1)+2
        jend   = jstart+NY_S-2
    end
    
    % Calculate the global size of NY
    NY=(NY_S-1)*proc+1;
    
    % Determine the number of records in the file based on its length
    nk=nk0+floor(length(turbo(:,1))/(NY_S+2));
    
    row=1;
    for k1=(nk0+1):nk
        k=k1;
%        if (k1>=nk0+2)
%	  k=k1-1;
%	end
        tii(k)=turbo(row,2);	
        dt(k)=turbo(row,3);
        row=row+1;
        ubulk(k)=turbo(row,1);
        row=row+1;
        if (proc~=1)
            row=row+1;
        end
        for j=jstart:jend
            gyf(j)=turbo(row,2);
            ume(j,k)=turbo(row,3);
            vme(j,k)=turbo(row,4);
            wme(j,k)=turbo(row,5);
            urms(j,k)=turbo(row,6);
            vrms(j,k)=turbo(row,7);
            wrms(j,k)=turbo(row,8);
            uv(j,k)=turbo(row,9);
            uw(j,k)=turbo(row,10);
            wv(j,k)=turbo(row,11);
            dudy0(j,k)=turbo(row,12); % QZ 09/29/15: Corrected
            dwdy0(j,k)=turbo(row,13); % QZ 09/30/15: Corrected
            cp(j,k)=turbo(row,14); % QZ 09/30/15: What is cp?
            % QZ 09/30/15: First row (time) of data looks odd.
            shear0(j,k)=turbo(row,15);% QZ 09/30/15: Corrected
            omega_x0(j,k)=turbo(row,16);% QZ 09/30/15: Corrected
            omega_y(j,k)=turbo(row,17);
            omega_z0(j,k)=turbo(row,18);% QZ 09/30/15: Corrected
            row=row+1;
        end
    end
    
    % Now read in scalars
    if (N_TH>0)
        % Determine the number of records in the file based on its length
        nk_th=nk0 + floor(length(turbo_th(:,1))/(N_TH*(NY_S+2)));
    else
        nk_th=0;
    end
    
    % Sometimes if a run was halted during writing of the mean files the
    % number of timesteps in mean.txt and mean_th.txt differ.  In this case,
    % use the smallest number
    nk_th=min(nk,nk_th);
    nk=min(nk,nk_th);
    
    row=1;
    for k1=(nk0+1):nk_th
        k=k1;
%        if (k1>=nk0+2)
%	  k=k1-1;
%	end
        tii2(k)=turbo_th(row,2);
        dt(k)=turbo_th(row,3);
        row=row+1;
        ubulk(k)=turbo_th(row,1);
        row=row+1;
        if (proc~=1)
            row=row+1;
        end
        for n=1:N_TH
            for j=jstart:jend
                thme(j,k,n)=turbo_th(row,3);
                dthdy0(j,k,n)=turbo_th(row,4); % Add one here if a background was subtracted
                thrms(j,k,n)=turbo_th(row,5);
                thv(j,k,n)=turbo_th(row,6);
                if (RI(n) ~= 0)
                    pe_diss(j,k,n)=turbo_th(row,7)*NU*RI(n)/Pr; % QZ 09/30/15: Needs to be corrected
                else
                    pe_diss(j,k,n)=turbo_th(row,7)*NU;
                end
                row=row+1;
            end
        end
    end
    
% End loop over NP procs
end
nk0=nk-1
end
% QZ 09/29/15 Recompute dthdy based on thme
%nk0=nk0+1;
for j=2:NY
    gy(j)=(gyf(j)+gyf(j-1))/2;
end

for n=1:N_TH
    thme_temp = squeeze(thme(:,:,n));
    dummy = diff(thme_temp,1,1);
    for k=1:nk
        dummy(:,k)=dummy(:,k)./diff(gyf)';
        dummy1=interp1(gy(2:end),dummy(:,k),gyf,'linear','extrap');
        dthdy(:,k,n)=dummy1;
    end
end

BV2 = dthdy*RI;
BV = sqrt(BV2);

% QZ 09/30/15 Recompute dudy based on ume
for n=1:N_TH
    ume_temp = squeeze(ume(:,:,n));
    dummy = diff(ume_temp,1,1);
    for k=1:nk
        dummy(:,k)=dummy(:,k)./diff(gyf)';
        dummy1=interp1(gy(2:end),dummy(:,k),gyf,'linear','extrap');
        dudy(:,k,n)=dummy1;
    end
end

% QZ 09/30/15 Recompute dudy based on wme
for n=1:N_TH
    wme_temp = squeeze(wme(:,:,n));
    dummy = diff(wme_temp,1,1);
    for k=1:nk
        dummy(:,k)=dummy(:,k)./diff(gyf)';
        dummy1=interp1(gy(2:end),dummy(:,k),gyf,'linear','extrap');
        dwdy(:,k,n)=dummy1;
    end
end

% QZ 09/30/15 Apply median filter to remove ``bands'' due to ghost points
shear    = medfilt1(shear0,3,[],1);
omega_x  = medfilt1(omega_x0,3,[],1);
omega_z  = medfilt1(omega_z0,3,[],1);

% Compute secondary quantities
for k=1:nk
    for j=1:NY
        tke(j,k)=0.5*(urms(j,k)^2.+vrms(j,k)^2.+wrms(j,k)^2.);
        if (dudy(j,k)~=0)
            nu_t(j,k)=-uv(j,k)/dudy(j,k);
        else
            nu_t(j,k)=0;
        end
        % Calculate the vertical taylor scale
        if (shear(j,k)~=0)
            taylor(j,k)=sqrt((ume(j,k)^2.+wme(j,k)^2.+urms(j,k)^2.+wrms(j,k)^2.)/shear(j,k));
        else
            taylor(j,k)=0;
        end
        if (N_TH > 0)
            for n=1:N_TH
                brunt(j,k,n)=sqrt(RI(n)*(dthdy(j,k,n)));
                if (shear(j,k)~=0)
                    grarich(j,k,n)=brunt(j,k,n)^2./shear(j,k);
                else
                    grarich(j,k,n)=0;
                end
                if (dthdy(j,k,n)~=0)
                    kappa_t(j,k,n)=-thv(j,k,n)/dthdy(j,k,n);
                else
                    kappa_t(j,k,n)=0;
                end
                tpe(j,k,n)=RI(n)*thrms(j,k,n).^2/dthdy(j,k,n);
            end
            
        end
    end
end

%tend=tii(end);
%tstart=tii(end)-100;

% Get the time index based on start time
kstart=0;
for k=1:nk
    if (tii(k) <= tstart)
        kstart=k;
    end
end
if (kstart == 0)
    kstart=1;
end
'Start of time averaging window: ',tii(kstart)

% Get the time index based on end time (if defined)
if exist('tend')
    kend=0;
    for k=1:nk
        if (tii(k) <= tend)
            kend=k;
        end
    end
    if (kend == 0)
        kend=1;
    end
else
    kend=nk0;
end
'End of time averaging window: ',tii(kend)


for j=1:NY
    ume_mean(j)=trapz(tii(kstart:kend),ume(j,kstart:kend))/(tii(kend)-tii(kstart));
    vme_mean(j)=trapz(tii(kstart:kend),vme(j,kstart:kend))/(tii(kend)-tii(kstart));
    wme_mean(j)=trapz(tii(kstart:kend),wme(j,kstart:kend))/(tii(kend)-tii(kstart));
    urms_mean(j)=trapz(tii(kstart:kend),urms(j,kstart:kend))/(tii(kend)-tii(kstart));
    vrms_mean(j)=trapz(tii(kstart:kend),vrms(j,kstart:kend))/(tii(kend)-tii(kstart));
    wrms_mean(j)=trapz(tii(kstart:kend),wrms(j,kstart:kend))/(tii(kend)-tii(kstart));
    dudy_mean(j)=trapz(tii(kstart:kend),dudy(j,kstart:kend))/(tii(kend)-tii(kstart));
    dwdy_mean(j)=trapz(tii(kstart:kend),dwdy(j,kstart:kend))/(tii(kend)-tii(kstart));
    tke_mean(j)=trapz(tii(kstart:kend),tke(j,kstart:kend))/(tii(kend)-tii(kstart));
    uv_mean(j)=trapz(tii(kstart:kend),uv(j,kstart:kend))/(tii(kend)-tii(kstart));
    wv_mean(j)=trapz(tii(kstart:kend),wv(j,kstart:kend))/(tii(kend)-tii(kstart));
    cp_mean(j)=trapz(tii(kstart:kend),cp(j,kstart:kend))/(tii(kend)-tii(kstart));
    omega_x_mean(j)=trapz(tii(kstart:kend),omega_x(j,kstart:kend))/(tii(kend)-tii(kstart));
    omega_y_mean(j)=trapz(tii(kstart:kend),omega_y(j,kstart:kend))/(tii(kend)-tii(kstart));
    omega_z_mean(j)=trapz(tii(kstart:kend),omega_z(j,kstart:kend))/(tii(kend)-tii(kstart));
    if (dudy_mean(j)~=0)
        nu_t_mean(j)=-uv_mean(j)/dudy_mean(j);
    else
        nu_t_mean(j)=0;
    end
    for n=1:N_TH
        thv_mean(j,n)=trapz(tii(kstart:kend),thv(j,kstart:kend,n))/(tii(kend)-tii(kstart));
        dthdy_mean(j,n)=trapz(tii(kstart:kend),dthdy(j,kstart:kend,n))/(tii(kend)-tii(kstart));
        thrms_mean(j,n)=trapz(tii(kstart:kend),thrms(j,kstart:kend,n))/(tii(kend)-tii(kstart));
        thme_mean(j,n)=trapz(tii(kstart:kend),thme(j,kstart:kend,n))/(tii(kend)-tii(kstart));
        pe_diss_mean(j,n)=trapz(tii(kstart:kend),pe_diss(j,kstart:kend,n))/(tii(kend)-tii(kstart));
        if (dthdy_mean(j,n)~=0)
            kappa_t_mean(j,n)=-thv_mean(j,n)/dthdy_mean(j,n);
        else
            kappa_t_mean(j,n)=0;
        end
    end
    shear_mean(j)=trapz(tii(kstart:kend),shear(j,kstart:kend))/(tii(kend)-tii(kstart));
end

% Calculate y-integrated quantities
urms_int1=trapz(gyf,urms,1)/2.0;
vrms_int1=trapz(gyf,vrms,1)/2.0;
wrms_int1=trapz(gyf,wrms,1)/2.0;
tke_int1=trapz(gyf,tke,1)/2.0;
tke_bar=trapz(gyf(1:NY),tke(1:NY,:),1)/(gyf(NY)-gyf(1));
hke_int1=trapz(gyf,(urms.^2+wrms.^2)/2,1)/2.0;;
vke_int1=trapz(gyf,vrms.^2/2,1)/2.0;;
tpe_int1=RI*trapz(gyf,thrms.^2,1)./2.0;
thv_int1=trapz(gyf,thv,1);
thrms_int1=trapz(gyf,thrms,1);

for j=2:NY-1
    dyf(j)=(gy(j+1)-gy(j));
end
for j=2:NY
    dy(j)=gyf(j)-gyf(j-1);
end

for j=2:NY-1
    if ((gyf(j)-gyf(j-1))~=0)
        ry(j)=(gyf(j+1)-gyf(j))/(gyf(j)-gyf(j-1));
    else
        ry(j)=1.0;
    end
end

% Calculate the friction velocity
utau=0.5*(sqrt(NU*dudy(2,:))+sqrt(NU*dudy(NY,:)));
B0=0.5*(kappa*dthdy(2,:)+kappa*dthdy(NY-1,:))*RI;
Obukhov=utau.^3/0.41./B0;
Re_Ob=Obukhov.*utau/NU;

% QZ 09/30/15: From the previous read_tke.m

% Loop over all processes
nk0=0;
for id=1:4
%base_dir=['./'];
base_dir=['./run' num2str(id) '/'];
for proc=1:NP
    
    % Define input files
    turbo=dlmread([base_dir 'tke' num2str(proc) '.txt']);
    
    % Set the start and end indices corresponding to the full size array in y
    if proc == 1
        jstart = (NY_S-1)*(proc-1)+1
        jend   = jstart+NY_S-1
    else
        jstart = (NY_S-1)*(proc-1)+2
        jend   = jstart+NY_S-2
    end
    
    % Calculate the global size of NY
    NY=(NY_S-1)*proc+1;
    
    % Determine the number of records in the file based on its length
    nk=nk0+floor(length(turbo(:,1))/(NY_S+1));
    
    row=1;
    
    for k1=(nk0+1):nk
      k=k1;
%      if (k1>=nk0+2)
%	k=k1-1;
%	end
        tii(k)=turbo(row,2);
        dt(k)=turbo(row,3);
        row=row+1;
        if (proc~=1)
            row=row+1;
        end
        for j=jstart:jend
%             gyf(j)=turbo(row,2);
            epsilon(j,k)=turbo(row,3);
            row=row+1;
        end
    end
    
end  % End loop over NP procs
nk0=nk-1;
end
% Row 1 was a ghost cell, set to zero
epsilon(1,:)=0;
%nk0=nk0+1;
% Compute secondary quantities
for k=1:nk
    for j=1:NY
        eta(j,k)=abs(epsilon(j,k))^(-0.25d0)*NU^(3/4);
    end
end

% Buoyancy Reynolds number
Re_b=epsilon(:,1:nk)/NU./(squeeze(dthdy(:,1:nk,1)*RI(1)));
loz=sqrt(epsilon./(dthdy*RI(1)).^(3/2));

% Compute dissipation and viscous scale
for j=1:NY
    epsilon_mean(j)=trapz(tii(kstart:kend),epsilon(j,kstart:kend))/(tii(kend)-tii(kstart));
    eta_mean(j)=trapz(tii(kstart:kend),eta(j,kstart:kend))/(tii(kend)-tii(kstart));
end


% Calculate y-integrated quantities
epsilon_int1=trapz(gyf,epsilon,1);
eta_int1=trapz(gyf,eta,1);

% QZ 09/30/15 Print out Re_tau and L^+ for verfification purpose
disp(['Re_{\tau}=',num2str(mean(utau)/NU,'%10.3e')])
disp(['L^+=',num2str(mean(Re_Ob),'%10.3e')])

