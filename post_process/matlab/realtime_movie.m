% User input
NX=128;
NY=64;
LX=3.14;
LY=2.0;

for i=1:NX
  xvec(i)=LX*i/NX;
end

null(1:NX,1:NY)=0;

k=1;
colormat=get(gca,'ColorOrder');

disp('Press Control-C to break');

figure
while (1~=0)
if (k~=1)
previous=moviefile;
end
moviefile=dlmread('./latest_slice.txt');
if (k==1)
  previous=moviefile;
end
% If we have updated the moviefile:
if ((previous==moviefile)&(k~=1))
pause(1);
else

k=k+1;
count=0;
for j=1:NY
count=count+1;
  yvec(j)=moviefile(count);
end

for i=1:NX
for j=1:NY
count=count+1;
  slice(i,j)=moviefile(count);
end 
end

surf(xvec,yvec,slice(:,:)','EdgeColor','none'),view(0,90)
%caxis([-1 1])
view(0,90); 
colorbar;
M(k-1)=getframe(gcf);

end


end



