nk2=nk;
urms_avg=zeros(nk2,1);
vrms_avg=zeros(nk2,1);
wrms_avg=zeros(nk2,1);

prod_avg=zeros(nk2,1);
bflux_avg=zeros(nk2,1);
tke_avg=zeros(nk2,1);
tpe_avg=zeros(nk2,1);
for k=1:nk2
  uavg=zeros(NZ,1);
  vavg=zeros(NZ,1);
  wavg=zeros(NZ,1);
  thavg=zeros(NZ,1);
  tkeavg=zeros(NZ,1);
  bfavg=zeros(NZ,1);
  prodavg=zeros(NZ,1);
  for i=1:NZ
				%    thme_zy(i,:,k)=thme_zy(i,:,k)-yz(i,:);
    tempu = urms_zy(i,:,k).^2';
    uavg(i) = trapz(gyf,tempu,1)/2.0;
    tempv = vrms_zy(i,:,k).^2';
    vavg(i) = trapz(gyf,tempv,1)/2.0;
    tempw = wrms_zy(i,:,k).^2';
    wavg(i) = trapz(gyf,tempw,1)/2.0;
    tempth = (thrms_zy(i,:,k).^2)';
    thavg(i) = trapz(gyf,tempth,1)/2.0;

    tempbf = thw_zy(i,:,k)';
    bfavg(i) = trapz(gyf,tempbf,1)/2.0;
    tempprod = uv_zy(i,:,k)';
    prodavg(i) = trapz(gyf,tempprod,1)/2.0;

    end
    urms_avg(k) = sqrt(sum(uavg)/NZ);
    vrms_avg(k) = sqrt(sum(vavg)/NZ);
    wrms_avg(k) = sqrt(sum(wavg)/NZ);
    
    tpe_avg(k) = RI*sum(thavg)/NZ;

    bflux_avg(k) = sum(bfavg)/NZ;
    prod_avg(k) = sum(prodavg)/NZ;
end
tke_avg = 0.5*(urms_avg.^2+vrms_avg.^2+wrms_avg.^2);
pediss_int1=trapz(gyf(2:NY-1),pe_diss(2:NY-1,:),1)/2.0;
% 
fileENERGY = fopen('energy.txt','a')
fileRMS = fopen('rms.txt','a')
  for i=1:nk2
	  fprintf(fileENERGY,'%e %e %e %e %e %e %e\n',tii(i),tke_avg(i),tpe_avg(i),bflux_avg(i),prod_avg(i),epsilon_int1(i),pediss_int1(i));
fprintf(fileRMS,'%e %e %e %e %e\n',tii(i),urms_avg(i),vrms_avg(i),wrms_avg(i),utau(i));
end
