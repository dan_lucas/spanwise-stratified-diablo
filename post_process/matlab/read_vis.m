vis=netcdf('vis.nc');
x=vis.VarArray(1).Data;
y=vis.VarArray(2).Data;
z=vis.VarArray(3).Data;
u=vis.VarArray(8).Data;
v=vis.VarArray(9).Data;
w=vis.VarArray(10).Data;
p=vis.VarArray(11).Data;
th=vis.VarArray(12).Data;
NX=length(x);
NY=length(y);
NZ=length(z);
N_TH=length(th(:,1,1,1));
if (N_TH >= 1) 
  th1(1:NX,1:NY,1:NZ)=th(1,1:NX,1:NY,1:NZ);
elseif (N_TH >=2)
  th2(1:NX,1:NY,1:NZ)=th(2,1:NX,1:NY,1:NZ);
elseif (N_TH >=3)
  th3(1:NX,1:NY,1:NZ)=th(3,1:NX,1:NY,1:NZ);
end
clear th;
clear vis;

[xmat,ymat,zmat]=meshgrid(x,y,z);
[xmat_s,ymat_s,zmat_s]=meshgrid(x(1:round(NX/5):NX),y(1:round(NY/5):NY),z(1:round(NZ/5):NZ));

figure
p=patch(isosurface(x,y,z,th1,0.5));
isonormals(x,y,z,th1,p);
set(p,'FaceColor','green','EdgeColor','none');
hold on

p=patch(isosurface(x,y,z,th2,0.5));
isonormals(x,y,z,th2,p);
set(p,'FaceColor','red','EdgeColor','none');

p=patch(isosurface(x,y,z,th2,0.5));
isonormals(x,y,z,th2,p);
set(p,'FaceColor','blue','EdgeColor','none');

%streamline(stream3(xmat,ymat,zmat,u,v,w,xmat_s,ymat_s,zmat_s));

daspect([1 1 1]);
axis([0 x(NX) 0 y(NY) 0 z(NZ)]);
view(3);
camlight; lighting phong;




