% Create a new figure
set(gcf,'Renderer','opengl')

for n=1:N_TH

% Store each scalar in a temprorary matrix for plotting
for j=1:NY
for k=1:nk_th
  temp(j,k)=thme(j,k,n);
  null(j,k)=0;
end
end

% Get the max of this scalar, used to make peak theta opaque
maxtemp=max(max(temp));
mintemp=min(min(temp));

% Get a matrix of colors to cycle through for each scalar
colormat=get(gca,'ColorOrder');
for i=1:3
  colorvec(i)=colormat(mod(n,7),i);
end

clear label_matrix
% Surface plot
surf(tii(1:nk_th),gyf(1:NY),null(1:NY,1:nk_th),'EdgeColor','none','FaceAlpha','flat','AlphaDataMapping','scaled','AlphaData',(temp(1:NY,1:nk_th)-mintemp)/(maxtemp-mintemp),'FaceColor',colorvec);
label_matrix(n,1:8)='<\Theta_';
if (n>=10) 
  label_matrix(n,9)=int2str(floor(n/10));
  label_matrix(n,10)=int2str(mod(n,10));
  label_matrix(n,11)='>';
else 
  label_matrix(n,9)=int2str(n);
  label_matrix(n,10)='>';
end

hold on

end

view(0,90);
axis tight;
legend(label_matrix);
xlabel('Time');
ylabel('Y');
title('Scalar mean');
