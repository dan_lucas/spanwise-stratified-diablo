NX=1024;
NY=128;
LX=100;
LY=1;
moviefile=dlmread('../../fronto/movie_v.txt');

for i=1:NX
  xvec(i)=LX*i/NX;
end
for j=1:NY
  yvec(j)=LY*j/NY;
end

null(1:NX,1:NY)=0;

colormat=get(gca,'ColorOrder');

% Get the number of timesteps
nk=length(moviefile(:,1))/(NX*NY);

count=0;

for k=1:nk

for i=1:NX
for j=1:NY
count=count+1;
  v_save(i,j,k)=moviefile(count);
end
end 
end

for k=1:nk
surf(xvec,yvec,v_save(:,:,k)','EdgeColor','none'),view(0,90)
view(0,90); 
colorbar;
M(k)=getframe;
clf
end



